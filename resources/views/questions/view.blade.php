@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       View Question
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('questions') }}">Question Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Type</h5>
                                   <strong>{{$questions->type}}</strong>                                     
                               </div>
                               <div class="col-sm-4 input_field_sections">
                                    <h5>User Type</h5>
                                   <strong>{{$questions->user_type}}</strong>                                     
                               </div>
                                
                                 {{--<div class="col-sm-4 input_field_sections">
                                    <h5>Category</h5>
                                      <strong>{{ isset($questions->category) && isset($questions->getcategory->name)?$questions->getcategory->name:'-'}}</strong>
                                   </div>
                                    <div class="col-sm-4 input_field_sections">
                                    <h5>Subcategory</h5>
                                      <strong>{{ isset($questions->subcategory) && isset($questions->getsubcategory->name)?$questions->getsubcategory->name:'-'}}</strong>
                                   </div> --}}
                                 
                              </div>
                              <?php $answertype = array('textbox'=>'Text box','textarea' =>'Text area','radio'=> 'Radio button','selectbox'=> 'Select box','checkbox'=> 'Check box','category'=> 'Category','date'=> 'Date'));?>
                              <div class="row">
                                <div class="col-sm-8 input_field_sections">
                                    <h5>Question</h5>
                                      <strong>{{$questions->question}}</strong>
                                   </div>
                                   <div class="col-sm-4 input_field_sections">
                                    <h5>Answer type</h5>
                                      <strong>{{ $answertype[$questions->answer_type]}}</strong>
                                   </div>
                              </div>
                              <div class="row">
                                @if($questions->answer_type == 'textbox')
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Input type</h5>
                                      <strong>{{ ucfirst($questions->input_type) }}</strong>
                                </div>
                                @elseif($questions->answer_type == 'radio' || $questions->answer_type == 'selectbox' || $questions->answer_type == 'checkbox' )
                                  @if($questions->answer_option != '')
                                  <?php
                                    $options = explode(',',  $questions->answer_option);
                                  ?>
                                  <div class="col-sm-4 input_field_sections">
                                    <h5>Input type</h5>
                                      @foreach($options as $opt)
                                      @if($opt!='')
                                      <i class="fa fa-check"></i> &nbsp;<strong>{{$opt}}</strong><br>
                                      @endif
                                      @endforeach
                                   </div>
                                   @endif
                                @endif
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Mandatory</h5>
                                      <strong>@if($questions->is_mandatory==1) Yes @else No @endif</strong>
                                   </div>
                                   <div class="col-sm-4 input_field_sections">
                                    <h5>Status</h5>
                                       <strong>@if($questions->status==1) Active @else Inactive @endif</strong>
                                   </div>
                              </div>
                             
                        </div>
                    </div>
                </div>
            </div>

          
           
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
