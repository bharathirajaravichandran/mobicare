 <?php 
  $type = array('signup','services')
 ?>
 <div class="row">
   <div class="col-sm-6 input_field_sections">
      <h5>Type</h5>
       <select class="form-control" name="type" id="type">
        <option value="">Select</option>
        @foreach($type as $k)
        <option value="{{$k}}" {{ isset($questions->type) && $questions->type == $k?'selected':'' }}>{{ ucfirst($k)}}</option>
        @endforeach
      </select>
   </div>
   <div class="col-sm-6 input_field_sections">
      <h5>User Type</h5>
       <select class="form-control" name="user_type" id="user_type">
        <option value="">Select</option>
        <option value="user" @if(isset($questions->user_type) && $questions->user_type=='user') selected @endif>User</option>
        <option value="provider" @if(isset($questions->user_type) && $questions->user_type=='provider') selected @endif>Provider</option>
        <option value="both" @if(isset($questions->user_type) && $questions->user_type=='both') selected @endif>Both</option>
      </select>
   </div>
</div>

  {{-- <div class="row servicediv" style="display: {{ isset($questions->type) && $questions->type == 'services'?"flex":"none" }};">
   
   <div class="col-sm-6 input_field_sections">
      <h5>Category</h5>
       <select class="form-control" name="category" id="category" onchange="return getsubcat(this.value)">
        <option value="">Select</option>
        @foreach($industry as $i)
        <option value="{{$i->id}}" {{ isset($questions->category) && $questions->category == $i->id?'selected':'' }}>{{$i->name}}</option>
        @endforeach
      </select>
   </div>

   <div class="col-sm-6 input_field_sections">
      <h5>Subcategory</h5>
       <select class="form-control" name="subcategory" id="subcategory">
        <option value="">Select</option>
        @if(isset($subcategory))
        @foreach($subcategory as $sub)
        <option value="{{$sub->id}}" {{ isset($questions->subcategory) && $questions->subcategory == $sub->id?'selected':'' }}>{{$sub->name}}</option>
        @endforeach
        @endif
      </select>
   </div>
</div> --}}  

<div class="row">
   <div class="col-sm-6 input_field_sections">
        <h5>Question</h5>
       <textarea name="question" class="form-control">{{ isset($questions->question)?$questions->question:'' }}</textarea>
   </div>
  <?php $answertype = array('textbox'=>'Text box','textarea' =>'Text area','radio'=> 'Radio button','selectbox'=> 'Select box','checkbox'=> 'Check box','category'=> 'Category','date'=> 'Date');?>
   <div class="col-sm-6 input_field_sections">
      <h5>Answer type</h5>
       <select class="form-control" name="answer_type" id="answer_type">
        <option value="">Select</option>
        @foreach($answertype as $k=>$val)
        <option value="{{$k}}" {{ isset($questions->answer_type) && $questions->answer_type == $k?'selected':'' }}>{{ ucfirst($val)}}</option>
        @endforeach
      </select>
   </div>
</div>
<div class="row"> 
   <div class="col-sm-6 input_field_sections optionTypeDiv" style="display: {{ isset($questions->answer_type) && ($questions->answer_type == 'radio' || $questions->answer_type == 'selectbox' || $questions->answer_type == 'checkbox')?"block":"none" }};">
      <h5>Options</h5>
      <div class="containerholder" >
        @if(isset($questions))
        <?php
          $options = explode(',', $questions->answer_option);
          foreach($options as $k=>$option){
            $keyval = $k+1;
        ?>
        <div class='element' id='div_{{$keyval}}'>
          <div style="width:90%; float:left; line-height: 0;">
          <input type='text' id='txt_{{$keyval}}' name="answer_option[]" class="form-control" value="{{$option}}" required="true">&nbsp;
          </div>
          <div style='width:10%; float:left'>
          @if($keyval == 1)
          <span class='add btn btn-success pull-right'><i class="fa fa-plus"></i></span>
          @else
          <span id='remove_{{$keyval}}' class='remove btn btn-danger pull-right'><i class='fa fa-remove'></i></span>
          @endif
          </div>
         </div>
        <?php } ?>
        @else
         <div class='element' id='div_1'>
          <div style="width:90%; float:left; line-height: 0;">
          <input type='text' id='txt_1' name="answer_option[]" class="form-control" required="true">&nbsp;
          </div>
          <div style='width:10%; float:left'>
          <span class='add btn btn-success pull-right'><i class="fa fa-plus"></i></span>
        </div>
         </div>
       @endif
      </div>
    </div>
    <?php $inputtype = array('text','number','email','date','location');?>
   <div class="col-sm-6 input_field_sections inputTypeDiv" style="display: {{ isset($questions->answer_type) && $questions->answer_type == 'textbox'?"block":"none" }}">
      <h5>Input type</h5>
       <select class="form-control" name="input_type" id="input_type">
        <option value="">Select</option>
        @foreach($inputtype as $type)
        @if($type!='')
        <option value="{{$type}}" {{ isset($questions->input_type) && $questions->input_type == $type?'selected':'' }}>{{ ucfirst($type)}}</option>
        @endif
        @endforeach
      </select>
   </div>
     <div id="sh1" class="col-sm-6 input_field_sections" style="display: {{ isset($questions->is_mandatory)?"block":"none" }}">
        <h5>Is mandatory?</h5>
        <input type="checkbox" name="is_mandatory" value="1" {{ isset($questions->is_mandatory) && $questions->is_mandatory == 1?'checked':'' }}>
      </div>
     <div id="sh2" class="col-sm-6 input_field_sections" style="display: {{ isset($questions->status) && $questions->status != ''?"block":"none" }}">                                    
        <h5>Status</h5>
        <select class="form-control" name="status">
         <option value="">-Select Status-</option>
         <option value="1" {{ isset($questions->status) && $questions->status == 1?'selected':'' }}>Active</option>
         <option value="0" {{ isset($questions->status) && $questions->status == 0?'selected':'' }}>Inactive</option>
        </select>
     </div>      
</div>

@if(Request::segment(2)=='add')
<div class="row" id="fh">
   <div class="col-sm-6 input_field_sections">
      <h5>Is mandatory?</h5>
      <input type="checkbox" name="is_mandatory" value="1" {{ isset($questions->is_mandatory) && $questions->is_mandatory == 1?'checked':'' }}>
    </div>
   <div class="col-sm-6 input_field_sections">                                    
      <h5>Status</h5>
      <select class="form-control" name="status">
       <option value="">-Select Status-</option>
       <option value="1" {{ isset($questions->status) && $questions->status == 1?'selected':'' }}>Active</option>
       <option value="0" {{ isset($questions->status) && $questions->status == 0?'selected':'' }}>Inactive</option>
      </select>
   </div>
</div>
@endif

<style>
.element {
    padding: 10px 0px;
    clear: both;
}
#txt_1-error { margin-top: 10px; }
</style>
<script>
  function getsubcat(category){
        $.ajax({
          url: '/subcategory/getsubcategory/'+category,
          success: function( data ){
                    $('#subcategory').html(data);
                },
        });
    }
  $(document).ready(function(){

  $('#type').change(function(){
    typeval = $(this).val();
    if(typeval == 'services'){
      $('.servicediv').css('display','flex');
    }else{
      $('.servicediv').css('display','none');
      $('#category').val('');
      $('#subcategory').val('');
    }
  });

  $('#answer_type').change(function(){
    $('#fh').remove();
    $('#sh1, #sh2').show();
    typeval = $(this).val();
    if(typeval == 'textbox'){
      $('.inputTypeDiv').css('display','block');
      $('.optionTypeDiv').css('display','none');
    }else if(typeval == 'radio' || typeval == 'selectbox' || typeval == 'checkbox'){
      $('.inputTypeDiv').css('display','none');
      $('#input_type').val('');
      $('.optionTypeDiv').css('display','block');
    }else if(typeval == 'category'){
       $('#txt_1').prop('required',false);       
       $('.inputTypeDiv').css('display','none');
       $('#input_type').val('');
       $('.optionTypeDiv').css('display','block');
    }else{
      $('#input_type').val('');
      $('.inputTypeDiv').css('display','none');
      $('.optionTypeDiv').css('display','none');
    }
  });

  

 $(".add").click(function(){
  var total_element = $(".element").length;
  var lastid = $(".element:last").attr("id");
  var split_id = lastid.split("_");
  var nextindex = Number(split_id[1]) + 1;

  var max = 5;
  if(total_element < max ){
   $(".element:last").after("<div class='element' id='div_"+ nextindex +"'></div>");
   $("#div_" + nextindex).append("<div style='width:90%;  float:left'><input type='text' class='form-control' name='answer_option[]' required='true' id='txt_"+ nextindex +"  '></div><div style='width:10%; float:left' ><span id='remove_" + nextindex + "' class='remove btn btn-danger pull-right'><i class='fa fa-remove'></i></span></div>");
  }else{
    alert('You cannot add more than 5 options');
  }
 });

 $('.containerholder').on('click','.remove',function(){ 
  var id = this.id;
  var split_id = id.split("_");
  var deleteindex = split_id[1];
  $("#div_" + deleteindex).remove();
 }); 
});

$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    // validate form on keyup and submit
    $("#question_form").validate({
        rules: {
            type: { 
                required: true,
            },
            user_type: { 
                required: true,
            },
            question: { 
                required: true,
                rangelength: [1, 150]
            },
            answer_type: { 
                required: true,
            },
            status: {
                required: true
            }
        },
        messages: {
            type: { 
                required: "Please select the type",
            },
            user_type: { 
                required: "Please select the user type",
            },
            question: { 
                required: "Please enter question"
            },
            answer_type: { 
                required: "Please select answer type"
            },
            status: { 
                required: "Please select the status"
            }               
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "type"||element.attr("name") == "user_type"||element.attr("name") == "answer_type"||element.attr("name") == "status") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });
});
</script>