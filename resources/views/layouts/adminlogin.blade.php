<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <title>Mobicare Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   <!--global styles-->
    <link type="text/css" rel="stylesheet" href="css/components.css" />
    <link type="text/css" rel="stylesheet" href="css/custom.css" />
    <link type="text/css" rel="stylesheet" href="css/pages/index.css">
</head>

<body class="body" style="background-color: #ffffff;">
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="img/loader.gif" style=" width: 50px;" alt="loading...">
    </div>
</div>
<div id="wrap">

<div class="wrapper">

    @yield('content')

</div>

 <!-- # right side -->
</div>
<!-- /#wrap -->

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
$(window).on("load",function () {
    $('.preloader img').fadeOut();
    $('.preloader').fadeOut(1000);
});

//back button disable when press back button 
history.pushState(null, null, location.href);
window.onpopstate = function () {
    history.go(1);
};
//back button disable reload page when press back button 
</script>


<style type="text/css">
    .help-block { color:crimson; }
</style>

</body>
</html>

