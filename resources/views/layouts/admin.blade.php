<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="UTF-8">
    <title>Mobicare</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--global styles-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/components.css?v=1.1') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/custom.css?v=1.4') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/pages/index.css') }}">
    <!--end global styles-->

    <link type="text/css" rel="stylesheet" href="{{ asset('vendors/datepicker/css/bootstrap-datepicker.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('vendors/datatables/css/dataTables.bootstrap.css') }}" />

     <!--plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('vendors/select2/css/select2.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('vendors/datatables/css/scroller.bootstrap.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/pages/dataTables.bootstrap.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('css/plugincss/responsive.dataTables.min.css') }}" />    
    <!-- end of plugin styles -->

    <link type="text/css" rel="stylesheet" href="{{ asset('css/pages/tables.css') }}" />

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>

</head>

<body class="body">
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="{{ asset('img/loader.gif') }}" style=" width: 50px;" alt="loading...">
    </div>
</div>
<div id="wrap">
    <div id="top">
        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0">
                <a class="navbar-brand" href="{{ url('admin') }}">
                    <h4>MobiCare / <span>Dashboard</span></h4>
                </a>
                <div class="menu mr-sm-auto">
                    <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars"></i>
                    </span>
                </div>
                {{--<div class="top_search_box d-none d-md-flex">
                    <form class="header_input_search">
                        <input type="text" placeholder="" name="search">
                        <button type="submit">
                            <span class="font-icon-search"></span>
                        </button>
                        <div class="overlay"></div>
                    </form>
                </div>--}}
                <div class="topnav dropdown-menu-right">
                    <div class="btn-group small_device_search" data-toggle="modal"
                         data-target="#search_modal">
                        <i class="fa fa-search text-primary"></i>
                    </div>
                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                @if(isset(Auth::user()->photo) && Auth::user()->photo!='')
                                <img src="{{ url('storage/user/'.Auth::user()->photo) }}" class="admin_img2 img-thumbnail rounded-circle avatar-img" alt="avatar"> 
                                @else
                                <img src="{{ asset('img/admin.jpg') }}" class="admin_img2 img-thumbnail rounded-circle avatar-img" alt="avatar"> 
                                @endif

                                <strong>{{ucfirst(Auth::user()->first_name)." ".Auth::user()->last_name}}</strong>
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                <a class="dropdown-item title" href="#">
                                    Hello {{ucfirst(Auth::user()->first_name." ".Auth::user()->last_name)}}</a>
                                <a class="dropdown-item" href="{{url('dashboard')}}"><i class="fa fa-cogs"></i>
                                    Dashboard</a>
                                <a class="dropdown-item" href="{{url('myprofile')}}"><i class="fa fa-user"></i>
                                    &nbsp;My Profile</a>
                                <a class="dropdown-item" href="{{route('logout')}}"><i class="fa fa-sign-out"></i>
                                    Log Out</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->

<div class="wrapper">    

    @include('sidebar')

    @yield('content')

</div>

 <!-- # right side -->
</div>
<!-- /#wrap -->

<script type="text/javascript" src="{{ asset('js/components.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendors/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datetimepicker/js/DateTimePicker.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendors/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/switchery/js/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js?v=1.1') }}"></script>
<script type="text/javascript" src="{{ asset('js/pluginjs/dataTables.tableTools.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pluginjs/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.rowReorder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.colVis.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.scroller.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pages/datatable.js?v=1.2') }}"></script>
<script>
    $('select').select2({
        placeholder: "Select",
    });
    
</script>

</body>

<style type="text/css">
    .back { display: none; }
</style>

</html>

