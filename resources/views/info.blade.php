@extends('layouts.admin')

@section('content')
<div class="wrap">
   <div class="logo">
      <h1>Permission Denied!</h1>
      <p>You don't have permission to {{Request::segment(2)}} {{Request::segment(3)}} page.</p>
      <div class="sub">
         <p><a href="{{url('dashboard')}}">Back to Home</a></p>
      </div>
   </div>
</div>

<style type="text/css">
.wrap{
	margin:0 auto;
	width:100%;
}
.logo{
	margin-top:0px;
}	
.logo h1 {
    font-size: 40px;
    color: #8F8E8C;
    text-align: center;
    margin-bottom: 1px;
    text-shadow: 1px 1px 6px #fff;
    padding-top: 150px;
}	
.logo p{
	color:indianred;
	font-size:16px;
	margin-top:1px;
	text-align:center;
	font-weight: bold;
}	
.logo p span{
	color:lightgreen;
}	
.sub a{
	color:white;
	background:#8F8E8C;
	text-decoration:none;
	padding:7px 20px;
	font-size:13px;
	font-family: arial, serif;
	font-weight:bold;
	-webkit-border-radius:3em;
	-moz-border-radius:.1em;
	-border-radius:.1em;
}	
</style>
@endsection
