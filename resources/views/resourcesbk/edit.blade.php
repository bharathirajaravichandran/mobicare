@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Resources
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('resources') }}">Education / Resources</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="resources_form" id="resources_form" method="post" action="{{ url('resources/update') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{$resources->id}}">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Title</h5>
                                   <input type="text" name="title"  class="form-control" value="{{$resources->title}}" />
                               </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Description</h5>
                                  <textarea name="description" class="form-control">{{$resources->description}}</textarea>
                               </div>
                              </div>          
                               <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                    <select name="type" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($resources->type=='1') selected="" @endif>User</option>
                                      <option value="2" @if($resources->type=='2') selected="" @endif>Provider</option>
                                    </select>
                               </div>   
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select name="status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($resources->status=='1') selected="" @endif>Active</option>
                                      <option value="2" @if($resources->status=='2') selected="" @endif>Inactive</option>
                                    </select>
                               </div>                              
                              </div>   
                               <div class="row">                                 
                               <div class="col-sm-6 input_field_sections">
                                    <h5>File (can attach more than one)</h5>                                    
                                     <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                Browse… <input type="file" id="upload_files" name="upfile[]" multiple="">
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly> 
                                     </div>      
                                    <div style="color:blue;font-size: 11px;">Only (jpeg, jpg, png, gif, bmp, flv, mp4, doc, pdf) file types are allowed to upload.</div>                     
                                    <br/>
                                    <div id="placeimg">                                      
                                        @foreach($resources->resourcefiles as $k => $r)
                                            <?php 
                                            $ext = explode('.', $r->file);
                                            $fileName = $r->file;
                                            $changefile = preg_replace("/[\-_ .]/", "", $r->file);
                                            if($ext[1] == 'jpg' || $ext[1] == 'jpeg' || $ext[1] == 'png' || $ext[1] == 'gif') { 
                                            ?>
                                            <li id="{{$changefile}}" class="list-group-item list-img">
                                              <div class="checkbox">
                                                  <label for="checkbox2">
                                                      <a href="/storage/resources/{{$r->file}}" target="_blank"><img src="/storage/resources/{{$r->file}}" style="width:100px;height:100px;"></a>
                                                  </label>
                                              </div>
                                              <div class="pull-right action-buttons">                                            
                                                    <a href="javascript:" class="trash del" data-file="{{$fileName}}" data-changefile="{{$changefile}}"><span class="fa fa-trash"></span></a>                     
                                               </div>
                                            </li>  
                                            <?php } ?>

                                            <?php if($ext[1] == 'pdf') { ?> 
                                            <li id="{{$changefile}}" class="list-group-item list-pdf">
                                              <a href="/storage/resources/{{$r->file}}" target="_blank"><div class="checkbox">
                                                  <label for="checkbox2">
                                                      {{$r->original_file}}
                                                  </label>
                                              </div></a>
                                              <div class="pull-right action-buttons">                                            
                                                    <a href="javascript:" class="trash del" data-file="{{$fileName}}" data-changefile="{{$changefile}}"<span class="fa fa-trash"></span></a>                     
                                               </div>
                                            </li>  
                                            <?php } ?>

                                            <?php if($ext[1] == 'mp4') { ?> 
                                            <li id="{{$changefile}}" class="list-group-item list-mp4">
                                              <a href="/storage/resources/{{$r->file}}" target="_blank"><div class="checkbox">
                                                  <label for="checkbox2">
                                                      {{$r->original_file}}
                                                  </label>
                                              </div></a>
                                              <div class="pull-right action-buttons">                                            
                                                    <a href="javascript:" class="trash del" data-file="{{$fileName}}" data-changefile="{{$changefile}}"><span class="fa fa-trash"></span></a>                     
                                               </div>
                                            </li>  
                                            <?php } ?>

                                            <?php if($ext[1] == 'doc') { ?> 
                                            <li id="{{$changefile}}" class="list-group-item list-doc">
                                              <a href="/storage/resources/{{$r->file}}" target="_blank"><div class="checkbox">
                                                  <label for="checkbox2">
                                                      {{$r->original_file}}
                                                  </label>
                                              </div></a>
                                              <div class="pull-right action-buttons">                                            
                                                    <a href="javascript:" class="trash del" data-file="{{$fileName}}" data-changefile="{{$changefile}}"><span class="fa fa-trash"></span></a>                     
                                               </div>
                                            </li>  
                                            <?php } ?>
                                        @endforeach                                       
                                        <textarea style="display: none;" id="time" name="time"></textarea>         
                                    </div>
                               </div>                        
                              </div>   
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('resources') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 


<script>
$(document).ready(function(){

    // validate form on keyup and submit
    $("#resources_form").validate({
        rules: {
            title: {
                required: true,
                remote: {
                    url: "/resources/title/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            description: {
                required: true
            },
            type: {
                required: true
            },
            status: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please enter the title",
                remote: "Name already exists"
            },
            description: {
                required: "Please enter the description"
            },
            type: {
                required: "Please select the type"
            },
            status: {
                required: "Please select the status"
            }            
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "type") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });

    $('#upload_files').on('change',function(e){
        e.preventDefault();
        var extension = $('#upload_files').val().split('.').pop().toLowerCase();
        if ($.inArray(extension, ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'flv', 'mp4', 'doc', 'pdf']) == -1) {
            alert('Only (jpeg, jpg, png, gif, bmp, flv, mp4, doc, pdf) file types are allowed to upload.');
        } else {
            var form = $('form')[0]; 
            var formData = new FormData(form);
            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('section', 'general');
            formData.append('action', 'previewImg');

            // Attach file
            for (var i = 0, len = document.getElementById('upload_files').files.length; i < len; i++) {
                formData.append("file" + i, document.getElementById('upload_files').files[i]);
            }

            $.ajax({
                url: '/resources/storefiles',
                data: formData,
                type: 'POST',
                contentType: false, 
                processData: false, 
                beforeSend: function() {
                   //
                },
                success: function(response) {  
                  var returnedData = JSON.parse(response);
                  $('#time').append(returnedData.time);
                  $('#time').append(",");
                  $('#placeimg').append(returnedData.html);
                },
                error: function() {
                    //
                }
            });
        }
    });

    $(document).on('click', '.del', function () {
        var id = $(this).data("id");
        var file = $(this).data("file");
        $.ajax({
            url: '/resources/deletefileInedit',
            type: 'POST',
            data: {"_token": "{{ csrf_token() }}", id:id, file:file},
            success: function (data) {
                $(".list-group #"+id).remove();
            }
        });
    });

});
</script>

<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
    background: #00c0ef !important;
    color: white;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.trash { color:rgb(209, 91, 71); }
.flag { color:rgb(248, 148, 6); }
.panel-body { padding:0px; }
.panel-footer .pagination { margin: 0; }
.panel .glyphicon,.list-group-item .glyphicon { margin-right:5px; }
.panel-body .radio, .checkbox { display:inline-block;margin:0px; }
.list-group-item:hover, a.list-group-item:focus {text-decoration: none;background-color: rgb(245, 245, 245);}
.list-group { margin-bottom:0px; }
.list-group-item.list-pdf, .list-group-item.list-mp4, .list-group-item.list-doc { position: relative;  }
.list-group-item.list-pdf:before, .list-group-item.list-mp4:before, .list-group-item.list-doc:before {
    position: absolute;
    content: '\f1c1';
    top: 12px;
    left: 20px;
    font-size: 50px;
    width: 95px;
    height: 95px;
    font-family: fontawesome;
    color: #dd3d7d;
    border: 1px solid #ddd;
    text-align: center;
    background: #fff;
}
.list-group-item.list-pdf .checkbox, .list-group-item.list-mp4 .checkbox, .list-group-item.list-doc .checkbox  { padding-top: 101px;   }
.list-group-item.list-img .checkbox img { border: 1px solid #ddd;  }

.list-group-item.list-mp4:before { content: '\f1c8'; }
.list-group-item.list-doc:before { content: '\f15c'; }
.list-group.list-group-view-page .list-group-item  { width:100%; }
#placeimg  .panel-body .list-group { margin-left: 14px;  } 
#placeimg  .panel-body .list-group .list-group-item { border-radius: 0px !important;  }
</style>
@endsection
