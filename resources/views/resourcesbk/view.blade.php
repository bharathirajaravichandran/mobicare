@extends('layouts.admin')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Resources
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('resources') }}">Education / Resources</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Title</h5>
                                   <strong>{{$resources->title}}</strong>
                                     
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Description</h5>
                                 <strong>{{$resources->description}}</strong>
                                 </div>                                                                                         
                              </div>
                             <div class="row">
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                      <strong>@if($resources->type==1) User @else Provider @endif</strong>
                                   </div>  
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                       <strong>@if($resources->status==1) Active @else Inactive @endif</strong>
                                   </div>                                              
                            </div>
                            <div class="row">
                                <!-- <div class="col-sm-6 input_field_sections">
                                    <h5>Files</h5>
                                     @if(count($resources->resourcefiles)>0)
                                        <div>                                      
                                          <ul style="padding: 0px;">
                                            @foreach($resources->resourcefiles as $r)
                                            <li style="list-style-type:none;"><img src="{{ url('storage/'.$r->file) }}" style="width:80px; height: 70px;"></li>
                                            @endforeach
                                          </ul>                                      
                                        </div>
                                     @endif
                                   </div> -->
                                <ul class="list-group list-group-view-page" style="margin-left:14px; margin-top: 14px;flex-wrap: wrap;flex-direction: initial !important;    -ms-flex-direction: initial !important;">
                                @foreach($resources->resourcefiles as $k => $r)
                                  @php $ext = explode('.', $r->file); @endphp   

                                  @php if($ext[1] == 'jpg' || $ext[1] == 'jpeg' || $ext[1] == 'png' || $ext[1] == 'gif') { @endphp
                                  <li id="{{$k}}" class="list-group-item list-img">
                                    <div class="checkbox">
                                        <label for="checkbox2">
                                            <a href="/storage/resources/{{$r->file}}" target="_blank"><img src="/storage/resources/{{$r->file}}" style="width:100px;height:100px;"></a>
                                        </label>
                                    </div>
                                  </li>  
                                  @php } @endphp

                                  @php if($ext[1] == 'pdf') { @endphp
                                  <li id="{{$k}}" class="list-group-item list-pdf">
                                    <a href="/storage/resources/{{$r->file}}" target="_blank"><div class="checkbox">
                                        <label for="checkbox2">
                                            {{$r->original_file}}
                                        </label>
                                    </div></a>
                                  </li>  
                                  @php } @endphp

                                  @php if($ext[1] == 'mp4') { @endphp
                                  <li id="{{$k}}" class="list-group-item list-mp4">
                                    <a href="/storage/resources/{{$r->file}}" target="_blank"><div class="checkbox">
                                        <label for="checkbox2">
                                            {{$r->original_file}}
                                        </label>
                                    </div></a>
                                  </li>  
                                  @php } @endphp

                                  @php if($ext[1] == 'doc') { @endphp
                                  <li id="{{$k}}" class="list-group-item list-doc">
                                    <a href="/storage/resources/{{$r->file}}" target="_blank"><div class="checkbox">
                                        <label for="checkbox2">
                                            {{$r->original_file}}
                                        </label>
                                    </div></a>
                                  </li>  
                                  @php } @endphp

                                @endforeach
                                </ul>     
                               <!--  <div class="col-sm-6 input_field_sections">
                                    <h5>Created At</h5>
                                      <strong>{{$resources->created_at}}</strong>
                                   </div>      -->                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>   

<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
    background: #00c0ef !important;
    color: white;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.trash { color:rgb(209, 91, 71); }
.flag { color:rgb(248, 148, 6); }
.panel-body { padding:0px; }
.panel-footer .pagination { margin: 0; }
.panel .glyphicon,.list-group-item .glyphicon { margin-right:5px; }
.panel-body .radio, .checkbox { display:inline-block;margin:0px; }
.list-group-item:hover, a.list-group-item:focus {text-decoration: none;background-color: rgb(245, 245, 245);}
.list-group { margin-bottom:0px; }
.list-group-item.list-pdf, .list-group-item.list-mp4, .list-group-item.list-doc { position: relative;  }
.list-group-item.list-pdf:before, .list-group-item.list-mp4:before, .list-group-item.list-doc:before {
    position: absolute;
    content: '\f1c1';
    top: 12px;
    left: 20px;
    font-size: 50px;
    width: 95px;
    height: 95px;
    font-family: fontawesome;
    color: #dd3d7d;
    border: 1px solid #ddd;
    text-align: center;
    background: #fff;
}
.list-group-item.list-pdf .checkbox, .list-group-item.list-mp4 .checkbox, .list-group-item.list-doc .checkbox  { padding-top: 101px;   }
.list-group-item.list-img .checkbox img { border: 1px solid #ddd;  }

.list-group-item.list-mp4:before { content: '\f1c8'; }
.list-group-item.list-doc:before { content: '\f15c'; }
.list-group.list-group-view-page { flex-direction: none; flex-wrap: wrap;  }
.list-group.list-group-view-page .list-group-item {
    width: 100%;
}
</style>
@endsection
