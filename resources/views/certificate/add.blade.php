@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-plus"></i>
                       Add Certificate
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('certificate') }}">Certificate Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="certificate_form" id="certificate_form" method="post" action="{{ url('certificate/store') }}" accept-charset="UTF-8">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                               <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                   <input type="text" name="name" class="form-control" />
                                 </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Points</h5>
                                   <input type="text" name="points" class="form-control" min="1" max="100" />  
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Industry</h5>
                                     <select class="form-control" name="industry">
                                      <option value="">Select</option>
                                      @foreach($industry as $i)
                                      <option value="{{$i->id}}">{{$i->name}}</option>
                                      @endforeach
                                    </select>
                                 </div>
                                 <div class="col-sm-6 input_field_sections">                                    
                                    <h5>Status</h5>
                                    <select class="form-control" name="status">
                                     <option value="">-Select Status-</option>
                                     <option value="1" selected="">Active</option>
                                     <option value="0">Inactive</option>
                                    </select>
                                 </div>
                              </div>

                              <div class="row">
                                 <div class="col-sm-2 input_field_sections">
                                    <h5>Mandatory</h5>
                                    <input type="checkbox" name="is_mandatory" value="1" />  
                                 </div>
                                 <div class="col-sm-2 input_field_sections">
                                    <h5>Primary</h5>
                                    <input type="checkbox" name="is_primary" value="1" />  
                                 </div>
                                 <div class="col-sm-2 input_field_sections">
                                    <h5>Graduate</h5>
                                    <input type="checkbox" name="is_graduate" value="1" />  
                                 </div>
                              </div>

                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('certificate') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    // validate form on keyup and submit
    $("#certificate_form").validate({
        rules: {
            name: { 
                required: true,
                maxlength: 60
            },
            points: { 
                required: true,
                number: true
            },
            industry: { 
                required: true,
            },
            status: {
                required: true
            }
        },
        messages: {
            name: { 
                required: "Please enter the name",
            },
            points: { 
                required: "Please enter the points"
            },
            industry: { 
                required: "Please select the industry"
            },
            status: { 
                required: "Please select the status"
            }               
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "industry") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });
});
</script>      
@endsection
