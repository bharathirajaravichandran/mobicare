@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       View Certificate
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('certificate') }}">Certificate Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Name</h5>
                                   <strong>{{$docs->name}}</strong>
                                     
                               </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Percentage</h5>
                                 <strong>{{$docs->points}}</strong>
                                 </div>
                                 <div class="col-sm-3 input_field_sections">
                                    <h5>Industry</h5>
                                      <strong>{{$docs->industry->name}}</strong>
                                   </div>
                                 <div class="col-sm-3 input_field_sections">
                                    <h5>Status</h5>
                                       <strong>@if($docs->status==1) Active @else Inactive @endif</strong>
                                   </div>
                              </div>
                             <div class="row">
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Mandatory</h5>
                                      <strong>@if($docs->is_mandatory==1) Yes @else No @endif</strong>
                                   </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Primary</h5>
                                      <strong>@if($docs->is_primary==1) Yes @else No @endif</strong>
                                   </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Graduate</h5>
                                      <strong> @if($docs->is_graduate==1) Yes @else No @endif</strong>
                                   </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

          
           
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
