@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-plus"></i>
                       Add Sub Category
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('category') }}">Category Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="category_form" id="category_form" method="post" action="{{ url('category/subcategory/store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{Request::segment(3)}}">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                   <input type="text" name="subcat_name" class="form-control" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Image</h5>
                                   <input type="file" name="subcat_image" id="image" onchange="return imagecheck();" />
                                   <div id="image_err" style="color: crimson"></div>
                               </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select class="form-control" name="subcat_status">
                                     <option value="">-Select Status-</option>
                                     <option value="1" selected="">Active</option>
                                     <option value="0">Inactive</option>
                                    </select>
                                 </div>
                                 <div class="col-sm-2 input_field_sections">
                                    <h5>Parent Category</h5>
                                    <input type="checkbox" name="parent_category" id="parent_category" value="1" checked="true">
                               </div>
                               <div class="col-sm-3 input_field_sections">
                                     <select class="form-control" name="parent_cat" id="parent_cat" style="display: none;" required>
                                     <option value="">-Select-</option>
                                     @foreach($category as $c)
                                     <option value="{{$c->id}}" @if(Request::segment(2)==$c->id) selected @endif>{{$c->name}}</option>
                                     @endforeach
                                     </select>
                               </div>
                              </div>     
                               <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Minimum fee</h5>
                                   <input type="text" name="minimum_fees" class="form-control"/>
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Competent skills</h5>
                                   <input type="text" name="competent_skills" class="form-control" />
                                   <div style="color:blue">Press comma to enter multiple skills</div>
                               </div>
                              </div> 
                               <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>100pt ID check required</h5>
                                    <input type="radio" name="hundred_pt_check" id="100pt" value="1" onclick="return fn100pt(1);">&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="hundred_pt_check" id="100pt" value="0" onclick="return fn100pt(0);">&nbsp;&nbsp;&nbsp;No
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Police Check required</h5>
                                    <input type="radio" name="police_check" value="1">&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;
                                    <input type="radio" name="police_check" value="0">&nbsp;&nbsp;&nbsp;No
                               </div>
                              </div>  

                              <div id="100_pt_check" style="display: none;"><br/>
                                  <h5><b>100pt ID check</b></h5>
                                  <table style="width:90%;" class="table">
                                  <tr>
                                    <th>Name</th>
                                    <th>Primary</th>
                                    <th>Points</th>
                                    <th>Add Certificate</th>
                                  </tr>
                                  @foreach($view_document as $d)
                                  <tr>
                                    <td>{{$d->name}}</td>
                                     <td>
                                      @if($d->is_primary==1)
                                        Yes
                                      @else
                                        No
                                      @endif
                                    </td>
                                    <td>{{$d->points}}</td>
                                    <td><input type="checkbox" name="chk[]" value="{{$d->id}}"></td>
                                  </tr>
                                  @endforeach
                                </table>
                              </div>               
                           
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('category') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<style type="text/css">
  #hundred_pt_check-error { float: left; width: 100%; }
</style>

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    $('#parent_category').click(function(){
        if($(this).is(":checked")){
            $('#parent_category').val(1);
            $('#parent_cat').hide();
        }
        else if($(this).is(":not(:checked)")){
            $('#parent_category').val(0);
            $('#parent_cat').show();
        }
    });

    jQuery.validator.addMethod(
        "money",
        function(value, element) {
            var isValidMoney = /^\d{0,4}(\.\d{0,2})?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Please enter a valid minimum fee"
    );

    // validate form on keyup and submit
    $("#category_form").validate({
        rules: {
            subcat_name: { 
                required: true,
                remote: {
                    url: "/category/subcategory/name/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            minimum_fees: {
                required: true,
                money: true,
            },
            subcat_status: {
                required: true
            },
            hundred_pt_check: {
              required: true
            },
            'chk[]': { 
                required: true
            }
        },
        messages: {
            subcat_name: { 
                required: "Please enter the name",
                remote: "Name already exists"
            },
            minimum_fees: {
                required: 'Please enter the minimum fee',
            },
            subcat_status: { 
                required: "Please select the status"
            },
            hundred_pt_check: {
              required: "Please select the 100pt check"
            },
            'chk[]': { 
                required: "Please select your certificate to add"
            }                    
        }
    });
});

function fn100pt(val) {
  if(val==1)
    $('#100_pt_check').show();
  else
    $('#100_pt_check').hide();
}

function imagecheck() {
    var ext = $('#image').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
        $('#image_err').html("");
        return true;
    }
    else
    {
        $('#image').val('');
        $('#image_err').html("Only .jpeg, .jpg, .png formats are allowed");
    }
}
</script>      
@endsection
