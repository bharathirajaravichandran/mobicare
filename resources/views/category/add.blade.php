@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-plus"></i>
                       Add Category
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('category') }}">Category Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="category_form" id="category_form" method="post" action="{{ url('category/store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                   <input type="text" name="name" class="form-control" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Image</h5>
                                   <input type="file" name="image" id="image" onchange="return imagecheck();" />
                                   <div id="image_err" style="color: crimson"></div>
                               </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select class="form-control" name="status">
                                     <option value="">-Select Status-</option>
                                     <option value="1" selected="">Active</option>
                                     <option value="0">Inactive</option>
                                    </select>
                                 </div>
                              </div>                      
                           
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('category') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 5MB.');

    // validate form on keyup and submit
    $("#category_form").validate({
        rules: {
            name: { 
                required: true,
                maxlength: 50,
                remote: {
                    url: "/category/name/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            image: {
                required: true,
                filesize: 5000000,
            },
            status: {
                required: true
            }
        },
        messages: {
            name: { 
                required: "Please enter the name",
                remote: "Name already exists"
            },
            image: {
                required: "Please select the image"
            },
            status: { 
                required: "Please select the status"
            }               
        },
        submitHandler: function(form) {
         
            if(imagecheck()==false) {
                return false;
            } else {
                form.submit();
            }
         
        }
    });
});

function imagecheck() {
    var ext = $('#image').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
        $('#image_err').html("");
        return true;
    }
    else
    {
        $('#image_err').html("Only .jpeg, .jpg, .png formats are allowed");
        return false;
    }
}
</script>      
@endsection
