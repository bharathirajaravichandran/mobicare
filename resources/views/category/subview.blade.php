@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       View Subcategory
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('subcategory') }}">Subcategory Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Name</h5>
                                   <strong>{{$subcategory->name}}</strong>
                                     
                               </div>
                                
                                 <div class="col-sm-4 input_field_sections">
                                    <h5>Image</h5>
                                      @if(isset($subcategory->image) && $subcategory->image!='')
                                      <img src="{{ url('storage/subcategory/'.$subcategory->image) }}" style="width:50px;height: 60px;">
                                      @else
                                      --
                                      @endif 
                                   </div>
                                    <div class="col-sm-4 input_field_sections">
                                    <h5>Category</h5>
                                      <strong>{{ $subcategory->categoryname}}</strong>
                                   </div>                               
                              </div>
                              
                             <div class="row">      
                                    <div class="col-sm-4 input_field_sections">
                                    <h5>Minimum fees</h5>
                                      <strong>{{ isset($subcategory->minimum_fees)?$subcategory->minimum_fees:'-' }}</strong>
                                   </div>                          
                                   @if(count($subcategory->skills)>1)
                                   <div class="col-sm-4 input_field_sections">
                                    <h5>Competent skills</h5>
                                      @foreach($subcategory->skills as $val)
                                      <strong>{{ $val }}</strong><br>
                                      @endforeach
                                   </div>
                                   @endif
                                    <div class="col-sm-4 input_field_sections">
                                    <h5>100pt ID check required</h5>
                                      <strong>{{ isset($subcategory->hundred_pt_check) && $subcategory->hundred_pt_check == 1?'Yes':'No' }}</strong>
                                   </div>
                                   <div class="col-sm-4 input_field_sections">
                                    <h5>Police Check required</h5>
                                      <strong>{{ isset($subcategory->police_check) && $subcategory->police_check == 1?'Yes':'No' }}</strong>
                                   </div>
                                   <div class="col-sm-4 input_field_sections">
                                    <h5>Status</h5>
                                       <strong>@if($subcategory->status==1) Active @else Inactive @endif</strong>
                                   </div>
                              </div>
                             
                        </div>
                    </div>
                </div>
            </div>

          
           
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
