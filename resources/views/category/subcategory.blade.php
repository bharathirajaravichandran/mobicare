
<div class="row">
   <div class="col-sm-6 input_field_sections">
      <h5>Parent Category</h5>
     <select class="form-control" name="parent_cat" id="parent_cat" class="parent_cat">
       <option value="">-Select-</option>
       @foreach($category as $c)
       <option value="{{$c->id}}" {{ isset($subcategory) && $subcategory->parent_category == $c->id ?'selected':''}}>{{$c->name}}</option>
       @endforeach
       </select>
       <p class="error catexits"></p>
 </div>
  <div class="col-sm-6 input_field_sections">
      <h5>Name</h5>
     <input type="text" name="subcat_name" id="subcat_name" class="form-control" value="{{ isset($subcategory->name)?$subcategory->name:'' }}" />
     <p class="error subcatexits"></p>
 </div>
  
</div>
<div class="row">
  <div class="col-sm-6 input_field_sections">
      <h5>Image</h5>
     <input type="file" name="subcat_image" id="image" onchange="return imagecheck();" />
     <div id="image_err" style="color: crimson"></div>
 </div>
   <div class="col-sm-6 input_field_sections">
      <h5>Status</h5>
      <select class="form-control" name="subcat_status">
       <option value="">-Select Status-</option>
       <option value="1" {{ isset($subcategory) && $subcategory->status == 1 ?'selected':'selected'}}>Active</option>
       <option value="0" {{ isset($subcategory) && $subcategory->status == 0 ?'selected':''}}>Inactive</option>
      </select>
   </div>
</div>     
 <div class="row">
  <div class="col-sm-6 input_field_sections">
      <h5>Minimum fee</h5>
     <input type="text" name="minimum_fees" class="form-control" value="{{ isset($subcategory->minimum_fees)?$subcategory->minimum_fees:'' }}"/>
 </div>
 <?php
  if(isset($subcategory->competent_skills)){
    $skillslist = explode(',',$subcategory->competent_skills);
  }
 ?>
  <div class="col-sm-6 input_field_sections">
      <h5 id="compskills" style="display: none;">Competent skills</h5>
      <div class="skilldiv">
      @isset($skills)
      @foreach($skills as $skill)
        <input type="checkbox" name="competent_skills[]" value="{{ $skill->id}}" {{ isset($subcategory->competent_skills) && in_array($skill->id, $skillslist)?'checked':''  }}>{{ $skill->name}}<br>
      @endforeach
      @endif
    </div>
     <!--<input type="text" name="competent_skills" class="form-control" />
     <div style="color:blue">Press comma to enter multiple skills</div>-->
 </div>
</div> 
 <div class="row">
  <div class="col-sm-6 input_field_sections">
      <h5>100pt ID check required</h5>
      <input type="radio" name="hundred_pt_check" id="100pt" value="1" {{ isset($subcategory) && $subcategory->hundred_pt_check == 1 ?'checked':''}} onclick="return fn100pt(1);">&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;
      <input type="radio" name="hundred_pt_check" id="100pt" value="0" {{ isset($subcategory) && $subcategory->hundred_pt_check == 0 ?'checked':''}} onclick="return fn100pt(0);">&nbsp;&nbsp;&nbsp;No
 </div>
  <div class="col-sm-6 input_field_sections">
      <h5>Police Check required</h5>
      <input type="radio" name="police_check" value="1" {{ isset($subcategory) && $subcategory->police_check == 1 ?'checked':''}} >&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;
      <input type="radio" name="police_check" value="0" {{ isset($subcategory) && $subcategory->police_check == 0 ?'checked':''}} >&nbsp;&nbsp;&nbsp;No
 </div>
</div>  
<?php
if(isset($subcategory)){
 $file = $subcategory->certificates_uploaded; 
 $file = explode(',', $file); 
}
?>
<div id="100_pt_check" style="display: {{ isset($subcategory) && $subcategory->hundred_pt_check == 1 ?'block':'none'}};"><br/>
    <h5><b>100pt ID check</b></h5>
    <table style="width:90%;" class="table">
    <tr>
      <th>Name</th>
      <th>Primary</th>
      <th>Points</th>
      <th>Add Certificate</th>
    </tr>
    @foreach($view_document as $d)
    <tr>
      <td>{{$d->name}}</td>
       <td>
        @if($d->is_primary==1)
          Yes
        @else
          No
        @endif
      </td>
      <td>{{$d->points}}</td>
      <td><input type="checkbox" name="chk[]" value="{{$d->id}}" {{ isset($subcategory) && in_array($d->id, $file)? 'checked':''}}></td>
    </tr>
    @endforeach
  </table>
</div>               
                        
<style type="text/css">
  #hundred_pt_check-error { float: left; width: 100%; }
</style>

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });   

    jQuery.validator.addMethod(
        "money",
        function(value, element) {
            var isValidMoney = /^\d{0,6}(\.\d{0,2})?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Please enter a valid minimum fee"
    );

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 5MB.');

    // validate form on keyup and submit
    $("#category_form").validate({  
        rules: {
          parent_cat: {
                required: true,
            },
            subcat_name: { 
                required: true,
                maxlength: 50,
                remote: {
                    url: "/subcategory/name/check",
                    type: "get",
                    data: {
                        id: function(){return $('select[name=parent_cat]').val();},
                        hid_id: function(){return $('input[name=hid_id]').val();}
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            minimum_fees: {
                required: true,
                money: true,
            },
            subcat_status: {
                required: true
            },
            hundred_pt_check: {
              required: true
            },
            subcat_image: {
                required: false,
                filesize: 5000000,
            },
            'chk[]': { 
                required: true
            }
        },
        messages: {
            parent_cat: {
                required: "Please select parent category",
            },
            subcat_name: { 
                required: "Please enter the name",
                remote: "Name already exists"
            },
            minimum_fees: {
                required: 'Please enter the minimum fee',
            },
            subcat_status: { 
                required: "Please select the status"
            },
            hundred_pt_check: {
              required: "Please select the 100pt check"
            },
            'chk[]': { 
                required: "Please select your certificate to add"
            }                    
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "parent_cat") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });
});

function fn100pt(val) {
  if(val==1)
    $('#100_pt_check').show();
  else
    $('#100_pt_check').hide();
}

function imagecheck() {
    var ext = $('#image').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
        $('#image_err').html("");
        return true;
    }
    else
    {
        $('#image').val('');
        $('#image_err').html("Only .jpeg, .jpg, .png formats are allowed");
    }
}
</script>      

