    <!-- sidebar -->
    <!-- start sidebar menu -->
    @php $sidebar = checkSidebar(); @endphp
 <div id="left">
            <div class="menu_scroll">
                <div class="left_media">
                    <div class="media user-media">
                        <div class="user-media-toggleHover">
                            <span class="fa fa-user"></span>
                        </div>
                        <div class="user-wrapper">
                            <a class="user-link" href="#">
                                @if(isset(Auth::user()->photo) && Auth::user()->photo!='')
                                  <img src="{{ url('storage/user/'.Auth::user()->photo) }}" class="media-object img-thumbnail user-img rounded-circle admin_img3">
                                @else
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture"
                                     src="{{ asset('img/admin.jpg') }}">
                                @endif
                                <p class="user-info menu_hide">Welcome {{ucfirst(Auth::user()->first_name." ".Auth::user()->last_name)}}</p>
                            </a>
                        </div>
                    </div>
                    <hr/>
                </div>
                <ul id="menu">
                    @foreach($sidebar as $k=> $s)                        

                            @if( isset($s['subcategory']) && count($s['subcategory']) > 0)
                            <?php
                             $sublinks = array_column($s['subcategory'], 'link');
                            $linkurl1 = Request::segment(1);
                            $linkurl2 = (Request::segment(2))?'/'.Request::segment(2):'';
                            $linkurl = $linkurl1.$linkurl2;
                            ?>
                            <li class="dropdown_menu {{ in_array($linkurl1, $sublinks)?'active':''}}">
                                <a href="javascript:;">
                                    <i class="fa fa-cog"></i>
                                    <span class="link-title menu_hide">&nbsp;{{$s['module_name']}}</span>
                                    <span class="fa arrow menu_hide"></span>
                                </a>
                                <ul>
                                    @foreach($s['subcategory'] as $category)
                                    <li @if($category['link'] != '' && $linkurl1== $category['link'] ) class="active" @endif>
                                        <a href="{{asset($category['link'])}}">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp; {{ $category['module_name'] }}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @elseif( isset($s['scategory']) && count($s['scategory']) > 0)
                            <?php
                             $sublinks = array_column($s['scategory'], 'link');
                            $linkurl1 = Request::segment(1);
                            $linkurl2 = (Request::segment(2))?'/'.Request::segment(2):'';
                            $linkurl = $linkurl1.$linkurl2;
                            ?>
                            <li class="dropdown_menu {{ in_array($linkurl1, $sublinks)?'active':''}}">
                                <a href="javascript:;">
                                    <i class="fa fa-cog"></i>
                                    <span class="link-title menu_hide">&nbsp;Category</span>
                                    <span class="fa arrow menu_hide"></span>
                                </a>
                                <ul>
                                    @foreach($s['scategory'] as $category)
                                    <li @if($category['link'] != '' && $linkurl1== $category['link'] ) class="active" @endif>
                                        <a href="{{asset($category['link'])}}">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp; {{ $category['module_name'] }}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @else
                                @if(@$s['view'] == 1 || @$s['add'] == 1 || @$s['edit'] == 1 || @$s['delete'] == 1)
                                <li @if(Request::segment(1)==$s['link']) class="active" @endif>
                                    <a href="{{asset($s['link'])}}">
                                        <i class="{{$s['font_awesome_icon']}}"></i>
                                        <span class="link-title menu_hide">&nbsp;{{$s['module_name']}} </span>
                                    </a>
                                </li>  
                                @endif                          
                            @endif
        
                    @endforeach                    
                </ul>
                <!-- /#menu -->
            </div>
        </div>
   
            <!-- end sidebar menu -->