@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                        View Content
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('help') }}">Help Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">                        
                        <div class="card-body">
                            <div class="row">
                                 <div class="col-sm-5 input_field_sections">
                                      <h5>Type</h5>
                                     <strong>
                                       @if($help->type=='help')
                                       Help
                                       @else
                                       Pricing Guide
                                       @endif
                                     </strong>                                     
                                 </div>
                                 @if($help->title!='')
                                 <div class="col-sm-5 input_field_sections">
                                    <h5>Title</h5>
                                    <strong>{{$help->title}}</strong>
                                </div>  
                                @endif
                            </div>                     
                            <div class="row">
                                 <div class="col-sm-5 input_field_sections">
                                      <h5>Question</h5>
                                     <strong>{{$help->question}}</strong>                                     
                                 </div>
                                 <div class="col-sm-5 input_field_sections">
                                    <h5>Answer</h5>
                                    <strong>{!!base64_decode($help->answer)!!}</strong>
                                </div>  
                            </div>
                            <div class="row">                                   
                                 <div class="col-sm-5 input_field_sections">
                                     <h5>Date</h5>
                                     <strong>{{$help->created_at}}</strong>
                                  </div>     
                                  <div class="col-sm-5 input_field_sections">
                                     <h5>Status</h5>
                                     <strong> @if($help->status==1) Active @else Inactive @endif</strong>
                                  </div>                              
                            </div>              
                        </div>
                    </div>
                </div>
            </div>      
        </div>
        <!-- /.outer -->
    </div>    
@endsection
