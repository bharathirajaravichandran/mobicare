@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Care Request
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('care_request') }}">Care Request Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <h5>View Job</h5>
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                   <strong>{{$jobs->type}}</strong>
                                     
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Care Request Id</h5>
                                 <strong>{{$jobs->job_id}}</strong>
                                 </div>                                                                                         
                              </div>
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Category</h5>
                                   <strong>{{ isset($jobs->getcategory->name)?$jobs->getcategory->name:'-'}}</strong>
                                     
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Subcategory</h5>
                                 <strong>{{ isset($jobs->getsubcategory->name)?$jobs->getsubcategory->name:'-'}}</strong>
                                 </div>                                                                                         
                              </div>
                             <div class="row">
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                      <strong>{{$jobs->name}}</strong>
                                   </div>  
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Description</h5>
                                       <strong>{{$jobs->description}}</strong>
                                   </div>                                              
                            </div>
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Location</h5>
                                    <strong>{{$jobs->venue}}</strong>  
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Budget</h5>
                                      <strong>{{$jobs->budget}}</strong>
                                   </div>                       
                            </div>
                           <div class="row">
                                   <div class="col-sm-6 input_field_sections">
                                    <h5>Start Date & Time</h5>
                                       <strong>{{$jobs->start_date}} {{$jobs->start_time}}</strong>
                                   </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>End Date & Time</h5>
                                      <strong>{{$jobs->end_date}} {{$jobs->end_time}}</strong>
                                   </div>                                                     
                            </div>
                            <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                  <h5>Working with children required ?</h5>
                                    <strong>
                                      @if($jobs->work_with_children==1) Yes @else No @endif
                                    </strong><br/>
                                    @if(isset($jobs->job_file) && $jobs->job_file!='')
                                    <a href="{{ url('storage/jobs/'.$jobs->job_file) }}" target="_blank" style="color:crimson;">Show file</a>
                                    @endif
                                 </div>      
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                      <strong>@if($jobs->status==1) Active @else Inactive @endif</strong>
                                   </div>                                              
                            </div>
                            <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                  <h5>Request Type</h5>
                                    <strong>
                                        @if($jobs->request_type=='normal')
                                          Normal
                                        @elseif($jobs->request_type=='mark_it_urgent')
                                          Markit Urgent
                                        @elseif($jobs->request_type=='mobicare_match')
                                          Mobicare Match
                                        @endif                                    
                                    </strong><br/>
                                 </div>  
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Created At</h5>
                                      <strong>{{$jobs->created_at}}</strong>
                                </div>                                                           
                            </div>
                            <div class="row">
                                 <!-- <div class="col-sm-6 input_field_sections">
                                  <h5>Working days</h5>
                                    <strong>
                                      <?php 
                                        $days = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
                                        $working_days = isset($jobs->working_days)?$jobs->working_days:array();
                                      ?>
                                       @if(count($working_days)>0)
                                         @foreach($days as $key=>$day) 
                                            @if(in_array($key,$working_days))
                                            {{$day}},
                                            @endif
                                         @endforeach     
                                       @else
                                          ---
                                       @endif
                                    </strong><br/>
                                 </div>  -->                                                                                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br/>
            <h5>Job Assigend Details</h5>
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">                        
                             <table class="table">
                              <tr>
                                <th>Name of Provider</th>                        
                                <th>Date</th>
                                <th>No. of Bids</th>
                                <th>Amount</th>
                              </tr>
                              @if(count($jobs->job_assigned)>0)
                                @foreach($jobs->job_assigned as $val)
                                <tr>
                                  <td>{{getUserNameById($val->user_id)}}</td>                        
                                  <td>{{$val->hired_at}}</td>              
                                  <td>0</td>              
                                  <td>{{$val->amount}}</td>              
                                </tr>
                                @endforeach
                              @else
                                <tr>
                                  <td colspan="4">No jobs assigned.</td>
                                </tr>
                              @endif
                             </table>
                        </div>
                    </div>
                </div>
            </div>

           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
