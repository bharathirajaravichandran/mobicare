<div class="row">
<div class="col-sm-6 input_field_sections">
    <h5>Type</h5>
   <select name="type" class="form-control">
     <option value="">Select</option>
     <option value="Personal request" @if(isset($jobs->type) && $jobs->type=='Personal request') selected="" @endif>Personal request</option>
     <option value="Number" @if(isset($jobs->type) && $jobs->type=='Number') selected="" @endif>Number</option>
   </select>
</div>
<div class="col-sm-6 input_field_sections">
    <h5>Care Request Id</h5>
   <input type="text" name="job_id"  class="form-control" value="{{ isset($jobs->job_id)?$jobs->job_id:'CXV'.$job_id}}" readonly="" />
</div>
</div>
<div class="row">
   
   <div class="col-sm-6 input_field_sections">
      <h5>Category</h5>
       <select class="form-control" name="category_id" id="category" onchange="return getsubcat(this.value)" required="true">
        <option value="">Select</option>
        @foreach($industry as $i)
        <option value="{{$i->id}}" {{ isset($jobs->category_id) && $jobs->category_id == $i->id?'selected':'' }}>{{$i->name}}</option>
        @endforeach
      </select>
   </div>

   <div class="col-sm-6 input_field_sections">
      <h5>Subcategory</h5>
       <select class="form-control" name="subcategory_id" id="subcategory">
        <option value="">Select</option>
        @if(isset($subcategory))
        @foreach($subcategory as $sub)
        <option value="{{$sub->id}}" {{ isset($jobs->subcategory_id) && $jobs->subcategory_id == $sub->id?'selected':'' }}>{{$sub->name}}</option>
        @endforeach
        @endif
      </select>
   </div>
</div>
<div class="row">
 <div class="col-sm-6 input_field_sections">
    <h5>Name</h5>
   <input type="text" name="name"  class="form-control" value="{{ isset($jobs->name)?$jobs->name:'' }}" />
</div>
<div class="col-sm-6 input_field_sections">
    <h5>Description</h5>
   <textarea name="description" class="form-control">{{ isset($jobs->description)?$jobs->description:''}}</textarea>
</div>
</div>          
<div class="row">
 <div class="col-sm-6 input_field_sections">
    <h5>Location</h5>
   <input type="text" name="venue" class="form-control" value="{{ isset($jobs->venue)?$jobs->venue:''}}" />
</div>
<div class="col-sm-6 input_field_sections">
    <h5>Budget</h5>
   <input type="text" name="budget" class="form-control" value="{{ isset($jobs->budget)?$jobs->budget:''}}" />
</div>
</div>   
<div class="row">
 <div class="col-sm-6 input_field_sections">
    <h5>Start Date</h5>
   <input type="text" name="start_date" id="start_date" class="form-control" value="{{ isset($jobs->start_date)?date('m/d/Y', strtotime($jobs->start_date)):''}}" />
</div>
<div class="col-sm-6 input_field_sections">
    <h5>Start Time</h5>
   <input type="text" name="start_time" id="start_time" class="form-control" value="{{ isset($jobs->start_time)?$jobs->start_time:'' }}" />
</div>                             
</div>     
<div class="row">
  <div class="col-sm-6 input_field_sections">
    <h5>End Date</h5>
   <input type="text" name="end_date" id="end_date" class="form-control" value="{{ isset($jobs->end_date)?date('m/d/Y', strtotime($jobs->end_date)):''}}" />
</div>
<div class="col-sm-6 input_field_sections">
    <h5>End Time</h5>
   <input type="text" name="end_time" id="end_time" class="form-control" value="{{ isset($jobs->end_time)?$jobs->end_time:''}}" />
</div>
</div>
<div class="row">
 <div class="col-sm-6 input_field_sections">
    <h5>Working with Children required ?</h5>
    @if(Request::segment(2)=='edit')
      <input type="radio" name="work_with_children" id="work_with_children1" value="1" @if(isset($jobs->work_with_children) && $jobs->work_with_children=='1') checked @endif onclick="return fnWorkChildren(1)">&nbsp;&nbsp;Yes&nbsp;&nbsp;
      <input type="radio" name="work_with_children" id="work_with_children2" value="0" @if(isset($jobs->work_with_children) && $jobs->work_with_children=='0') checked @endif onclick="return fnWorkChildren(0)">&nbsp;&nbsp;No
    @else
      <input type="radio" name="work_with_children" id="work_with_children" value="1" onclick="return fnWorkChildren(1)">&nbsp;&nbsp;Yes&nbsp;&nbsp;
      <input type="radio" name="work_with_children" id="work_with_children" value="0" onclick="return fnWorkChildren(0)" checked>&nbsp;&nbsp;No
    @endif    
</div>
<?php $requesttype =array('normal'=>'Normal','mark_it_urgent'=>'Mark It Urgent','mobicare_match'=>'Mobicare Match'); ?>
<div class="col-sm-6 input_field_sections">
    <h5>Request type</h5>
     <select class="form-control" name="request_type">
     <option value="">-Select Request type-</option>
     @foreach($requesttype as $key=>$type)
     <option value="{{ $key }}" {{ isset($jobs->request_type) && $jobs->request_type==$key?'selected':''  }}>{{ $type }}</option>
     @endforeach
    </select>
</div>
</div>     

<div class="row" >
  <div class="col-sm-6 input_field_sections" id="card_upload" @if(isset($jobs->work_with_children) && $jobs->work_with_children==1) style="display: block;" @else style="display: none;" @endif>
  @if(isset($jobs->job_file) && $jobs->job_file!='')
     <h5>Blue Card</h5>
     <div class="input-group">
          <label class="input-group-btn">
              <span class="btn btn-primary">
                  Browse&hellip; <input type="file" name="job_file" id="job_file" onchange="return imagecheck();" style="display: none;">
              </span>
          </label>
          <input type="text" class="form-control" style="height: 38px;" readonly>          
     </div>
     <div style="color:blue;font-size: 12px;">Hint: Only .jpeg, .jpg, .png, .doc, .docx, .pdf formats are allowed</div>
     <a href="{{ url('storage/jobs/'.$jobs->job_file) }}" target="_blank" class="care-req-id">Show file</a>
 @else
     <h5>Blue Card</h5>
     <div class="input-group">
          <label class="input-group-btn">
              <span class="btn btn-primary">
                  Browse&hellip; <input type="file" name="job_file" id="job_file" onchange="return imagecheck();" style="display: none;">
              </span>
          </label>
          <input type="text" class="form-control" style="height: 38px;" readonly>
     </div>
     <div style="color:blue;font-size: 12px;">Hint: Only .jpeg, .jpg, .png, .doc, .docx, .pdf formats are allowed</div>
 @endif
</div>
 <div class="col-sm-6 input_field_sections">
    <h5>Status</h5>
     <select class="form-control" name="status">
     <option value="">-Select Status-</option>
     <option value="1" @if(isset($jobs->status) && $jobs->status=='1') selected="" @endif>Paid</option>
     <option value="2" @if(isset($jobs->status) && $jobs->status=='2') selected="" @endif>InEscrow</option>
     <option value="3" @if(isset($jobs->status) && $jobs->status=='3') selected="" @endif>Live</option>
    </select>
</div>
</div>   


<script type="text/javascript" src="{{asset('js/jquery.timepicker.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}" />
<script type="text/javascript">
  $('#start_time, #end_time').timepicker({ 'timeFormat': 'H:i', 'step': 15 });
</script>

<script>
$(document).ready(function() {

    $("#start_time, #end_time").keypress(function(event) {event.preventDefault();});

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    }); 


    $('#start_date, #end_date').datepicker({ 
        todayHighlight: true
    });

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    jQuery.validator.addMethod(
        "money",
        function(value, element) {
            var isValidMoney = /^\d{0,6}(\.\d{0,2})?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Please enter a valid minimum fee"
    );

    jQuery.validator.addMethod("greaterThanDate", function(value, element, params) {
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) >= new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val())); 
    },'End Date must be greater than Start Date.');


    jQuery.validator.addMethod("timeValidator",
       function (value, element, params) {

        var sdate = $('#start_date').val();
        var edate = $('#end_date').val();
        
        if(sdate==edate) {
           var val = new Date('1/1/1991' + ' ' + value);       
           var par = new Date('1/1/1991' + ' ' + $(params).val());
           if (!/Invalid|NaN/.test(new Date(val))) {
              return new Date(val) >= new Date(par);
           }
           return isNaN(val) && isNaN(par) || (Number(val) > Number(par));
        } else {
           return true;
        }
    }, 'End Time must be greater than Start Time.');


    // validate form on keyup and submit
    $("#care_request_form").validate({
        rules: {
            type: { 
                required: true
            },
            name: {
                required: true,
                maxlength: 60,
                remote: {
                    url: "/care_request/name/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            category_id: {
                required: true
            },
            description: {
                required: true
            },
            venue: {
                required: true
            },
            budget: {
                required: true,
                money: true
            },
            start_date: {
                required: true
            },
            start_time: {
                required: true
            },
            end_date: {
                required: true,
                greaterThanDate: "#start_date"
            },
            end_time: {
                required: true,
                timeValidator: "#start_time"
            },
            request_type: {
                required: true
            },
            status: {
                required: true
            }
        },
        messages: {
            type: { 
                required: "Please select the type"
            },
            name: {
                required: "Please enter the name",
                remote: "Name already exists"
            },
            category_id: {
                required: "Please select the category"
            },
            description: {
                required: "Please enter the description"
            },
            venue: {
                required: "Please enter the venue"
            },
            budget: {
                required: "Please enter the budget"
            },
            start_date: {
                required: "Please select start date"
            },
            start_time: {
                required: "Please select start time"
            },
            end_date: {
                required: "Please select end date"
            },
            end_time: {
                required: "Please select end time"
            },
            request_type: {
                required: "Please select the request type"
            },
            status: {
                required: "Please select the status"
            }            
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "type"||element.attr("name") == "category_id"||element.attr("name") == "request_type"||element.attr("name") == "status") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });
});

function fnWorkChildren(val) {
  if(val==1) {
    $('#card_upload').show();
    $('#work_with_children1').attr('checked',true);
    $('#work_with_children2').attr('checked',false);
  }
  else {
    $('#card_upload').hide();
    $("#job_file").val("");
    //$("#job_file").closest('form').trigger('reset');
    $('#work_with_children1').attr('checked',false);
    $('#work_with_children2').attr('checked',true);
  }
}

function imagecheck() {
    var ext = $('#job_file').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg"  || ext == "doc" || ext == "docx" || ext == "pdf") {        
        var oFile = document.getElementById("job_file").files[0];
        if (oFile.size > 5242880) // 5 mb for bytes.
        {
            alert("File size must under 5mb!");
            return false;
        }
        return true;
    }
    else
    {
        alert("Only .jpeg, .jpg, .png, .doc, .docx, .pdf formats are allowed");
        $('#job_file').val("");
        return false;
    }
}
</script>                    