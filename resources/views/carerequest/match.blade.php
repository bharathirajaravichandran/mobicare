@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-map-signs"></i>
                       Mobicare match
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('care_request') }}">Care Request Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Type</h5>
                                   <strong>{{$jobs->type}}</strong>
                                     
                               </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Care Request Id</h5>
                                 <strong>{{$jobs->job_id}}</strong>
                                 </div>  
                                 <div class="col-sm-3 input_field_sections">
                                    <h5>Category</h5>
                                   <strong>{{ isset($jobs->getcategory->name)?$jobs->getcategory->name:'-'}}</strong>
                                     
                               </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Subcategory</h5>
                                 <strong>{{ isset($jobs->getsubcategory->name)?$jobs->getsubcategory->name:'-'}}</strong>
                                 </div>                                                                                         
                                
                                   </div>
                            <div class="row">
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Name</h5>
                                      <strong>{{$jobs->name}}</strong>
                                   </div> 
                                   <div class="col-sm-3 input_field_sections">
                                    <h5>Description</h5>
                                       <strong>{{$jobs->description}}</strong>
                                   </div> 
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Location</h5>
                                    <strong>{{$jobs->venue}}</strong>  
                                   </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Budget</h5>
                                      <strong>{{$jobs->budget}}</strong>
                                   </div>   
                            </div>
                            <div class="row">                    
                                   <div class="col-sm-3 input_field_sections">
                                    <h5>Start Date & Time</h5>
                                       <strong>{{$jobs->start_date}} {{$jobs->start_time}}</strong>
                                   </div>
                                 <div class="col-sm-3 input_field_sections">
                                    <h5>End Date & Time</h5>
                                      <strong>{{$jobs->end_date}} {{$jobs->end_time}}</strong>
                                   </div>                                                     
                            
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Working with children required ?</h5>
                                    <strong>{{ ($jobs->work_with_children == 1)?'Yes':'No'}}</strong>  
                                   </div>
                                   <div class="col-sm-3 input_field_sections">
                                    <h5>Status</h5>
                                    <strong>{{ ($jobs->status == 1)?'Active':'Inactive'}}</strong>  
                                   </div>
                              </div>
                            


                        </div>
                        @if(isset($users) && !empty($users) && count($users) > 0)
                        <form action="{{ url('care_request/matchstore') }}" method="POST" name="mobicare_match" id="mobicare_match">
                          {{ csrf_field() }}
                          <input type="hidden" name="job_id" value="{{ $jobs->id }}">
                        <div class="card-body">
                            <div class="row col-md-12">
                              
                              <table class="table table-bordered">
                                  <thead>
                                      <th style="width:50px; text-align: center;">Select</th>
                                      <th>User</th>
                                  </thead>
                                  <tbody>
                                    @foreach($users as $user)
                                      <tr>
                                        <td style="width:50px; text-align: center;"><input type="checkbox" id="select" name="user_id[]"  value="{{ $user->id }}" {{ isset($suggestion) && in_array($user->id, $suggestion)?'checked':''}}></td>
                                        <td> 
                                          <strong>{{ $user->first_name.' '.$user->last_name}} </strong>({{ $user->user_types_id == '2'?'User':'Both' }}) <span class="pull-right">Rating : {{ isset($user->rating) && $user->rating!= ''? number_format($user->rating,1):0}}</span><br>
                                          <i class="fa fa-envelope"></i> <strong>{{ $user->email}}</strong><br>
                                          Category : <strong>{{ implode(' | ',$user->Category)}}</strong><br>
                                          Subcategory : <strong>{{ implode(' | ',$user->Subcategory)}}</strong>
                                        </td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                              </table>
                             
                          </div>
                        </div>

                        <div class="m-t-35">
                            <div class="form-actions form-group row">
                                <div class="col-xl-12 text-center">
                                   <input type="submit" class="btn btn-primary" value="Submit">
                                    <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('care_request') }}'">
                                </div>
                            </div>
                        </div>  
                      </form>
                      @endif
                    </div>
                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>  

<script type="text/javascript">
  $('#mobicare_match').submit(function(e){
    isvalid = 0;
    var len = $("[name='user_id[]']:checked").length;
    if(len == 0){
      alert('Please select users');
      isvalid = 0;
    }else if(len <= 5){
      isvalid = 1;
    }else{
      isvalid = 0;
      alert('You cannot add more than 5 users.');
    }
    if(isvalid == 1){
      return true;
    }else{
      e.preventDefault();
    }

  });
</script>
@endsection
