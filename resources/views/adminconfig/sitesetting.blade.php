@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-4 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-th"></i>
                        Site Settings
                    </h4>
                </div>
                <div class="col-lg-6 col-md-8 col-sm-8">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin/site_settings') }}">Site Settings</a>
                        </li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </header>

    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-12 data_tables">
                 
                    <!-- BEGIN EXAMPLE2 TABLE PORTLET-->
                    <div class="card">

                        <div class="card-body m-t-35">
                           <div class=" m-t-15">

                             <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th width="100">Site Name</th>
                                        <th>Phone</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE2 TABLE PORTLET-->
                   
                   
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
</div>
<!-- startsec End -->     

<script type="text/javascript">
$(document).ready(function() {
    var oTable = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth:false,
        aaSorting: [[0, 'desc']],
        ajax: {
            url: '{{ route('admin/site_settings/getdata') }}'
        },
        columns: [
            {data: 'site_name', name: 'site_name'},
            {data: 'phone', name: 'phone'},
            {data: 'mobile', name: 'mobile'},            
            {data: 'email', name: 'email'},
            {data: 'address', name: 'address'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });

    $('#search').on('click', function(e) {
        oTable.draw();
        e.preventDefault();
    });

    setTimeout(function() {
      $('.alert').fadeOut('slow');
    }, 3000); 
});
</script>  
@endsection
