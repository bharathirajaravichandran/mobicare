@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Admin and Permission
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin/users') }}">Admin and Permission</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <h5>User Details</h5>
                    <div class="card">                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>First Name</h5>
                                   <strong>{{$user->first_name}}</strong>
                                     
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Last Name</h5>
                                 <strong>{{$user->last_name}}</strong>
                                 </div>
                              </div>
                                <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Phone No</h5>
                                      <strong>{{$user->phone_country_code}} {{$user->phone}}</strong>
                                   </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                       <strong>{{$user->email}}</strong>
                                   </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                      <strong> @if($user->status==1) Active @else Inactive @endif</strong>
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Created Date</h5>
                                      <strong>{{$user->created_at}}</strong>
                                   </div>
                            </div>                           
                        </div>
                    </div>

                    <br/>

                    <h5>Permission Details</h5>
                    <div class="card">                        
                        <div class="card-body">
                            <table class="table">
                              <tr>
                                <th style="width: 230px;">Module</th>                        
                                <th>View</th>
                                <th>Add</th>
                                <th>Edit</th>
                                <th>Delete</th>
                              </tr>
                              @if(count($user_permission)>0)
                                @foreach($user_permission as $m)
                                @php $i=$loop->index; @endphp
                                <tr>
                                  <td>{{$m->module_name}}</td>                        
                                  <td>@if($m->view==1) <font color="green">Yes</font> @else <font color="crimson">No</font> @endif</td>
                                  <td>@if($m->add==1) <font color="green">Yes</font> @else <font color="crimson">No</font> @endif</td>
                                  <td>@if($m->edit==1) <font color="green">Yes</font> @else <font color="crimson">No</font> @endif</td>
                                  <td>@if($m->delete==1) <font color="green">Yes</font> @else <font color="crimson">No</font> @endif</td>
                                  <td></td>
                                </tr>
                                @endforeach
                              @endif
                            </table>      
                        </div>
                    </div>
                </div>
            </div>

          
           
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
