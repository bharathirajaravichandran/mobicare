@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Admin and Permission
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin/users') }}">Admin & Permissions</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="user_form" id="user_form" method="post" action="{{ url('admin/users/update') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{$user->id}}">
            <div class="row">
                <div class="col">
                    <h5>Edit Admin</h5>
                    <div class="card">                       
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>First Name</h5>
                                   <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Last Name</h5>
                                   <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}" />  
                                 </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-2 input_field_sections">
                                    <h5>Phone code</h5>
                                   <input type="text" name="phone_country_code" class="form-control" value="{{$user->phone_country_code}}" />
                               </div>
                               <div class="col-sm-4 input_field_sections">
                                    <h5>Phone number</h5>
                                   <input type="text" name="phone" class="form-control" value="{{$user->phone}}" />
                                   <div style="font-size: 11px; color:blue;">Phone number should be ten digits</div>
                               </div>
                              <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                   <input type="text" name="email" class="form-control" value="{{$user->email}}" />
                               </div>  
                              </div>
                              <div class="row">                             
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select name="status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($user->status==1) selected @endif>Active</option>
                                      <option value="0" @if($user->status==0) selected @endif>Inactive</option>
                                    </select>
                               </div>                                                    
                              </div>                               
                        </div>                        
                    </div>

                    <hr/><br/>
                    <?php //echo "<pre>"; print_r($user_permission); die;?>
                    <h5>Permissions</h5>
                     <div class="card">                       
                        <div class="card-body">
                            <table class="table">
                              <tr>
                                <th style="width: 230px;">Module</th>                        
                                <th>View</th>
                                <th>Add</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>Select All&nbsp;&nbsp;<input type="checkbox" id="selectAll">
                              </tr>
                              <?php /*@if(count($user_permission)>0)
                                @foreach($user_permission as $m)
                                @php $i=$loop->index; @endphp
                                <tr>
                                  <td>{{$m->module_name}}</td>                        
                                  <td><input type="hidden" name="module_id[]" value="{{$m->id}}">
                                    <input type="hidden" name="view[{{$i}}]" value=0>
                                    <input name="view[{{$i}}]" type="checkbox" @if($m->view==1) checked @endif value=1 class="checkBoxClass"></td>
                                  <td><input type="hidden" name="add[{{$i}}]" value=0>
                                    <input name="add[{{$i}}]" type="checkbox" @if($m->add==1) checked @endif value=1 class="checkBoxClass"></td>
                                  <td><input type="hidden" name="edit[{{$i}}]" value=0>
                                    <input name="edit[{{$i}}]" type="checkbox" @if($m->edit==1) checked @endif value=1 class="checkBoxClass"></td>
                                  <td><input type="hidden" name="delete[{{$i}}]" value=0>
                                    <input name="delete[{{$i}}]" type="checkbox" @if($m->delete==1) checked @endif value=1 class="checkBoxClass"></td>
                                  <td></td>
                                </tr>
                                @endforeach
                              @else*/ ?>
                                @foreach($modules as $m)
                                @php $i=$loop->index; @endphp
                                <tr>
                                  <td>{{$m->module_name}}</td>                        
                                  <td><input type="hidden" name="module_id[]" value="{{$m->id}}">
                                    <input type="hidden" name="view[{{$i}}]" value=0>
                                    <input name="view[{{$i}}]" type="checkbox" {{ isset($user_permission) && isset($user_permission[$m->id]) && $user_permission[$m->id]->view == 1 ?'checked':'no'}} value=1 class="checkBoxClass"></td>
                                  <td><input type="hidden" name="add[{{$i}}]" value=0>
                                    <input name="add[{{$i}}]" type="checkbox" {{ isset($user_permission) && isset($user_permission[$m->id]) && $user_permission[$m->id]->add == 1 ?'checked':'no'}} value=1 class="checkBoxClass"></td>
                                  <td><input type="hidden" name="edit[{{$i}}]" value=0>
                                    <input name="edit[{{$i}}]" type="checkbox" {{ isset($user_permission) && isset($user_permission[$m->id]) && $user_permission[$m->id]->edit == 1 ?'checked':'no'}} value=1 class="checkBoxClass"></td>
                                  <td><input type="hidden" name="delete[{{$i}}]" value=0>
                                    <input name="delete[{{$i}}]" type="checkbox" {{ isset($user_permission) && isset($user_permission[$m->id]) && $user_permission[$m->id]->delete == 1 ?'checked':'no'}} value=1 class="checkBoxClass"></td>
                                  <td></td>
                                </tr>
                                @endforeach
                             
                            </table>                    
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('admin/users') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $("#selectAll").click(function () {
        $(".checkBoxClass").attr('checked', this.checked);
    });

    // validate form on keyup and submit
    $("#user_form").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            gender: "required",  
            phone_country_code: {
                required: true,
                number: true,
                minlength: 1
            },
            phone: {
                required: true,
                minlength:9,
                maxlength:10,
                number: true,
                remote: {
                    url: "/users/phone/check",
                    type: "get",
                    data: {
                        phone: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: "/users/email/check",
                    type: "get",
                    data: {
                        email: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            status: "required", 
            'chk[]': "required"     
        },
        messages: {
            first_name: "Please enter the first name",
            last_name: "Please enter the last name",
            gender: "Please select the gender",      
            phone_country_code: {
                required: "Please enter the phone code"
            },
            phone: {
                required: "Please enter the phone number",
                remote: "Phone number already exists"
                //phoneAU: "Phone number is invalid"
            }, 
            email: {
                required: "Please enter a valid email address",
                remote: "Email already exists"
            },    
            status: "Please select the status"                
        }
    });
});
</script>      
@endsection
