@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Bank Accounts
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin/bank_accounts') }}">Bank Accounts</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Bank Name</h5>
                                   <strong>{{$bankaccounts->bank_name}}</strong>
                                     
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Display Name</h5>
                                 <strong>{{$bankaccounts->display_name}}</strong>
                                 </div>
                              </div>
                                <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Account No</h5>
                                      <strong>{{$bankaccounts->account_no}}</strong>
                                   </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>IFSC Code</h5>
                                       <strong>{{$bankaccounts->ifsc_code}}</strong>
                                   </div>
                            </div>
                             <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Branch Name</h5>
                                      <strong>{{$bankaccounts->branch_name}}</strong>
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Primary</h5>
                                      <strong>@if($bankaccounts->is_primary==1) Yes @else No @endif</strong>
                                   </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                      <strong> @if($bankaccounts->status==1) Active @else Inactive @endif</strong>
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Created Date</h5>
                                      <strong>{{$bankaccounts->created_at}}</strong>
                                   </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>

          
           
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
