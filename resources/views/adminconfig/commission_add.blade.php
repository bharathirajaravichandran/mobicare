@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-plus"></i>
                       Add Commission
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin/commission') }}">Commission</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="commission_form" id="commission_form" method="post" action="{{ url('admin/commission/store') }}" accept-charset="UTF-8">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                   <input type="text" name="name" class="form-control" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Commission (%)</h5>
                                   <input type="text" name="commission_value" class="form-control" min="0" max="100" />
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Commission Type</h5>
                                   <input type="text" name="commission_type" class="form-control" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                   <select class="form-control" name="status">
                                     <option value="">-Select Status-</option>
                                     <option value="1" selected="">Active</option>
                                     <option value="0">Inactive</option>
                                    </select>
                                </div>
                              </div>
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('admin/commission') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Accepts only letters"); 

    // validate form on keyup and submit
    $("#commission_form").validate({
        rules: {
            name: { 
                required: true,
                lettersonly: true,
                maxlength: 50,
                remote: {
                    url: "/admin/commission/name/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            commission_value: {
                required: true
            },
            commission_type: {
                required: true,
                maxlength: 40
            },
            status: {
                required: true
            }
        },
        messages: {
            name: { 
                required: "Please enter the name",
                remote: "Name already exists"
            },
            commission_value: { 
                required: "Please enter the commission value"
            },
            commission_type: { 
                required: "Please enter the commission type"
            },
            status: { 
                required: "Please select the status"
            }               
        }
    });
});
</script>      
@endsection
