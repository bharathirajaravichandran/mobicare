@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Bank Account
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin/bank_accounts') }}">Bank Accounts</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="bank_acc_form" id="bank_acc_form" method="post" action="{{ url('admin/bank_accounts/update') }}" accept-charset="UTF-8">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{ $bankaccounts->id }}">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Bank Name</h5>
                                   <input type="text" name="bank_name" class="form-control" value="{{ $bankaccounts->bank_name }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Display Name</h5>
                                   <input type="text" name="display_name" class="form-control" value="{{ $bankaccounts->display_name }}" />
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Account No.</h5>
                                   <input type="text" name="account_no" id="account_no" class="form-control" value="{{ $bankaccounts->account_no }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>IFSC Code</h5>
                                   <input type="text" name="ifsc_code" id="ifsc_code" class="form-control" value="{{ $bankaccounts->ifsc_code }}" />
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Branch Name</h5>
                                   <input type="text" name="branch_name" class="form-control" value="{{ $bankaccounts->branch_name }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Is Primary</h5>
                                   <input type="checkbox" name="is_primary" @if($bankaccounts->is_primary==1) checked value="1" @else $value="0" @endif />
                                </div>
                              </div>
                              
                              <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select class="form-control" name="status">
                                     <option value="">-Select Status-</option>
                                     <option value="1" @if($bankaccounts->status==1) selected @endif>Active</option>
                                     <option value="0" @if($bankaccounts->status==0) selected @endif>Inactive</option>
                                    </select>
                               </div>
                              </div>
                           
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('admin/bank_accounts') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $("#is_primary").change(function() {
        var ischecked= $(this).is(':checked');
        if(!ischecked) {
          $('#is_primary').val(0);
        } else {
          $('#is_primary').val(1);
        }          
    }); 

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Accepts only letters"); 

    jQuery.validator.addMethod("noSpace", function(value, element) { 
      return value.indexOf(" ") < 0 && value != ""; 
    }, "No space please and don't leave it empty");

    jQuery.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = value;

        if (!regex.test(key)) {
           return false;
        }
        return true;
    }, "Please use only alphanumeric or alphabetic characters");

    // validate form on keyup and submit
    $("#bank_acc_form").validate({
        rules: {
            bank_name: { 
                required: true,
                lettersonly: true,
                maxlength: 35,
                remote: {
                    url: "/admin/bank_accounts/name/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            display_name: {
                required: true,
                maxlength: 35,
            },
            account_no: {
                required: true,
                number: true,
                remote: {
                    url: "/admin/bank_accounts/number/check",
                    type: "get",
                    data: {
                        number: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            ifsc_code: {
                required: true,
                noSpace: true,
                specialChars: true
            },
            branch_name: {
                required: true,
                lettersonly: true,
                maxlength: 40,
            },
            status: {
                required: true
            }
        },
        messages: {
            bank_name: { 
                required: "Please enter the bank name",
                remote: "Bank name already exists"
            },
            display_name: { 
                required: "Please enter bank display name"
            },
            account_no: { 
                required: "Please enter the account number",
                remote: "Account number already exists"
            },
            ifsc_code: { 
                required: "Please enter the ifsc code"
            },
            branch_name: { 
                required: "Please enter the branch name"
            },
            status: { 
                required: "Please select the status"
            }               
        }
    });

    var minLength = 5;
    var maxLength = 30;
    $('#account_no').on('keydown keyup change', function(){
        var char = $(this).val();
        var charLength = $(this).val().length;
        if(charLength < minLength){
            $('#numval').text('Length is short, minimum '+minLength+' required.');
        }else if(charLength > maxLength){
            $('#numval').text('Length is not valid, maximum '+maxLength+' allowed.');
            $(this).val(char.substring(0, maxLength));
        }else{
            $('#numval').text('');
        }
    });

    var min = 5;
    var max = 20;
    $('#ifsc_code').on('keydown keyup change', function(){
        var char = $(this).val();
        var charLength = $(this).val().length;
        if(charLength < min){
            $('#nval').text('Length is short, minimum '+min+' required.');
        }else if(charLength > max){
            $('#nval').text('Length is not valid, maximum '+max+' allowed.');
            $(this).val(char.substring(0, max));
        }else{
            $('#nval').text('');
        }
    });
});
</script>      
@endsection
