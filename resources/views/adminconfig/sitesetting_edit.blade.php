@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Site Settings
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin/site_settings') }}">Site Settings</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="sitesetting_form" id="sitesetting_form" method="post" action="{{ url('admin/site_settings/update') }}" accept-charset="UTF-8">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{ $setting->id }}">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Site Name</h5>
                                   <input type="text" name="site_name" class="form-control" value="{{ $setting->site_name }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Phone</h5>
                                   <input type="text" name="phone" class="form-control" value="{{ $setting->phone }}" />
                                </div>
                              </div>

                               <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Mobile</h5>
                                   <input type="text" name="mobile" class="form-control" value="{{ $setting->mobile }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                   <input type="text" name="email" class="form-control" value="{{ $setting->email }}" />
                                </div>
                              </div> 

                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Alternate Email</h5>
                                   <input type="text" name="alter_email" class="form-control" value="{{ $setting->alter_email }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Copyrights</h5>
                                   <input type="text" name="copyright" class="form-control" value="{{ $setting->copyright }}" />
                                </div>
                              </div> 

                               <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Pin payment Secret Key</h5>
                                   <input type="text" name="pin_payment_secret" class="form-control" value="{{ $setting->pin_payment_secret }}" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Address</h5>
                                   <textarea name="address" id="address" class="form-control">{{$setting->address}}</textarea>
                                </div>
                              </div>

                               <div class="row">                                
                                <div class="col-sm-12 input_field_sections">
                                    <h5>Subscription Benefits</h5>
                                   <textarea name="subscription_benefits" id="subscription_benefits" class="form-control">{!! $setting->subscription_benefits !!}</textarea>
                                </div>
                              </div>
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('admin/site_settings') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>

<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
var roxyFileman = '/fileman/index.html?integration=ckeditor';
$(function(){
  //CKEDITOR.replace( 'address');
  CKEDITOR.replace( 'subscription_benefits',{filebrowserBrowseUrl:roxyFileman, 
                               filebrowserImageBrowseUrl:roxyFileman+'&type=image',
                               removeDialogTabs: 'link:upload;image:upload'});
});

$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    $.validator.methods.email = function( value, element ) {
        var email = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return email.test(value);
    }

    jQuery.validator.addMethod("alternateemail", function(value, element) {
      return this.optional(element) || /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value);
    }, "Please enter a valid email address");

    // validate form on keyup and submit
    $("#sitesetting_form").validate({
        rules: {
            site_name: {
              required: true,
              maxlength: 50
            },           
            phone: {
                required: true,
                minlength:5,
                maxlength:20,
                number: true
            },
            mobile: {
                required: true,
                minlength:9,
                maxlength:10,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            alter_email: {
                required: false,
                alternateemail: true
            }
        },
        messages: {
            site_name: {
              required: "Please enter the site name"
            },           
            phone: {
                required: "Please enter the phone number"
            },
            mobile: {
                required: "Please enter the mobile number"
            },
            email: {
                required: "Please enter the email address",
                email: "Please enter a valid email address"
            }
        }
    });
});
</script>
@endsection
