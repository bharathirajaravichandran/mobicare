@extends('layouts.adminlogin')

@section('content')
<div class="container">    
    <div class="outer" style="width: 50%;margin-left: 270px;margin-top: 130px;">
          <h3 style="text-align: center; margin-bottom: 20px;">Mobicare Login</h4>
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <div class="inner bg-container forms">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12 input_field_sections">
                                            <h5>Email</h5>
                                              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif

                                                @if(Session::has('message'))
                                                    <span class="help-block">
                                                        <strong>{{ Session::get('message') }}</strong>
                                                    </span>
                                                @endif
                                        </div>                                       
                                    </div>
                                    <div class="row">                                        
                                         <div class="col-sm-12 input_field_sections">
                                            <h5>Password</h5>
                                            <input id="password" type="password" class="form-control" name="password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">                                        
                                         <div class="col-sm-12 input_field_sections text-center">
                                            <input type="submit" class="btn btn-primary" value="Login">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                   
                </div>
                </form>
                <!-- /.outer -->
            </div>

</div>
@endsection
