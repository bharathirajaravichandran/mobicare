@php
if(isset($subscription))
  $idval = $subscription->subscription_id;
else
  $idval = 'PLAN'.$subscription_id;
@endphp
<div class="row">
  <div class="col-sm-6 input_field_sections">
      <h5>Subscription plan Id</h5>
     <input type="text" name="subscription_id"  class="form-control" value="{{$idval}}" readonly="" />
 </div>
 <div class="col-sm-6 input_field_sections">
      <h5>Name</h5>
     <input type="text" name="name"  class="form-control" value="{{ isset($subscription->name)?$subscription->name:''}}" />
 </div>
</div>
<div class="row">
 <div class="col-sm-6 input_field_sections">
      <h5>Description</h5>
     <textarea name="description" class="form-control">{{ isset($subscription->description)?$subscription->description:''}}</textarea>
 </div>
 @php
  $duration = array('1'=>'1 Month','2'=>'2 Months','3'=>'3 Months','6'=>'6 Months','12'=>'1 year','24'=>'2 years')
 @endphp
 <div class="col-sm-6 input_field_sections">
      <h5>Validity</h5>
      <select class="form-control" name="duration">
       <option value="">-Select validity-</option>
       @foreach($duration as $key=>$val)
        <option value="{{ $key }}" {{ isset($subscription->duration) && $subscription->duration == $key?'selected':''}}>{{$val}}</option>
       @endforeach
     </select>
 </div>
</div>          
 <div class="row">
   <div class="col-sm-6 input_field_sections">
      <h5>Amount</h5>
     <input type="text" name="amount" class="form-control" value="{{ isset($subscription->amount)?$subscription->amount:''}}" />
 </div>
<div class="col-sm-6 input_field_sections">
      <h5>Status</h5>
       <select class="form-control" name="status">
       <option value="">-Select Status-</option>
       <option value="1" {{ isset($subscription->status) && $subscription->status == '1'?'selected':''}}>Active</option>
       <option value="0" {{ isset($subscription->status) && $subscription->status == '0'?'selected':''}}>Inactive</option>
      </select>
 </div>
</div>   
 
<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    jQuery.validator.addMethod(
        "money",
        function(value, element) {
            var isValidMoney = /^\d{0,6}(\.\d{0,2})?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Please enter a valid minimum fee"
    );

    jQuery.validator.addMethod("lettersonly", function(value, element) {
       return this.optional(element) || /^[a-z\s]+$/i.test(value);
    });

    // validate form on keyup and submit
    $("#subscription_form").validate({
        rules: {
            type: { 
                required: true
            },
            name: {
                required: true,
                lettersonly: true,
                maxlength: 30,
                remote: {
                    url: "/subscription/name/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            description: {
                required: true
            },
            duration: {
                required: true
            },
            amount: {
                required: true,
                money: true
            },
            status: {
                required: true
            }
        },
        messages: {
            type: { 
                required: "Please select the type"
            },
            name: {
                required: "Please enter the name",
                lettersonly: "Please enter characters only",
                remote: "Name already exists"
            },
            description: {
                required: "Please enter the description"
            },
            duration: {
                required: "Please select the duration"
            },
            amount: {
                required: "Please enter the amount"
            },
            status: {
                required: "Please select the status"
            }            
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "duration"||element.attr("name") == "status") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });
});

function fnWorkChildren(val) {
  if(val==1)
    $('#card_upload').show();
  else
    $('#card_upload').hide();
}
</script>                         