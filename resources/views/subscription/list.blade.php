@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-4 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-th"></i>
                        Subscription plan Management
                    </h4>
                </div>
                <div class="col-lg-6 col-md-8 col-sm-8">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('subscription') }}">Subscription plan Management</a>
                        </li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </header>

    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-12 data_tables">
                 
                    <!-- BEGIN EXAMPLE2 TABLE PORTLET-->
                    <div class="card">
                        
                        <div class="card-body m-t-35">
                            <div class="row">
                                <div class="col-sm-2">  
                                    <div class="btn-group show-hide">
                                      @php $access = checkAdminPermission(); if(in_array('add', $access)) { @endphp
                                      <a class="btn btn-primary" href="{{url('subscription/add')}}"> Add New </a>
                                      @php } @endphp
                                      
                                    </div>
                                </div>                                
                                
                                <!--<div class="col-sm-3 ">
                                    <select class="form-control input-sm" name="status" id="status" onchange="$('.datatable').DataTable().draw()">
                                        <option value="">Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <button type="submit" id="search" class="btn btn-primary">Search</button>-->
                            </div>

                           @if(in_array('view', $access)||in_array('edit', $access)||in_array('delete', $access))   
                           <div class=" m-t-15">
                              <form name="list" id="list" method="post">
                                {{ csrf_field() }} 
                                <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Subscription plan ID</th>
                                            <th width="100">Plan Name</th>
                                            <th width="100">Description</th>
                                            <th>Duration</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                     @php
                                      $duration = array('1'=>'1 Month','2'=>'2 Months','3'=>'3 Months','6'=>'6 Months','12'=>'1 year','24'=>'2 years');
                                      $action = '';
                                      $access = checkAdminPermission();      
                                     @endphp
                                    <tbody>
                                        @foreach($subscription as $key => $val)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $val->subscription_id }}</td>
                                            <td>{{ $val->name }}</td>
                                            <td>{{ str_limit($val->description, $limit = 100, $end = '...') }}</td>
                                            <td>{{ $duration[$val->duration] }}</td>
                                            <td>${{ $val->amount }}</td>
                                            <td><?php echo ($val->status==1)?'<span class="label label-default">Active</span>':'<span class="label label-default">Inactive</span>' ;?></td>
                                            <td>
                                                @if(in_array('edit', $access)) 
                                                <a href="subscription/edit/{{$val->id}}" class="btn btn-primary btn-xs" title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                @endif
                                                
                                                @if(in_array('view', $access)) 
                                                <a href="subscription/view/{{$val->id}}" class="btn btn-info btn-xs" title="View">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @endif

                                                @if(in_array('delete', $access)) 
                                                <a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal({{$val->id}});"><i class="fa fa-trash-o "></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                              </form>

                        </div>
                        @endif
                        </div>
                    </div>
                    <!-- END EXAMPLE2 TABLE PORTLET-->
                   
                   
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
</div>

<form name="deleteForm" id="deleteForm" method="post">
     {{ csrf_field() }} 
     <input type="hidden" name="hid_id" id="hid_id">
<div id="deleteModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="material-icons">&#xE5CD;</i>
                </div>              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger" onclick="return fnDelete();">Delete</button>
            </div>
        </div>
    </div>
</div>  
</form> 


<!-- startsec End -->     

<script type="text/javascript">
$(document).ready(function() {
    var oTable = $('.datatable').DataTable({
        ordering: true,
        buttons: ['csv'],
    });    

    $('#search').on('click', function(e) {
        oTable.draw();
        e.preventDefault();
    });

    setTimeout(function() {
      $('.alert').fadeOut('slow');
    }, 3000); 
});

var url = "{{ url('') }}";
function showDeleteModal(id)
{
    $('#deleteForm #hid_id').val(id);
}

function fnDelete() 
{
    var id = $('#deleteForm #hid_id').val();
    $('form[name=deleteForm]').attr('action',url+'/subscription/delete/'+id);
    $('form[name=deleteForm]').submit();
}
</script>  
@endsection
