@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Subscription plan
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('subscription') }}">Subscription plan Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Subscription plan Id</h5>
                                 <strong>{{$subscription->subscription_id}}</strong>
                                 </div>     
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                      <strong>{{$subscription->name}}</strong>
                                   </div>                                                                                     
                              </div>
                             <div class="row">
                                <div class="col-sm-12 input_field_sections">
                                    <h5>Description</h5>
                                       <strong>{{$subscription->description}}</strong>
                                   </div>                                              
                            </div>
@php
  $duration = array('1'=>'1 Month','2'=>'2 Months','3'=>'3 Months','6'=>'6 Months','12'=>'1 year','24'=>'2 years')
 @endphp
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Duration</h5>
                                    <strong>{{ $duration[$subscription->duration] }}</strong>  
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Amount</h5>
                                      <strong>${{$subscription->amount}}</strong>
                                   </div>                       
                            </div>
                          
                            <div class="row">
                                   
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                      <strong>@if($subscription->status==1) Active @else Inactive @endif</strong>
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Created At</h5>
                                      <strong>{{$subscription->created_at}}</strong>
                                   </div>                      
                            </div>


                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
