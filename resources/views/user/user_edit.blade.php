@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit User
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('users') }}">Users</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="customer_form" id="customer_form" method="post" action="{{ url('users/update') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{ $user->id }}">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>First Name</h5>
                                   <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Last Name</h5>
                                   <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}" />  
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Gender</h5>
                                   <input type="radio" name="gender" value="Male" @if($user->gender=='Male') checked @endif>Male
                                   <input type="radio" name="gender" value="Female" @if($user->gender=='Female') checked @endif>Female
                                   <input type="radio" name="gender" value="Trans" @if($user->gender=='Trans') checked @endif>Trans
                                 </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Photo</h5>
                                   <input type="file" name="photo" id="photo" onchange="return imagecheck();" />  
                                   <div id="photo_err" style="color:crimson;"></div>
                                   @if(isset($user->photo) && $user->photo!='')
                                   <img src="{{ url('storage/user/'.$user->photo) }}" style="width:50px; height: 60px; margin-top: 10px;">
                                   @endif
                                 </div>
                                  <div class="col-sm-3 input_field_sections">
                                    <h5>Video</h5>
                                   <input type="file" name="video" id="video" onchange="return videocheck();" />  <br/> <br/>
                                   @if(isset($user->video) && $user->video!='')
                                   <a href="{{ url('storage/user/video/'.$user->video) }}" target="_blank" class="btn btn-success btn-sm">Show video</a>
                                   @endif
                                   <div id="video_err" style="color:crimson;"></div>
                                 </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Age</h5>
                                   <input type="text" name="age" class="form-control" value="{{$user->age}}" />
                               </div>
                                <div class="col-sm-2 input_field_sections">
                                    <h5>Phone code</h5>
                                   <select name="phone_country_code" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($countries as $c)
                                      <option value="{{$c->phonecode}}" @if($user->phone_country_code==$c->phonecode) selected @endif>{{$c->phonecode}}</option>
                                      @endforeach
                                    </select>
                               </div>
                               <div class="col-sm-4 input_field_sections">
                                    <h5>Phone number</h5>
                                   <input type="text" name="phone" class="form-control" value="{{$user->phone}}" />
                                   <div style="font-size: 11px; color:blue;">Phone number should be ten digits</div>
                               </div>
                               
                              </div>
                           
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                   <input type="text" name="email" class="form-control" value="{{$user->email}}" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                    <select name="type" id="type" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($usertypes as $u)
                                      <option value="{{$u->id}}" @if($user->user_types_id==$u->id) selected @endif>{{$u->name}}</option>
                                      @endforeach
                                    </select>
                               </div>                                                  
                              </div>                               
                              <div class="row" id="user">  
                                   <div class="col-sm-6 input_field_sections">
                                    <h5>Health Condition</h5>
                                    <select name="health_condition" class="form-control">
                                      <option value="">Select</option>
                                      <option value="Diabetic" @if($user->health_condition=='Diabetic') selected @endif>Diabetic</option>
                                      <option value="Heart issues" @if($user->health_condition=='Heart issues') selected @endif>Heart issues</option>
                                    </select>
                                 </div>   
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Join Date</h5>
                                    <input type="text" name="join_date" id="join_date" class="form-control startdate" value="{{date('m/d/Y')}}" />
                                    <div class="error" id="joindate_err"></div>
                               </div>     
                              </div>
                              <div class="row">   
                               <div class="col-sm-6 input_field_sections">
                                   <!--  <h5>Location</h5>
                                    <input type="text" name="location" class="form-control" value="{{$user->location}}" /> -->

                                <div class="col-sm-6 input_field_sections">
                                    <label for="address_address">Address</label>
                                    <input type="text" id="address-input" name="location" value="{{$user->location}}" class="form-control map-input">
                                    <input type="hidden" name="address_latitude" id="address-latitude" value="{{$user->latitude}}"/>
                                    <input type="hidden" name="address_longitude" id="address-longitude" value="{{$user->longitude}}"/>
                                </div> 

                                <div id="address-map-container" style="width:100%;height:400px;">
                                    <div style="width: 100%; height: 100%" id="address-map"></div>
                                </div>

                               </div>                             
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Blocked status</h5>
                                    <select name="blocked_status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($user->blocked_status==1) selected @endif>Blocked</option>
                                      <option value="0" @if($user->blocked_status==0) selected @endif>No</option>
                                    </select>
                                </div>   
                              </div>   
                              <?php 
                                $categoryArray = $subcategoryArray = array();
                                if(isset($user->category) && $user->category != ''){
                                  $categoryArray = explode(',',$user->category);
                                }
                                if(isset($user->subcategory) && $user->subcategory != ''){
                                  $subcategoryArray = explode(',',$user->subcategory);
                                }
                              ?>

                              <div class="row" id="provider_or_both" @if($user->user_types_id==2) style="display: none;" @endif>  
                                 <!--  <div class="col-sm-6 input_field_sections">
                                      <h5>Verifications</h5>
                                      <a class="btn btn-primary btn-sm" data-toggle="modal" href="#100pt_check">100pt ID Check</a>
                                  </div> -->

                              <div class="col-sm-6 input_field_sections">
                                <h5>Working Days </h5>
                                  <?php 
                                 $days = array('0'=>'Sunday','1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday');
                                    $availability = isset($user->availability)? $user->availability:array();
                                  ?>
                                   <select class="form-control" name="availability[]" multiple="multiple">
                                     @foreach($days as $key=>$day) 
                                        <option value="{{$key}}" @if(in_array($key,$availability)) selected @endif>{{$day}}</option>
                                     @endforeach     
                                  </select>
                              </div>


                                {{--  <div class="col-sm-6 input_field_sections">
                                       <h5>Category</h5>
                                       <select name="industry[]" multiple="true" class="form-control select2" onchange="return getsubcategory(this.value);">
                                         @foreach($category as $c)
                                         @if(isset($subcategory[$c->id]))
                                            <optgroup label="{{$c->name}}">
                                              @foreach($subcategory[$c->id] as $parent => $sub)
                                                <option value="{{ $c->id.'_'.$sub->id }}" {{ (isset($user->subcategory) && in_array($sub->id, $subcategoryArray))?'selected':'' }}>{{$sub->name}}</option>
                                                @endforeach
                                            </optgroup>
                                         @else
                                         <option value="{{$c->id}}" {{ (isset($user->category) && in_array($c->id, $categoryArray))?'selected':'' }}>{{$c->name}}</option>
                                         @endif
                                         @endforeach
                                       </select>
                                  </div> --}}

                                  <!--<div class="col-sm-6 input_field_sections">
                                       <h5>Subcategory</h5>
                                       <div id="subcat_new"></div>
                                       <select name="subcategory" id="subcat_old" class="form-control">
                                         <option value="">Select</option>
                                         @foreach($category as $sc)
                                         <option value="{{$sc->id}}" @if($user->subcategory==$sc->id) selected @endif>{{$sc->name}}</option>
                                         @endforeach
                                       </select>
                                  </div>-->
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Fee</h5>
                                       <input type="text" class="form-control" name="fee" value="{{$user->fee}}">
                                  </div>   
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Description</h5>
                                       <textarea name="description" class="form-control">{{$user->description}}</textarea>
                                  </div>   
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Experience</h5>
                                       <input type="text" class="form-control" name="experience" value="{{$user->experience}}">
                                  </div> 

                                <div class="col-sm-6 input_field_sections">
                                    <h5>Subscription plan</h5>
                                    <select name="subscription_id" class="form-control" {{ isset($user->subscription_id)?'readonly=true':''}}>
                                      <option value="">Select</option>
                                      @foreach($subscription as $plan_id=>$name)
                                      <option value="{{$plan_id}}" {{ isset($user->subscription_id) && $user->subscription_id==$plan_id ?'selected':'' }} >{{$name}}</option>
                                      @endforeach
                                    </select>
                               </div>  
                               @if($user->subscription_id != '')
                                <div class="col-sm-6 input_field_sections">
                                  <h5>Plan Start date</h5>
                                    <input type="text" class="form-control" value="{{ isset($plan->start_date)?date('d-m-Y',strtotime($plan->start_date)):'' }}" readonly="true" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                  <h5>Plan End date</h5>
                                    <input type="text" class="form-control" value="{{ isset($plan->end_date)?date('d-m-Y',strtotime($plan->end_date)):'' }}" readonly="true" />
                                </div>
                                <div class="col-sm-6 input_field_sections">
                                  <h5>Amount paid</h5>
                                    <input type="text" class="form-control" value="{{ isset($plan->amount)?$plan->amount:'' }}" readonly="true" />
                                </div>
                                @endif
                              </div>
               
                              
                                                        
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select name="status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($user->status==1) selected @endif>Active</option>
                                      <option value="0" @if($user->status==0) selected @endif>Inactive</option>
                                    </select>
                               </div>  
                              </div>

                        </div>                         
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('users') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>

            {{--<div id="100pt_check" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">        
                    <h4 class="modal-title">View Document</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <table style="width:100%;">
                      <tr>
                        <th>Name</th>
                        <th>Action</th>
                      </tr>
                      @foreach($view_document as $d)
                      <tr>
                        <td>{{$d->documents->name}}</td>
                        <td><a href="{{ url('storage/certificates/'.$d->file_path) }}" target="_blank" style="color:crimson;">Download</a></td>
                      </tr>
                      @endforeach
                    </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>--}}

            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $( "#join_date" ).change(function() {
        var currentYear = (new Date).getFullYear();
        currentYear = currentYear-1;
        var value = $(this).val();
        var date = value.split("/");
        if(date[2]<currentYear) {
            $('#joindate_err').html('Join date should be above ' + currentYear + ' year.');
        } else {
            $('#joindate_err').html('');
        }
    });

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    $("#type").change(function(){
        var val = this.value;
        if(val==3) {
          $('#provider_or_both').show();
          $('#user').hide();
        }
        else if(val==4) {
          $('#provider_or_both').show();
          $('#user').hide();
        }
        else {
          $('#provider_or_both').hide();
          $('#user').show();
        }
    });

    $.validator.methods.email = function( value, element ) {
        var email = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return email.test(value);
    }

    jQuery.validator.addMethod("lettersonly", function(value, element) {
       return this.optional(element) || /^[a-z\s]+$/i.test(value);
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 5MB.');

    // validate form on keyup and submit
    $("#customer_form").validate({
        rules: {
            first_name: {
              required: true,
              lettersonly: true,
              maxlength: 40
            },
            last_name: {
              required: true,
              lettersonly: true,
              maxlength: 40
            },
            gender: "required",  
            age: {
                required: true,
                number: true,
                min:1,
                max:100
            },
            phone_country_code: {
                required: true,
                number: true
            },
            phone: {
                required: true,
                minlength:9,
                maxlength:10,
                number: true,
                remote: {
                    url: "/users/phone/check",
                    type: "get",
                    data: {
                        phone: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: "/users/email/check",
                    type: "get",
                    data: {
                        email: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            photo: {
                required: false,
                filesize: 5000000,
            },
            subscription_id: "required",
            join_date: "required",
            type: "required",             
            status: "required",   
            //subscription_id: "required",   
        },
        messages: {
            first_name: {
                required: "Please enter the first name",
                lettersonly: "Please enter characters only"
            },
            last_name: {
                required: "Please enter the last name",
                lettersonly: "Please enter characters only"
            },
            gender: "Please select the gender",      
            age: {
                required: "Please enter the age",
                number: "Please enter valid age"
            },
            phone_country_code: {
                required: "Please select the phone code"
            },
            phone: {
                required: "Please enter the phone number",
                remote: "Phone number already exists"
                //phoneAU: "Phone number is invalid"
            }, 
            email: {
                required: "Please enter a valid email address",
                remote: "Email already exists"
            },
            //subscription_id: "Please select the subscription plan",
            join_date: "Please enter the join date",
            type: "Please select the type",    
            subscription_id: "Please select the subscription plan",          
            status: "Please select the status"                
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "subscription_id"||element.attr("name") == "type"||element.attr("name") == "phone_country_code") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: function (form) {         
            var currentYear = (new Date).getFullYear();
            currentYear = currentYear-1;
            var value = $("#join_date").val();
            var date = value.split("/");
            if(date[2]<currentYear) {
                $('#joindate_err').html('Join date should be above ' + currentYear + ' year.');
            } else {
                form.submit();
            }
        }
    });
});

function imagecheck() {
    var ext = $('#photo').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
        $('#photo_err').html("");
        return true;
    }
    else
    {
        $('#photo').val('');
        $('#photo_err').html("Only .jpeg, .jpg, .png formats are allowed");
    }
}

function videocheck() {
    var ext = $('#video').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "mp4" || ext == "flv") { 
        $('#video_err').html("");
        return true;
    }
    else
    {
        $('#video').val('');
        $('#video_err').html("Only .mp4, .flv formats are allowed");
    }
}

function getsubcategory(val) {
    $.ajax({
        url: '/subcat',
        type: 'POST',
        data: {"_token": "{{ csrf_token() }}", cat_id:val},
        success: function (data) { 
          $('#subcat_old').remove();
          $('#subcat_new').html(data);
        }
    }); 
}
</script>    

<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
  
<script type="text/javascript">

function initialize() {

    $('form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    const locationInputs = document.getElementsByClassName("map-input");

    const autocompletes = [];
    const geocoder = new google.maps.Geocoder;
    for (let i = 0; i < locationInputs.length; i++) {

        const input = locationInputs[i];
        const fieldKey = input.id.replace("-input", "");
        const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';

        const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -33.8688;
        const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 151.2195;

        const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
            center: {lat: latitude, lng: longitude},
            zoom: 13
        });
        const marker = new google.maps.Marker({
            map: map,
            position: {lat: latitude, lng: longitude},
        });

        marker.setVisible(isEdit);

        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
    }

    for (let i = 0; i < autocompletes.length; i++) {
        const input = autocompletes[i].input;
        const autocomplete = autocompletes[i].autocomplete;
        const map = autocompletes[i].map;
        const marker = autocompletes[i].marker;

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker.setVisible(false);
            const place = autocomplete.getPlace();

            geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    const lat = results[0].geometry.location.lat();
                    const lng = results[0].geometry.location.lng();
                    setLocationCoordinates(autocomplete.key, lat, lng);
                }
            });

            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                input.value = "";
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

        });
    }
}

function setLocationCoordinates(key, lat, lng) {
    const latitudeField = document.getElementById(key + "-" + "latitude");
    const longitudeField = document.getElementById(key + "-" + "longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
}

/*$("#address-input").trigger("click");*/

</script>  
@endsection
