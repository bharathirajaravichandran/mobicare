@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View User
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('users') }}">Users</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4 input_field_sections">
                                    <h5>First Name</h5>
                                   <strong>{{$user->first_name}}</strong>
                                     
                               </div>
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Last Name</h5>
                                 <strong>{{$user->last_name}}</strong>
                                 </div>
                                    <div class="col-sm-4 input_field_sections">
                                    <h5>Gender</h5>
                                      <strong>{{$user->gender}}</strong>
                                   </div>                                                               
                              </div>
                             <div class="row">
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Photo</h5>
                                        @if(isset($user->photo) && $user->photo!='')
                                         <img src="{{ url('storage/user/'.$user->photo) }}" style="width:50px; height: 60px;">
                                        @else
                                        ---
                                        @endif                                        
                                         @if(isset($user->video) && $user->video!='')
                                         <h5>Video</h5>
                                         <a href="{{ url('storage/user/video/'.$user->video) }}" target="_blank" class="btn btn-success btn-sm">Show video</a>
                                         @endif
                                   </div>   
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Age</h5>
                                      <strong>{{$user->age}}</strong>
                                   </div>
                                 <div class="col-sm-4 input_field_sections">
                                    <h5>Phone</h5>
                                       <strong>{{$user->phone_country_code}} {{$user->phone}}</strong>
                                   </div>                                                           
                            </div>
                           <div class="row">
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Email</h5>
                                      <strong>{{$user->email}}</strong>
                                   </div>   
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Join Date</h5>
                                      <strong>{{isset($user->join_date)?$user->join_date:'--'}}</strong>
                                   </div>  
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Fee</h5>
                                      <strong>{{isset($user->fee)?$user->fee:'--'}}</strong>
                                   </div>                                                  
                          </div>
                          <div class="row">                           
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Description</h5>
                                      <strong>{{isset($user->description)?$user->description:'--'}}</strong>
                                   </div>
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Experience</h5>
                                      <strong>{{isset($user->experience)?$user->experience:'--'}}</strong>
                                   </div>
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Location</h5>
                                      <strong>{{isset($user->location)?$user->location:'--'}}</strong>
                                   </div>
                          </div>
                          <div class="row">                               
                                 <div class="col-sm-4 input_field_sections">
                                    <h5>Type</h5>
                                      <strong>{{$user->user_types->name}}</strong>
                                   </div>  
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Verified Status</h5>
                                      <strong>{{(isset($user->verified_status) && $user->verified_status==1)?'Verified':'Not Verified'}}</strong>
                                   </div>                                               
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Blocked Status</h5>
                                      <strong>{{(isset($user->blocked_status) && $user->blocked_status==1)?'Blocked':'Not'}}</strong>
                                   </div>                              
                            </div>
                            <div class="row">                            
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Health Condition</h5>
                                      <strong>{{isset($user->health_condition)?$user->health_condition:'--'}}</strong>
                                   </div>
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Status</h5>
                                      <strong>{{(isset($user->status) && $user->status==1)?'Active':'Inactive'}}</strong>
                                   </div>
                                <div class="col-sm-4 input_field_sections">
                                    <h5>Created At</h5>
                                      <strong>{{$user->created_at}}</strong>
                                   </div>
                            </div>                        

                            @if(count($doc)>0)
                             <br/><hr/>
                             <h4 class="modal-title">View document</h4>
                             <table style="width:40%;">
                              <tr>
                                <th>Name</th>                        
                                <th>Action</th>
                              </tr>
                              @foreach($doc as $value)
                              <tr>
                                <td>{{$value->documents->name}}</td>                        
                                <td><a href="{{url('storage/certificates').'/'.$value->file_path}}" target="_blank" style="color:crimson;">Download</a></td>
                              </tr>
                              @endforeach
                            </table>
                            @endif

                            @if(isset($plan) && count($plan) > 0)
                            <br><hr>
                            <h4 class="modal-title">Subscription plan</h4>
                            <table class="table">
                                <thead>
                                  <th>Plan name</th>
                                  <th>Start Date</th>
                                  <th>End date</th>
                                  <th>Amount</th>
                                  <th>Status</th>
                                </thead>
                                <tbody>
                                  @foreach($plan as $list)
                                  <tr>
                                    <td>{{$list->plan_name}}</td>
                                    <td>{{$list->start_date}}</td>
                                    <td>{{$list->end_date}}</td>
                                    <td>{{$list->amount_paid}}</td>
                                    <td>{{$list->expiry_status==1?'Active':'Expired'}}</td>
                                  </tr>
                                  @endforeach
                                </tbody>

                            </table>
                            @endif

                        </div>
                    </div>
                </div>
            </div>

          
           
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
