@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-plus"></i>
                       Add User
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('users') }}">Users</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="customer_form" id="customer_form" method="post" action="{{ url('users/store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" value="hid_id">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>First Name</h5>
                                   <input type="text" name="first_name" class="form-control" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Last Name</h5>
                                   <input type="text" name="last_name" class="form-control" />  
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Gender</h5>
                                   <input type="radio" name="gender" value="Male">Male
                                   <input type="radio" name="gender" value="Female">Female
                                   <input type="radio" name="gender" value="Trans">Trans
                                 </div>
                                <div class="col-sm-3 input_field_sections">
                                    <h5>Photo</h5>
                                   <input type="file" name="photo" id="photo" onchange="return imagecheck();" />  
                                   <div id="photo_err" style="color:crimson;"></div>
                                 </div>
                                 <div class="col-sm-3 input_field_sections">
                                    <h5>Video</h5>
                                   <input type="file" name="video" id="video" onchange="return videocheck();" />  
                                   <div id="video_err" style="color:crimson;"></div>
                                 </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Age</h5>
                                   <input type="text" name="age" class="form-control" />
                               </div>
                                <div class="col-sm-2 input_field_sections">
                                    <h5>Phone code</h5>
                                    <select name="phone_country_code" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($countries as $c)
                                      <option value="{{$c->phonecode}}">{{$c->phonecode}}</option>
                                      @endforeach
                                    </select>
                               </div>
                               <div class="col-sm-4 input_field_sections">
                                    <h5>Phone number</h5>
                                   <input type="text" name="phone" class="form-control"  />
                                   <div style="font-size: 11px; color:blue;">Phone number should be ten digits</div>
                               </div>
                               
                              </div>
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                   <input type="text" name="email" class="form-control"  />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                    <select name="type" id="type" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($usertypes as $u)
                                      <option value="{{$u->id}}">{{$u->name}}</option>
                                      @endforeach
                                    </select>
                                    <span>                                      
                               </div>                                  
                              </div>                               
                              <div class="row" id="user">  
                                   <div class="col-sm-6 input_field_sections">
                                    <h5>Health Condition</h5>
                                    <select name="health_condition" class="form-control">
                                      <option value="">Select</option>
                                      <option value="Diabetic">Diabetic</option>
                                      <option value="Heart issues">Heart issues</option>
                                    </select>
                                 </div>   
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Join Date</h5>
                                    <input type="text" name="join_date" id="join_date" class="form-control startdate" value="{{date('m/d/Y')}}" />
                                    <div class="error" id="joindate_err"></div>
                               </div>     
                              </div>
                              <div class="row" id="provider_or_both" style="display: none;">  
                                  <!-- <div class="col-sm-6 input_field_sections">
                                      <h5>Verifications</h5>
                                      <a class="btn btn-primary btn-sm" data-toggle="modal" href="#100pt_check">100pt ID Check</a>
                                  </div> -->

                              <div class="col-sm-6 input_field_sections">
                                <h5>Working Days </h5>
                                  <?php 
                                  $days = array('0'=>'Sunday','1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday');
                                    $availability = isset($user->availability)? $user->availability:array();
                                  ?>
                                   <select class="form-control" name="availability[]" multiple="multiple">
                                     @foreach($days as $key=>$day) 
                                        <option value="{{$key}}" @if(in_array($key,$availability)) selected @endif>{{$day}}</option>
                                     @endforeach     
                                  </select>
                              </div>


                                {{--  <div class="col-sm-6 input_field_sections">
                                       <h5>Category</h5>
                                       <select name="industry[]" multiple="true" class="form-control select2" onchange="return getsubcategory(this.value);">
                                         @foreach($category as $c)
                                         @if(isset($subcategory[$c->id]))
                                            <optgroup label="{{$c->name}}">
                                              @foreach($subcategory[$c->id] as $parent => $sub)
                                                <option value="{{ $c->id.'_'.$sub->id }}" {{ (isset($user->subcategory) && in_array($sub->id, $subcategoryArray))?'selected':'' }}>{{$sub->name}}</option>
                                                @endforeach
                                            </optgroup>
                                         @else
                                         <option value="{{$c->id}}" {{ (isset($user->category) && in_array($c->id, $categoryArray))?'selected':'' }}>{{$c->name}}</option>
                                         @endif
                                         @endforeach
                                       </select>
                                  </div> --}}
                                  <!--<div class="col-sm-6 input_field_sections">
                                       <h5>Category</h5>
                                       <select name="category" class="form-control" onchange="return getsubcategory(this.value);">
                                         <option value="">Select</option>
                                         @foreach($category as $c)
                                         <option value="{{$c->id}}">{{$c->name}}</option>
                                         @endforeach
                                       </select>
                                  </div>
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Subcategory</h5>
                                       <div id="subcat_new"></div>
                                       <select name="subcategory" id="subcat_old" class="form-control">
                                         <option value="">Select</option>
                                       </select>
                                  </div>-->
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Fee</h5>
                                       <input type="text" class="form-control" name="fee">
                                  </div>   
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Description</h5>
                                       <textarea name="description" class="form-control"></textarea>
                                  </div>  
                                  <div class="col-sm-6 input_field_sections">
                                       <h5>Experience</h5>
                                       <input type="text" class="form-control" name="experience">
                                  </div> 
                              </div>
                              <div class="row">    
                                <div class="col-sm-6 input_field_sections">
                                  <div>
                                      <label for="address_address">Address</label>
                                      <input type="text" id="address-input" name="location" class="form-control map-input">
                                      <input type="hidden" name="address_latitude" id="address-latitude" />
                                      <input type="hidden" name="address_longitude" id="address-longitude" />
                                  </div> 
                                  <div id="address-map-container" style="width:100%;height:400px;">
                                      <div style="width: 100%; height: 100%" id="address-map"></div>
                                  </div>
                                </div>                                                     

                                <div class="col-sm-6 input_field_sections">
                                    <h5>Blocked status</h5>
                                    <select name="blocked_status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1">Blocked</option>
                                      <option value="0" selected="">No</option>
                                    </select>
                                </div>  

                                <div class="col-sm-6 input_field_sections">
                                    <h5>Subscription plan</h5>
                                    <select name="subscription_id" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($subscription as $plan_id=>$name)
                                      <option value="{{$plan_id}}" {{ isset($user->subscription_id) && $user->subscription_id==$plan_id ?'selected':'' }} >{{$name}}</option>
                                      @endforeach
                                    </select>
                               </div>                                             
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select name="status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" selected="">Active</option>
                                      <option value="0">Inactive</option>
                                    </select>
                               </div>                           
                              </div> 
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" id="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('users') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>


            {{--<div id="100pt_check" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">        
                    <h4 class="modal-title">100pt ID Check</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <table style="width:100%;">
                      <tr>
                        <th style="width: 230px;">Name</th>                        
                        <th>Primary</th>
                        <th>Points</th>
                        <th>Action</th>
                      </tr>
                      @foreach($docs as $d)
                      <tr>
                        <td>{{$d->name}}</td>                        
                        <td>
                          @if($d->is_primary==1)
                            Yes
                          @else
                            No
                          @endif
                        </td>
                        <td>{{$d->points}}</td>
                        <td><button type="button" class="btn btn-success btn-sm addcert" data-name="{{$d->name}}" data-id="{{$d->id}}" data-points="{{$d->points}}">Add</button><br/></td>
                      </tr>
                      @endforeach
                    </table><br/>
                    <table class="cert" style="width: 100%; display: none;">
                      <tr>
                        <th>Attach certificates</th>
                      </tr>   
                      <tr class="birth_certificate"></tr>
                      <tr class="driver_license"></tr>              
                      <tr class="passport"></tr>              
                      <tr class="cert_aus_citizenship"></tr>                   
                      <tr class="change_name_cert"></tr>
                      <tr class="aus_visa"></tr>                   
                      <tr class="marriage_cert"></tr>                                      
                    </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>--}}
            
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $( "#join_date" ).change(function() {
        var currentYear = (new Date).getFullYear();
        currentYear = currentYear-1;
        var value = $(this).val();
        var date = value.split("/");
        if(date[2]<currentYear) {
            $('#joindate_err').html('Join date should be above ' + currentYear + ' year.');
        } else {
            $('#joindate_err').html('');
        }
    });

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    $("#type").change(function(){
        var val = this.value;
        if(val==3) {
          $('#provider_or_both').show();
          $('#user').hide();
        }
        else if(val==4) {
          $('#provider_or_both').show();
          $('#user').hide();
        }
        else {
          $('#provider_or_both').hide();
          $('#user').show();
        }
    });

    $.validator.methods.email = function( value, element ) {
        var email = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return email.test(value);
    }

    jQuery.validator.addMethod("lettersonly", function(value, element) {
       return this.optional(element) || /^[a-z\s]+$/i.test(value);
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 5MB.');

    // validate form on keyup and submit
    $("#customer_form").validate({
        rules: {
            first_name: {
              required: true,
              lettersonly: true,
              maxlength: 40
            },
            last_name: {
              required: true,
              lettersonly: true,
              maxlength: 40
            },
            gender: "required",  
            age: {
                required: true,
                number: true,
                min:1,
                max:100
            },
            phone_country_code: {
                required: true,
                number: true
            },
            phone: {
                required: true,
                minlength:9,
                maxlength:10,
                number: true,
                remote: {
                    url: "/users/phone/check",
                    type: "get",
                    data: {
                        phone: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: "/users/email/check",
                    type: "get",
                    data: {
                        email: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            photo: {
                required: false,
                filesize: 5000000,
            },
            //industry: "required",
            join_date: "required",
            type: "required",   
            subscription_id : "required",          
            status: "required",      
        },
        messages: {
            first_name: {
                required: "Please enter the first name",
                lettersonly: "Please enter characters only"
            },
            last_name: {
                required: "Please enter the last name",
                lettersonly: "Please enter characters only"
            },
            gender: "Please select the gender",      
            age: {
                required: "Please enter the age",
                number: "Please enter valid age",
            },
            phone_country_code: {
                required: "Please select the phone code"
            },
            phone: {
                required: "Please enter the phone number",
                remote: "Phone number already exists"
                //phoneAU: "Phone number is invalid"
            }, 
            email: {
                required: "Please enter a valid email address",
                remote: "Email already exists"
            },
            subscription_id: "Please select the subscription plan",
            join_date: "Please enter the join date",
            type: "Please select the type",              
            status: "Please select the status"                
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "subscription_id"||element.attr("name") == "type"||element.attr("name") == "phone_country_code") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: function (form) {         
            var currentYear = (new Date).getFullYear();
            currentYear = currentYear-1;
            var value = $("#join_date").val();
            var date = value.split("/");
            if(date[2]<currentYear) {
                $('#joindate_err').html('Join date should be above ' + currentYear + ' year.');
            } else {
                form.submit();
            }
        }
    });
});

function imagecheck() {
    var ext = $('#photo').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
        $('#photo_err').html("");
        return true;
    }
    else
    {
        $('#photo').val('');
        $('#photo_err').html("Only .jpeg, .jpg, .png formats are allowed");
    }
}

function videocheck() {
    var ext = $('#video').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "mp4" || ext == "flv") { 
        $('#video_err').html("");
        return true;
    }
    else
    {
        $('#video').val('');
        $('#video_err').html("Only .mp4, .flv formats are allowed");
    }
}

function getsubcategory(val) {
    $.ajax({
        url: '/subcat',
        type: 'POST',
        data: {"_token": "{{ csrf_token() }}", cat_id:val},
        success: function (data) { 
          $('#subcat_old').remove();
          $('#subcat_new').html(data);
        }
    }); 
}
</script>      
<script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
  
<script type="text/javascript">

function initialize() {

    $('form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
    const locationInputs = document.getElementsByClassName("map-input");

    const autocompletes = [];
    const geocoder = new google.maps.Geocoder;
    for (let i = 0; i < locationInputs.length; i++) {

        const input = locationInputs[i];
        const fieldKey = input.id.replace("-input", "");
        const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';

        const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -33.8688;
        const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 151.2195;

        const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
            center: {lat: latitude, lng: longitude},
            zoom: 13
        });
        const marker = new google.maps.Marker({
            map: map,
            position: {lat: latitude, lng: longitude},
        });

        marker.setVisible(isEdit);

        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
    }

    for (let i = 0; i < autocompletes.length; i++) {
        const input = autocompletes[i].input;
        const autocomplete = autocompletes[i].autocomplete;
        const map = autocompletes[i].map;
        const marker = autocompletes[i].marker;

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker.setVisible(false);
            const place = autocomplete.getPlace();

            geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    const lat = results[0].geometry.location.lat();
                    const lng = results[0].geometry.location.lng();
                    setLocationCoordinates(autocomplete.key, lat, lng);
                }
            });

            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                input.value = "";
                return;
            }

            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

        });
    }
}

function setLocationCoordinates(key, lat, lng) {
    const latitudeField = document.getElementById(key + "-" + "latitude");
    const longitudeField = document.getElementById(key + "-" + "longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
}
</script>
@endsection

