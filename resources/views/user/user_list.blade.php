@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-4 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-th"></i>
                        Users
                    </h4>
                </div>
                <div class="col-lg-6 col-md-8 col-sm-8">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('user') }}">Users</a>
                        </li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </header>

    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-12 data_tables">
                 
                    <!-- BEGIN EXAMPLE2 TABLE PORTLET-->
                    <div class="card">
                        
                        <div class="card-body m-t-35">

                            <div class="row">
                                <div class="col-sm-2">  
                                    <div class="btn-group show-hide">
                                      @php $access = checkAdminPermission(); if(in_array('add', $access)) { @endphp
                                      <a class="btn btn-primary" href="{{url('users/add')}}"> Add New </a>
                                      @php } @endphp
                                      
                                    </div>
                                </div>  
                                @if(in_array('view', $access))                                        
                                <div class="col-sm-3">
                                    <input type="text" class="form-control startdate" name="start_date" id="start_date" placeholder="Start Date">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control enddate" name="end_date" id="end_date" placeholder="End Date">
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control input-sm" name="type" id="type" onchange="$('.datatable').DataTable().draw()">
                                        <option value="">Select Type</option>
                                        <option value="2">User</option>
                                        <option value="3">Provider</option>
                                        <option value="4">Both</option>
                                    </select>
                                </div>
                                <button type="submit" id="search" class="btn btn-primary">Search</button>
                                @endif
                            </div>

                           <div class=" m-t-15">
                             @if(in_array('view', $access)||in_array('edit', $access)||in_array('delete', $access))           
                             <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Type</th>
                                        <th width="80">Date</th>
                                        <th>Status</th>
                                        <th width="170">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            @endif
                        </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE2 TABLE PORTLET-->
                   
                   
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
</div>

<form name="deleteForm" id="deleteForm" method="post">
     {{ csrf_field() }} 
     <input type="hidden" name="hid_id" id="hid_id">
<div id="deleteModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="material-icons">&#xE5CD;</i>
                </div>              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger" onclick="return fnDelete();">Delete</button>
            </div>
        </div>
    </div>
</div>  
</form> 
<!-- startsec End -->     

<div id="viewdoc" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title">View Document</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    var oTable = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth:false,
        order:[[5,"desc"]],
        ajax: {
            url: '{{ route('users/getdata') }}',
            data: function (d) {
                d.type = $('select[name=type]').val();
                d.start_date = $('input[name=start_date]').val();
                d.end_date = $('input[name=end_date]').val();
            }
        },
        columns: [
            {data: 'id', name: null, orderable: false, searchable: false},
            {data: 'first_name', name: 'first_name', sortable: true},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},            
            {data: 'user_types_id', name: 'user_types_id'},
            {data: 'created_at', name: 'users.created_at'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        buttons: ['csv'],
        columnDefs: [
            {
                targets: 1,
                render: function ( data, type, row ) {
                    return row.first_name + ' ' + row.last_name;
                }
            },
            {
                targets: 4,
                render: function ( data, type, row ) {
                    if (row.user_types_id==2) {
                        return '<span class="label label-default">User</span>';
                    }
                    else if (row.user_types_id==3) {
                        return '<span class="label label-default">Provider</span>';
                    }
                    else {
                        return '<span class="label label-default">Both</span>';
                    }
                }
            },           
            {
                targets: 6,
                "render": function(data, type, row) {
                    if(row.user_types_id==3 && row.verified_status==1)
                        return '<span class="label label-default">Verified</span>';
                    else if(row.user_types_id==3 && row.verified_status==0)
                        return '<span class="label label-default">Waiting for review</span>';
                    else if(row.user_types_id==2 && row.status==1)
                        return '<span class="label label-default">Active</span>';
                    else if(row.user_types_id==2 && row.status==0)
                        return '<span class="label label-default">Inactive</span>';
                    else if(row.user_types_id==4 && row.status==1)
                        return '<span class="label label-default">Active</span>';
                    else if(row.user_types_id==4 && row.status==0)
                        return '<span class="label label-default">Inactive</span>';
                    else
                        return '--';
                 }
            }
        ],
        "rowCallback": function (nRow, aData, iDisplayIndex) {
             var oSettings = this.fnSettings ();
             $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
             return nRow;
        }
    });

    

    $('#search').on('click', function(e) {
        oTable.draw();
        e.preventDefault();
    });

    setTimeout(function() {
      $('.alert').fadeOut('slow');
    }, 3000); 

});

var url = "{{ url('') }}";
function showDeleteModal(id)
{
    $('#deleteForm #hid_id').val(id);
}

function fnDelete() 
{
    var id = $('#deleteForm #hid_id').val();
    $('form[name=deleteForm]').attr('action',url+'/users/delete/'+id);
    $('form[name=deleteForm]').submit();
}
function fnViewdoc(id)
{
    var id = id;
    $.ajax({
        url: "user/view_document",
        type: "post",
        data: { "_token": "{{ csrf_token() }}", id : id },
        success: function(data){
            $('#viewdoc').modal('toggle');
            $('.modal-body').html(data);
        }
    });
}
</script>
@endsection
