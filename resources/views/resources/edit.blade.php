@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Resources
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('resources') }}">Education / Resources</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="resources_form" id="resources_form" method="post" action="{{ url('resources/update') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{$resources->id}}">
                <input type="hidden" name="image_delete" id="image_delete">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Title</h5>
                                   <input type="text" name="title"  class="form-control" value="{{$resources->title}}" />
                               </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Description</h5>
                                  <textarea name="description" class="form-control">{{$resources->description}}</textarea>
                               </div>
                              </div>          
                               <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                    <select name="type" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($resources->type=='1') selected="" @endif>User</option>
                                      <option value="2" @if($resources->type=='2') selected="" @endif>Provider</option>
                                    </select>
                               </div>   
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select name="status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" @if($resources->status=='1') selected="" @endif>Active</option>
                                      <option value="2" @if($resources->status=='2') selected="" @endif>Inactive</option>
                                    </select>
                               </div>                              
                              </div>   
                               <div class="row">                                 
                               <div class="col-sm-6 input_field_sections">
                                    <h5>File</h5>         

                                    <div class="input-group file-caption-main" style="position:relative;">
                                        <div class="form-control file-caption  kv-fileinput-caption">
                                        <span class='label' style="color:#444;" id="upload-file-info"></span>
                                        </div>
                                        <div class="input-group-btn">
                                        <a class='btn btn-primary btn_primaryNN' href='javascript:;' style="position: relative;">
                                          Browse
                                          <input id="resfile" type="file" name="upfile[]" onchange='$("#upload-file-info").html($(this).val());'>
                                        </a>
                                        </div>                                    
                                    </div><br/>
                                   
                                    <div id="proimg" @if(count($resources->resourcefiles)>0) class="file_drop_disabled" @endif>
                                        <div class="file_preview_thumbnails">
                                            <div id="mainimg">
                                             @foreach($resources->resourcefiles as $r)
                                                   <div id="previewImg0" class="previewBox">
                                                      <span class="delete" onclick="return fnDelImage();">X</span>
                                                      <img src="{{ url('/') }}/storage/{{$r->file}}" style="width: 150px;height: 150px;" /> 
                                                  </div>                                        
                                              @endforeach
                                            </div>
                                        </div>    
                                    </div>    
     
                               </div>                        
                              </div>   
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('resources') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 


<script>
$(document).ready(function(){

    $('#title, #description').change(function() {
        $(this).val($(this).val().trim());
    });

    // validate form on keyup and submit
    $("#resources_form").validate({
        rules: {
            title: {
                required: true,
                maxlength: 100, 
                remote: {
                    url: "/resources/title/check",
                    type: "get",
                    data: {
                        name: this.value,
                        id: $('#hid_id').val()
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            description: {
                required: true
            },
            type: {
                required: true
            },
            status: {
                required: true
            }
        },
        messages: {
            title: {
                required: "Please enter the title",
                remote: "Name already exists"
            },
            description: {
                required: "Please enter the description"
            },
            type: {
                required: "Please select the type"
            },
            status: {
                required: "Please select the status"
            }            
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "type") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });


    deletePreview = function (ele, i) {
      "use strict";
      try {          
        $('#proimg').hide();
        $('#upload-file-info').hide();
        $(ele).parent().remove();
        window.filesToUpload.splice(i, 1);
        console.log(count(window.filesToUpload));
      } catch (e) {
        console.log(e.message);
      }
    }

    $("#resfile").on('change', function() {
        "use strict";

        $('#proimg').show();
        $('#upload-file-info').show();

        var fileInput = document.getElementById('resfile');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
        if(!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
            fileInput.value = '';
            return false;
        }
        var oFile = document.getElementById("resfile").files[0];
        if (oFile.size > 5242880) // 5 mb for bytes.
        {
            alert("File size must under 5mb!");
            return;
        }

        // create an empty array for the files to reside.
        window.filesToUpload = [];

        if (this.files.length >= 1) {
           $("[id^=previewImg]").remove();
           $('.file_drop_disabled').css('border', '1px solid #ddd');
           $.each(this.files, function(i, img) { 

              var file = img.name;
              var extension = file.substr( (file.lastIndexOf('.') +1) );

              var reader = new FileReader();
              var new_row = document.createElement("div");
              new_row.setAttribute("id", "previewImg" + i);
              new_row.setAttribute("class", "previewBox");
              new_row.appendChild( document.createTextNode("") );
              document.body.appendChild( new_row );

              var sn = document.createElement("span");
              sn.setAttribute("class", "delete");
              sn.onclick = function() { deletePreview(this, i); };
              sn.appendChild( document.createTextNode("X") );
              document.body.appendChild( sn );

              reader.onload = function (e) {
                  var img = new Image();
                  img.src = e.target.result;
                  img.setAttribute('style', 'width:150px; height:150px;');    
                  // you can adjust the image size by changing the width value. 
                  $(new_row).appendTo('#mainimg');
                  $(sn).prependTo(new_row);
                  $('.previewBox').append(img);               
              };
              reader.readAsDataURL(this);
           });            
        }
    });
});

function fnDelImage() {
    try {          
      $('#proimg').hide();
      $('#upload-file-info').hide();
      $('#image_delete').val('yes');
      $(ele).parent().remove();
      window.filesToUpload.splice(i, 1);
      console.log(count(window.filesToUpload));
    } catch (e) {
      console.log(e.message);
    }
}
</script>

<style type="text/css">
#resfile {
z-index:2;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0; cursor: pointer;
}
#img {
  width: 17px;
  border: none;
  height: 17px;
  margin-left: -20px;
  margin-bottom: 191px;
}
.previewBox {
  text-align: center;
  position: relative;
  width: 150px;
  height: 150px;
  margin-right: 10px;
  margin-bottom: 20px;
  float: left;
}
.previewBox img {
  height: 150px;
  width: 150px;
  padding: 5px;
  border: 1px solid rgb(232, 222, 189);
}
.delete {
  color: red;
  font-weight: bold;
  position: absolute;
  top: 0;
  cursor: pointer;
  width: 20px;
  height:  20px;
  border-radius: 50%;
  background: #ccc;
}
.file_drop_disabled { position: relative; width:100%; border: 1px solid #ddd; padding: 15px; display: inline-block; width: 100%; }
.file_drop_disabled  #mainimg   img { padding: 0px !Important; border: 0px !Important;  }
.file_drop_disabled .previewBox {     position: inherit; }
.file_drop_disabled .previewBox .delete { background: #000; color:#fff; right:5px;  }
.file_drop_disabled #otherimg .delete { background: #000; color:#fff;  }
</style>
@endsection
