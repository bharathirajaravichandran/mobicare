@extends('layouts.admin')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Resources
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('resources') }}">Education / Resources</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Title</h5>
                                   <strong>{{$resources->title}}</strong>
                                     
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Description</h5>
                                 <strong>{{$resources->description}}</strong>
                                 </div>                                                                                         
                              </div>
                             <div class="row">
                                  <div class="col-sm-6 input_field_sections">
                                    <h5>Type</h5>
                                      <strong>@if($resources->type==1) User @else Provider @endif</strong>
                                   </div>  
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                       <strong>@if($resources->status==1) Active @else Inactive @endif</strong>
                                   </div>                                              
                            </div>
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Files</h5>
                                     @if(count($resources->resourcefiles)>0)
                                        <div>                                      
                                          <ul style="padding: 0px;">
                                            @foreach($resources->resourcefiles as $r)
                                            <li style="list-style-type:none;"><img src="{{ url('storage/'.$r->file) }}" style="width:80px; height: 70px;"></li>
                                            @endforeach
                                          </ul>                                      
                                        </div>
                                     @else
                                        --
                                     @endif
                                   </div>                                
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Created At</h5>
                                      <strong>{{$resources->created_at}}</strong>
                                   </div>                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>   
@endsection
