 <div class="row">   
   <div class="col-sm-6 input_field_sections">
      <h5>Category</h5>
       <select class="form-control" name="category_id" id="category_id">
        <option value="">Select</option>
        @foreach($industry as $i)
        <option value="{{$i->id}}" {{ isset($skill->category_id) && $skill->category_id == $i->id?'selected':'' }}>{{$i->name}}</option>
        @endforeach
      </select>
   </div>

   <div class="col-sm-6 input_field_sections">
      <h5>Name</h5>
     <input type="text" name="name" class="form-control" value="{{ isset($skill->name)?$skill->name:'' }}" />
   </div>

   <div class="col-sm-6 input_field_sections">                                    
      <h5>Status</h5>
      <select class="form-control" name="status">
       <option value="">-Select Status-</option>
       <option value="1" {{ isset($skill->status) && $skill->status == 1?'selected':'' }}>Active</option>
       <option value="0" {{ isset($skill->status) && $skill->status == 0?'selected':'' }}>Inactive</option>
      </select>
   </div>
</div>

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    // validate form on keyup and submit
    $("#skill_form").validate({
        rules: {
            name: { 
                required: true,
                maxlength: 50,
                remote: {
                    url: "/skills/name/check",
                    type: "get",
                    data: {
                        id: function(){return $('select[name=category_id]').val();},
                        hid_id: function(){return $('input[name=hid_id]').val();}
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                } 
            },
            category_id: { 
                required: true,
            },
            status: {
                required: true
            }
        },
        messages: {
            name: { 
                required: "Please enter the name",
                remote: "Name already exists"
            },
            category_id: { 
                required: "Please select the category"
            },
            status: { 
                required: "Please select the status"
            }               
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "category_id"||element.attr("name") == "status") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        }
    });
});
</script>