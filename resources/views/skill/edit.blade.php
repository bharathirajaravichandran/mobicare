@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Skill
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('skills') }}">Skill Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="skill_form" id="skill_form" method="post" action="{{ url('skills/update', $skill->id) }}" accept-charset="UTF-8">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{$skill->id}}">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              @include ('skill.form', [
                                        'skill' => $skill,
                                      ]) 


                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('skills') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $("#is_mandatory").change(function() {
        var ischecked= $(this).is(':checked');
        if(!ischecked) {
          $('#is_mandatory').val(0);
        } else {
          $('#is_mandatory').val(1);
        }          
    }); 

    $("#is_primary").change(function() {
        var ischecked= $(this).is(':checked');
        if(!ischecked) {
          $('#is_primary').val(0);
        } else {
          $('#is_primary').val(1);
        }          
    }); 

    $("#is_graduate").change(function() {
        var ischecked= $(this).is(':checked');
        if(!ischecked) {
          $('#is_graduate').val(0);
        } else {
          $('#is_graduate').val(1);
        }          
    }); 

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    // validate form on keyup and submit
    $("#certificate_form").validate({
        rules: {
            name: { 
                required: true,
            },
            points: { 
                required: true,
                number: true
            },
            industry: { 
                required: true,
            },
            status: {
                required: true
            }
        },
        messages: {
            name: { 
                required: "Please enter the name",
            },
            points: { 
                required: "Please enter the points"
            },
            industry: { 
                required: "Please select the industry"
            },
            status: { 
                required: "Please select the status"
            }               
        }
    });
});
</script>      
@endsection
