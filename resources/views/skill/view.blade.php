@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Skill
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('skills') }}">Skill Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row">                                
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Category</h5>
                                       <strong>{{@$skill->category->name}}</strong>
                                   </div>      
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Name</h5>
                                      <strong>{{$skill->name}}</strong>
                                   </div>     
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                      <strong>@if($skill->status==1) Active @else Inactive @endif</strong>
                                   </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Created At</h5>
                                      <strong>{{$skill->created_at}}</strong>
                                   </div>                                                                                     
                              </div>

                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>    
@endsection
