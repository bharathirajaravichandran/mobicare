@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-plus"></i>
                       Add Content
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('cms') }}">Content Management</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="cms_form" id="cms_form" method="post" action="{{ url('cms/store') }}" accept-charset="UTF-8">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id">
            <div class="row">
                <div class="col">
                    <div class="card">                       
                        <div class="card-body">
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Page Name</h5>
                                   <input type="text" name="pagename" id="pagename" class="form-control" />
                               </div>
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Slug</h5>
                                   <input type="text" name="pagelink" id="pagelink" class="form-control" />
                               </div>
                              </div>
                              
                              <div class="row">
                                 <div class="col-sm-12 input_field_sections">
                                    <h5>Content</h5>                                   
                                    <textarea id="editor1" name="pagecontent" class="form-control"></textarea>
                               </div>
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select class="form-control" name="status">
                                     <option value="">-Select Status-</option>
                                     <option value="1" selected="">Active</option>
                                     <option value="0">Inactive</option>
                                    </select>
                               </div>
                              </div>
                           
                        </div>                        
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">
                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('cms') }}'">
                            </div>
                        </div>
                    </div>                    

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 


<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script>
var roxyFileman = '/fileman/index.html?integration=ckeditor';
$(function(){
  CKEDITOR.replace( 'editor1',{filebrowserBrowseUrl:roxyFileman, 
                               filebrowserImageBrowseUrl:roxyFileman+'&type=image',
                               removeDialogTabs: 'link:upload;image:upload'});
});
</script>

<script>
$(document).ready(function() {

    $("#pagename").on("keydown", function () {
       $("#pagelink").val($(this).val().toLowerCase().replace(/\s+/g, "-"));
    });

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    // validate form on keyup and submit
    $("#cms_form").validate({
        ignore: [],
        rules: {
            pagename: { 
                required: true,
                maxlength: 50
            },
            pagelink: { 
                required: true
            },
            pagecontent: { 
                ckeditor_required: true
            },
            status: {
                required: true
            }
        },
        messages: {
            pagename: { 
                required: "Please enter the page name"
            },
            pagelink: { 
                required: "Please enter the page slug"
            },
            status: { 
                required: "Please select the status"
            }               
        }
    });

    jQuery.validator.addMethod("ckeditor_required", function(value, element) {
       var editorId = $(element).attr('id');
        var messageLength = CKEDITOR.instances[editorId].getData().replace(/<[^>]*>/gi, '').length;
        if(messageLength>0) {
          return true;         
        } else {
          return messageLength.length === 0;
        }
    }, "Please enter the content");
    
});
</script>      
@endsection
