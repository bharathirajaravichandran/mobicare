@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-4 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-th"></i>
                        Comments and complaints
                    </h4>
                </div>
                <div class="col-lg-6 col-md-8 col-sm-8">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('comments') }}">Comments and complaints</a>
                        </li>
                        
                    </ol>
                </div>
            </div>
        </div>
    </header>

    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
                <div class="col-12 data_tables">
                 
                    <!-- BEGIN EXAMPLE2 TABLE PORTLET-->
                    <div class="card">
                        
                        <div class="card-body m-t-35">
                            <div class="row">
                                <div class="col-sm-2">  
                                    <div class="btn-group show-hide">
                                      <!--  -->
                                    </div>
                                </div>          

                                @php $access = checkAdminPermission(); @endphp
                            
                            </div>

                           <div class=" m-t-15">
                            @if(in_array('view', $access)||in_array('edit', $access)||in_array('delete', $access))   
                              <form name="list" id="list" method="post">
                                {{ csrf_field() }} 
                                <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>CareRequest ID</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th style="width: 130px;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($comments as $key => $val)
                                            @if(@$val->jobs->job_id!='')
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ @$val->jobs->job_id }}</td>
                                                <td>{{ $val->created_at }}</td>
                                                <td><?php echo ($val->status==1)?'<span class="label label-default">Active</span>':'<span class="label label-default">Inactive</span>' ;?></td>
                                                <td>                                               
                                                    @if(in_array('view', $access)) 
                                                    <a href="comments/view/{{$val->jobs->id}}" class="btn btn-info btn-xs" title="View">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                              </form>
                            @endif
                        </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE2 TABLE PORTLET-->
                   
                   
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
</div>
<!-- startsec End -->     

<script type="text/javascript">
$(document).ready(function() {
    var oTable = $('.datatable').DataTable({
        ordering: true,
        buttons: ['csv'],
    });    

    $('#search').on('click', function(e) {
        oTable.draw();
        e.preventDefault();
    });

    setTimeout(function() {
      $('.alert').fadeOut('slow');
    }, 3000); 
});
</script>  
@endsection
