@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Comments and Complaints
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('comments') }}">Comments and Complaints</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>

        <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <h5 style="background: #eaeaea;padding: 10px;">Job {{$jobs->job_id}} Comments</h5>
                        <div class="card-body">                  
                            @foreach($comments as $k => $c)
                            <div class="card">         
                                <div class="card-body">             
                                    <div class="row">
                                        <div class="col-md-2">
                                            
                                            @if($c->user->photo!='')
                                              <img src="{{url('storage/user/'.$c->user->photo)}}" class="img img-rounded img-fluid"/>
                                            @else
                                              <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                            @endif

                                            <p class="text-secondary text-center">{{Carbon\Carbon::createFromTimeStamp(strtotime($c->created_at))->diffForHumans()}}</p>
                                        </div>
                                        <div class="col-md-10" style="margin-top: 10px;">
                                            <p>
                                                <a class="float-left" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>{{$c->user->first_name}} {{$c->user->last_name}}</strong></a>
                                           </p>
                                           <div class="clearfix"></div>
                                            <p>{{$c->feedback}}</p>
                                            <p>
                                                @if($c->reply_status==0)
                                                <a class="float-right btn btn-outline-primary btn-sm" onclick="return fnShow('{{$k}}');"> <i class="fa fa-reply"></i> Reply</a>
                                                @else
                                                <a class="float-right btn btn-outline-primary btn-sm"> <i class="fa fa-reply"></i> Replied</a>
                                                @endif
                                           </p>
                                        </div>
                                    </div>
                                      
                                      @if($c->reply_status==1)
                                        <div class="card card-inner">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                                        <p class="text-secondary text-center">{{Carbon\Carbon::createFromTimeStamp(strtotime($c->updated_at))->diffForHumans()}}</p>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Admin</strong></a></p>
                                                        <p>{{$c->reply_text}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      @endif

                                      <form name="comment_form" id="comment_form" method="post" action="{{ url('comments/reply') }}" accept-charset="UTF-8">
                                        {{ csrf_field() }} 
                                        <input type="hidden" name="jobs_id" id="jobs_id" value="{{$c->jobs_id}}">
                                        <input type="hidden" name="id" id="id" value="{{$c->id}}">
                                        <div id="replydiv{{$k}}" class="card card-inner" style="display:none;">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Enter Text</label>
                                                        <textarea class="form-control comment_input" name="reply_text" rows="4" cols="45" required></textarea>
                                                        <p style="margin-top: 10px;">
                                                          <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-success btn-sm">
                                                          <input type="button" name="cancel" id="cancel" value="Cancel" onclick="return fnHide('{{$k}}');" class="btn btn-danger btn-sm">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </form>

                                </div>
                            </div><br/>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>  

   

<style type="text/css">
  .card-inner{
    margin-left: 4rem;
}
.img-fluid {
    height: 60px;
    margin-top: 10px;
    margin-left: 15px;
}
.text-secondary {
    color: #727b84 !important;
}
</style>  

<script>
function fnShow(id) {
  $('#replydiv'+id).show();
}

function fnHide(id) {
  $('#replydiv'+id).hide();
}


$(document).ready(function() {
    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });   
});
</script>
@endsection
