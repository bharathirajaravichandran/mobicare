@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-user"></i>
                       My Profile
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('myprofile') }}">My Profile</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">

                	 <div class="card" id="viewprofile">                       
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>First Name</h5>
                                   <b>{{$user->first_name}}</b>
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Last Name</h5>
                                    <b>{{$user->last_name}}</b>
                                 </div>
                              </div>
                              <div class="row">                                 
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Phone number</h5>
                                    <b>{{$user->phone_country_code}}-{{$user->phone}}</b>
                               </div>
                              <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                    <b>{{$user->email}}</b>
                               </div>  
                              </div>
                              <div class="row">    
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Photo</h5>
                                     @if(isset($user->photo) && $user->photo!='')
                                      <img src="{{ url('storage/user/'.$user->photo) }}" style="width:50px; height: 60px;border: 1px solid lightgray;">
                                     @else
                                      <img src="{{ url('img/noprofile.png') }}" style="width: 50px; height: 60px;border: 1px solid lightgray;">
                                     @endif                                        
                               </div>                         
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                     <b>{{(isset($user->status) && $user->status==1)?'Active':'Inactive'}}</b>
                               </div>                                                    
                              </div>                               
                        </div>     
                        <div class=" m-t-35">
	                        <div class="form-actions form-group row">
	                            <div class="col-xl-12 text-center">
	                               <input type="button" class="btn btn-primary" id="btneditprofile" value="Edit Profile" onclick="window.location='{{ url('editprofile') }}'">
	                            </div>
	                        </div>
	                    </div>                     
                    </div>                     

                </div>
            </div>       
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 
@endsection
