@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-pencil"></i>
                       Edit Profile
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('myprofile') }}">My Profile</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <form name="user_form" id="user_form" method="post" action="{{ url('myprofile/update') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }} 
                <input type="hidden" name="hid_id" id="hid_id" value="{{$user->id}}">
            <div class="row">
                <div class="col">
                    <div class="card" id="editprofile">                       
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>First Name</h5>
                                   <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Last Name</h5>
                                   <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}" />  
                                 </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-2 input_field_sections">
                                    <h5>Phone code</h5>
                                   <select name="phone_country_code" class="form-control">
                                      <option value="">Select</option>
                                      @foreach($countries as $c)
                                      <option value="{{$c->phonecode}}" @if($c->phonecode==$user->phone_country_code) selected @endif>{{$c->phonecode}}</option>
                                      @endforeach
                                    </select>
                               </div>
                               <div class="col-sm-4 input_field_sections">
                                    <h5>Phone number</h5>
                                   <input type="text" name="phone" class="form-control" value="{{$user->phone}}" />
                                   <div style="font-size: 11px; color:blue;">Phone number should be ten digits</div>
                               </div>
                              <div class="col-sm-6 input_field_sections">
                                    <h5>Email</h5>
                                   <input type="text" name="email" class="form-control" value="{{$user->email}}" />
                               </div>  
                              </div>
                              <div class="row">
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Password</h5>
                                   <input type="password" name="password" id="password" class="form-control" />
                               </div>
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Confirm password</h5>
                                   <input type="password" name="password_confirm" id="password_confirm" class="form-control" />  
                                 </div>
                              </div>
                              <div class="row">    
                                <div class="col-sm-6 input_field_sections">
                                    <h5>Photo</h5>
                                   <input type="file" name="photo" id="photo" onchange="return imagecheck();" /><br/><br/>
                                   	@if(isset($user->photo) && $user->photo!='')
                                      <img src="{{ url('storage/user/'.$user->photo) }}" style="width:50px; height: 60px;border: 1px solid lightgray;">
                                     @else
                                      <img src="{{ url('img/noprofile.png') }}" style="width: 50px; height: 60px;border: 1px solid lightgray;">
                                     @endif 
                               </div>                         
                               <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select name="status" class="form-control">
                                      <option value="">Select</option>
                                      <option value="1" selected="">Active</option>
                                      <option value="0">Inactive</option>
                                    </select>
                               </div>                                                    
                              </div>                               
                        </div>             
                        <!-- /.row -->
	                    <div class=" m-t-35">
	                        <div class="form-actions form-group row">
	                            <div class="col-xl-12 text-center">
	                               <input type="submit" class="btn btn-primary" value="Submit">
	                               <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{ url('myprofile') }}'">
	                            </div>
	                        </div>
	                    </div>                            
                    </div>                       

                </div>
            </div>
            </form>          
           
        </div>
        <!-- /.outer -->
    </div>
</div>
<!-- startsec End --> 

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    $.validator.methods.email = function( value, element ) {
        var email = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return email.test(value);
    }

    jQuery.validator.addMethod("lettersonly", function(value, element) {
       return this.optional(element) || /^[a-z\s]+$/i.test(value);
    });

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 2MB.');

    // validate form on keyup and submit
    $("#user_form").validate({
        rules: {
            first_name: {
              required: true,
              lettersonly: true,
              maxlength: 40
            },
            last_name: {
              required: true,
              lettersonly: true,
              maxlength: 40
            },
            gender: "required",  
            phone_country_code: {
                required: true,
                number: true,
                minlength: 1
            },
            phone: {
                required: true,
                minlength:9,
                maxlength:10,
                number: true,
                remote: {
                    url: "/users/phone/check",
                    type: "get",
                    data: {
                        id: function(){return $('input[name=hid_id]').val();}
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
            email: {
                required: true,
                email: true,
                remote: {
                    url: "/users/email/check",
                    type: "get",
                    data: {
                        id: function(){return $('input[name=hid_id]').val();}
                    },
                    complete: function(data) {
                        //console.log(data);
                    }
                }
            },
         	password : {
         		   required: false,
               minlength : 5
            },
            password_confirm : {
            	  required: false,
                minlength : 5,
                equalTo : "#password"
            },
            image: {
                required: false,
                filesize: 2000000,
            },
            status: "required"    
        },
        messages: {
            first_name: {
                required: "Please enter the first name",
                lettersonly: "Please enter characters only"
            },
            last_name: {
                required: "Please enter the last name",
                lettersonly: "Please enter characters only"
            },
            gender: "Please select the gender",      
            phone_country_code: {
                required: "Please select the phone code"
            },
            phone: {
                required: "Please enter the phone number",
                remote: "Phone number already exists"
                //phoneAU: "Phone number is invalid"
            }, 
            email: {
                required: "Please enter a valid email address",
                remote: "Email already exists"
            },    
            password : {
            	required: "Please enter the password",
            },
            password_confirm : {
            	required: "Please enter the confirm password",
            },
            status: "Please select the status"                
        },
        errorPlacement: function(error, element) {
          if(element.attr("name") == "phone_country_code") { 
            error.appendTo( element.next(".select2") );
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: function (form) {    
        	if($('#photo').val()!='') {   
	        	  var ext = $('#photo').val().split(".");
    			    ext = ext[ext.length - 1].toLowerCase();
    			    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
                  var oFile = document.getElementById("photo").files[0];
                  if (oFile.size > 5242880) // 5 mb for bytes.
                  {
                      alert("File size must under 5mb!");
                      return false;
                  }
    			        form.submit();
    			    }
    			    else
    			    {
    			        alert("Only .jpeg, .jpg, .png formats are allowed");
    			        return false;
    			    }
    			} else {
    				  form.submit();
    			}
        }
    });
});

function imagecheck() {
    var ext = $('#photo').val().split(".");
    ext = ext[ext.length - 1].toLowerCase();
    if (ext == "png" || ext == "jpeg" || ext == "jpg" ) { 
        var oFile = document.getElementById("photo").files[0];
        if (oFile.size > 5242880) // 5 mb for bytes.
        {
            alert("File size must under 5mb!");
            return false;
        }
        return true;
    }
    else
    {
        alert("Only .jpeg, .jpg, .png formats are allowed");
        return false;
    }
}
</script>      
@endsection
