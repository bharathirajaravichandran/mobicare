<p>Hi {{$email}},</p>

<p>To activate your account, please use this verification code.</p>

<div>Email Verification Code : {{$code}} </div>

<p>
Thanks, <br/>
Mobicare Team
</p>