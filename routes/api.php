<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('CommanUpdate', 'App\APIController@CommanUpdate'); 

Route::post('register', 'App\APIController@register');
Route::post('login', 'App\APIController@login');
Route::post('email_verify', 'App\APIController@email_verify');
Route::post('CreateProfile', 'App\APIController@createProfile'); 
Route::post('subscription', 'App\APIController@subscription'); 


Route::middleware('jwt.auth')->group(function() {
    Route::post('LogOut', 'App\APIController@LogOut'); 
    Route::post('Createjob', 'App\APIController@Createjob'); 
	Route::post('GetProfile', 'App\APIController@GetProfile'); 
    Route::post('ProfileUpdate', 'App\APIController@ProfileUpdate');    
    Route::post('category', 'App\APIController@category');
    Route::post('subcategory', 'App\APIController@subcategory');
    Route::post('providers', 'App\APIController@providers');
    Route::post('getAllproviders', 'App\APIController@getAllproviders');
    Route::post('provider_details', 'App\APIController@provider_details');
    Route::post('GetAllJobs', 'App\APIController@GetAllJobs');   
    Route::post('job_details', 'App\APIController@job_details');
    Route::post('rating', 'App\APIController@rating');
    Route::post('reviews', 'App\APIController@reviews');  
    Route::post('Dashboard', 'App\APIController@Dashboard'); 
    Route::post('Makebooking', 'App\APIController@Makebooking'); 
    Route::post('Appointments', 'App\APIController@Appointments');
    Route::post('AppointmentSingleData', 'App\APIController@AppointmentSingleData');
    Route::post('JobBookingList', 'App\APIController@JobBookingList');
    Route::post('GetMyallChat', 'App\APIController@GetMyallChat');
    Route::post('SaveMyChat', 'App\APIController@SaveMyChat'); 
    Route::post('ReviewPost', 'App\APIController@ReviewPost'); 
    Route::post('CommentsReview', 'App\APIController@CommentsReview'); 
    Route::post('Notifications', 'App\APIController@Notifications');
    //Route::post('MycareJobs', 'App\APIController@MycareJobs');    
    Route::post('PaymentHistroy', 'App\APIController@PaymentHistroy');   
    Route::post('PricingGuide', 'App\APIController@PricingGuide');
    Route::post('Educationlist', 'App\APIController@Educationlist');      
    Route::post('Help', 'App\APIController@Help');
    Route::post('skills', 'App\APIController@skills');
    Route::post('education', 'App\APIController@Educationlist');        
    Route::post('settings', 'App\APIController@settings');   
    Route::post('setting_update', 'App\APIController@setting_update');   
    Route::post('verify_providers', 'App\APIController@verify_providers');
    Route::post('apply_jobs', 'App\APIController@apply_jobs');
    Route::post('DeleteDocument', 'App\APIController@DeleteDocument');
    Route::post('AddNotes', 'App\APIController@AddNotes');
    Route::post('Myoffers', 'App\APIController@Myoffers');
    Route::post('AddUserBankDetails', 'App\APIController@AddUserBankDetails');
    Route::post('GetUserBankDetails', 'App\APIController@GetUserBankDetails');
    Route::post('payment', 'App\APIController@payment');
});