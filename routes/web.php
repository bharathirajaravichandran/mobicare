<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    if(!Auth::check()) {
        Request::session()->put('url.intended',url()->previous());
        return view('auth/login');
    } else {
        return redirect('/dashboard');
    }
});
Route::get('/admin', function () {
    if(!Auth::check()) {
        Request::session()->put('url.intended',url()->previous());
        return view('auth/login');
    } else {
        return redirect('/dashboard');
    }
});
Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/current_time', function () {   
    echo date('Y-m-d H:i:s');
});


//admin
Route::group(['middleware' => ['auth']], function(){
    Route::get('/dashboard', 'AdminController@dashboard');
    Route::get('/logout', 'AdminController@logout');

    Route::get('/users', 'UserController@list_users');
    Route::get('users/getdata', 'UserController@get_users')->name('users/getdata');
    Route::get('/users/add', 'UserController@user_add');
    Route::post('/users/store', 'UserController@user_store');
    Route::get('/users/email/check', 'UserController@email_check');
    Route::get('/users/phone/check', 'UserController@phone_check');
    Route::get('/users/view/{id}', 'UserController@user_view');
    Route::post('/users/delete/{id}', 'UserController@user_delete');
    Route::get('/users/edit/{id}', 'UserController@user_edit');
    Route::post('/users/update', 'UserController@user_update');
    Route::post('/user/view_document', 'UserController@view_document');  
    Route::any('/subcat', 'UserController@subcategory');     

    Route::get('/category', 'CategoryController@index');
    Route::get('category/getdata', 'CategoryController@get_category')->name('category/getdata');
    Route::get('/category/add', 'CategoryController@add');
    Route::post('/category/store', 'CategoryController@store');
    Route::get('/category/name/check', 'CategoryController@category_check');    
    Route::get('/category/edit/{id}', 'CategoryController@edit');
    Route::get('/category/view/{id}', 'CategoryController@view');
    Route::post('/category/update', 'CategoryController@update');
    Route::post('/category/delete/{id}', 'CategoryController@delete');   
    Route::any('/subcategory/update/{id}', 'CategoryController@subcat_update'); 
    Route::post('/subcategory/delete/{id}', 'CategoryController@subcat_delete');     
    Route::get('/subcategory/name/check', 'CategoryController@subcategory_check');

    Route::get('/subcategory', 'CategoryController@subcategoryindex');
    Route::get('subcategory/getsubdata', 'CategoryController@getsubdata')->name('subcategory/getsubdata');
    Route::get('/subcategory/add', 'CategoryController@subcategory');
    Route::get('/subcategory/edit/{id}', 'CategoryController@subedit');
    Route::get('/subcategory/namecheck/{name}/{id}', 'CategoryController@subcategory_check'); 
    Route::post('/category/subcategory/store', 'CategoryController@subcategory_store');
    Route::get('/subcategory/getsubcategory/{id}', 'CategoryController@getsubcategory');
    Route::get('/subcategory/getskills/{id}', 'CategoryController@getskills');
    Route::get('/subcategory/view/{id}', 'CategoryController@subview');


    Route::get('/certificate', 'CertificateController@index');
    Route::get('certificate/getdata', 'CertificateController@get_certificate')->name('certificate/getdata');
    Route::get('/certificate/add', 'CertificateController@add');
    Route::post('/certificate/store', 'CertificateController@store');
    Route::get('/certificate/edit/{id}', 'CertificateController@edit');
    Route::post('/certificate/update', 'CertificateController@update');
    Route::get('/certificate/view/{id}', 'CertificateController@view');
    Route::post('/certificate/delete/{id}', 'CertificateController@delete');

    Route::get('/care_request', 'CareRequestController@index');
    Route::get('care_request/getdata', 'CareRequestController@get_care_request')->name('care_request/getdata');
    Route::get('/care_request/add', 'CareRequestController@add');
    Route::post('/care_request/store', 'CareRequestController@store');
    Route::get('/care_request/name/check', 'CareRequestController@care_request_name_check'); 
    Route::get('/care_request/edit/{id}', 'CareRequestController@edit');
    Route::post('/care_request/update', 'CareRequestController@update');
    Route::get('/care_request/view/{id}', 'CareRequestController@view');
    Route::post('/care_request/delete/{id}', 'CareRequestController@delete'); 
    Route::get('/care_request/match/{id}', 'CareRequestController@match'); 
    Route::post('/care_request/matchstore', 'CareRequestController@matchstore');
    

    Route::get('/comments', 'CommentsController@index'); 
    Route::get('comments/getdata', 'CommentsController@get_comments')->name('comments/getdata');
    Route::get('/comments/view/{id}', 'CommentsController@view');
    Route::post('/comments/reply', 'CommentsController@reply'); 

    Route::get('/admin/bank_accounts', 'BankaccountController@index');
    Route::get('/admin/bank_accounts/getdata', 'BankaccountController@get_bank_account')->name('admin/bank_accounts/getdata');
    Route::get('/admin/bank_accounts/add', 'BankaccountController@add');
    Route::post('/admin/bank_accounts/store', 'BankaccountController@store');
    Route::get('/admin/bank_accounts/name/check', 'BankaccountController@bank_name_check');    
    Route::get('/admin/bank_accounts/number/check', 'BankaccountController@bank_number_check');    
    Route::get('/admin/bank_accounts/edit/{id}', 'BankaccountController@edit');
    Route::post('/admin/bank_accounts/update', 'BankaccountController@update');
    Route::get('/admin/bank_accounts/view/{id}', 'BankaccountController@view');
    Route::post('/admin/bank_accounts/delete/{id}', 'BankaccountController@delete');

    Route::get('/admin/commission', 'CommissionController@index');
    Route::get('/admincommission/getdata', 'CommissionController@get_commission')->name('admin/commission/getdata');
    Route::get('/admin/commission/add', 'CommissionController@add');
    Route::post('/admin/commission/store', 'CommissionController@store');
    Route::get('/admin/commission/name/check', 'CommissionController@commission_name_check');  
    Route::get('/admin/commission/edit/{id}', 'CommissionController@edit');
    Route::post('/admin/commission/update', 'CommissionController@update');
    Route::post('/admin/commission/delete/{id}', 'CommissionController@delete');

    Route::get('/resources', 'ResourcesController@index');
    Route::get('resources/getdata', 'ResourcesController@get_resources')->name('resources/getdata');
    Route::get('/resources/add', 'ResourcesController@add');
    Route::post('/resources/store', 'ResourcesController@store');
    Route::get('/resources/title/check', 'ResourcesController@title_check');  
    Route::get('/resources/edit/{id}', 'ResourcesController@edit');
    Route::post('/resources/update', 'ResourcesController@update');
    Route::get('/resources/view/{id}', 'ResourcesController@view');
    Route::post('/resources/delete/{id}', 'ResourcesController@delete');

    Route::get('/cms', 'CmsController@index');
    Route::get('cms/getdata', 'CmsController@get_cms')->name('cms/getdata');
    Route::get('/cms/add', 'CmsController@add');
    Route::post('/cms/store', 'CmsController@store');
    Route::get('/cms/name/check', 'CmsController@category_check');    
    Route::get('/cms/edit/{id}', 'CmsController@edit');
    Route::post('/cms/update', 'CmsController@update');
    Route::get('/cms/view/{id}', 'CmsController@view');
    Route::post('/cms/delete/{id}', 'CmsController@delete');

    Route::get('/help', 'HelpController@index');
    Route::get('help/getdata', 'HelpController@get_cms')->name('help/getdata');
    Route::get('/help/add', 'HelpController@add');
    Route::post('/help/store', 'HelpController@store');
    Route::get('/help/name/check', 'HelpController@category_check');    
    Route::get('/help/edit/{id}', 'HelpController@edit');
    Route::post('/help/update', 'HelpController@update');
    Route::get('/help/view/{id}', 'HelpController@view');
    Route::post('/help/delete/{id}', 'HelpController@delete');


    Route::get('/admin/users', 'AdminuserController@index');
    Route::get('admin/users/getdata', 'AdminuserController@get_users')->name('admin/users/getdata');
    Route::get('/admin/users/add', 'AdminuserController@add');
    Route::post('/admin/users/store', 'AdminuserController@store');
    Route::get('/admin/users/edit/{id}', 'AdminuserController@edit');
    Route::post('/admin/users/update', 'AdminuserController@update');
    Route::get('/admin/users/{string}/{id}', 'AdminuserController@lock');
    Route::get('/admin/user/view/{id}', 'AdminuserController@view');
    Route::post('/admin/users/delete/{id}', 'AdminuserController@delete');


    Route::get('/subscription', 'SubscriptionController@index');
    Route::get('subscription/getdata', 'SubscriptionController@get_subscription')->name('subscription/getdata');
    Route::get('/subscription/name/check', 'SubscriptionController@subscription_name_check'); 
    Route::get('/subscription/add', 'SubscriptionController@add');
    Route::post('/subscription/store', 'SubscriptionController@store');
    Route::get('/subscription/edit/{id}', 'SubscriptionController@edit');
    Route::post('/subscription/update/{id}', 'SubscriptionController@update');
    Route::get('/subscription/view/{id}', 'SubscriptionController@show');
    Route::post('/subscription/delete/{id}', 'SubscriptionController@delete');

    Route::get('/skills', 'SkillsController@index');
    Route::get('skills/getdata', 'SkillsController@get_list')->name('skills/getdata');
    Route::get('/skills/add', 'SkillsController@add');
    Route::post('/skills/store', 'SkillsController@store');
    Route::get('/skills/edit/{id}', 'SkillsController@edit');
    Route::post('/skills/update/{id}', 'SkillsController@update');
    Route::get('/skills/view/{id}', 'SkillsController@show');
    Route::post('/skills/delete/{id}', 'SkillsController@delete');
    Route::get('/skills/name/check', 'SkillsController@namecheck');

    Route::get('/questions', 'QuestionsController@index');
    Route::get('questions/getdata', 'QuestionsController@get_list')->name('questions/getdata');
    Route::get('/questions/name/check', 'QuestionsController@questions_name_check'); 
    Route::get('/questions/add', 'QuestionsController@add');
    Route::post('/questions/store', 'QuestionsController@store');
    Route::get('/questions/edit/{id}', 'QuestionsController@edit');
    Route::post('/questions/update/{id}', 'QuestionsController@update');
    Route::get('/questions/view/{id}', 'QuestionsController@show');
    Route::post('/questions/delete/{id}', 'QuestionsController@delete');

    Route::get('/admin/user_types', 'UsertypesController@index');
    Route::get('/admin/user_types/getdata', 'UsertypesController@get_usertypes')->name('usertypes/getdata');
    Route::get('/admin/user_types/add', 'UsertypesController@add');
    Route::post('/admin/user_types/store', 'UsertypesController@store');
    Route::get('/admin/user_types/name/check', 'UsertypesController@name_check');    
    Route::get('/admin/user_types/edit/{id}', 'UsertypesController@edit');
    Route::post('/admin/user_types/update', 'UsertypesController@update');
    //Route::post('/admin/user_types/delete/{id}', 'UsertypesController@delete');     

    Route::get('/admin/site_settings', 'SitesettingController@index');
    Route::get('/admin/site_settings/getdata', 'SitesettingController@get_sitesetting')->name('admin/site_settings/getdata');  
    Route::get('/admin/site_settings/edit/{id}', 'SitesettingController@edit');
    Route::post('/admin/site_settings/update', 'SitesettingController@update');

    Route::get('/myprofile', 'HomeController@myprofile');
    Route::get('/editprofile', 'HomeController@editprofile');
    Route::post('/myprofile/update', 'HomeController@update');
});

Route::get('/info/{msg}/{module}', function () { return view('info'); });