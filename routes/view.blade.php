@extends('layouts.admin')

@section('content')
<div id="content" class="bg-container">
    <header class="head">
        <div class="main-bar">
           <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                   <h4 class="nav_top_align">
                       <i class="fa fa-eye"></i>
                       View Category
                   </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                   <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                       <li class="breadcrumb-item">
                           <a href="{{ url('admin') }}">
                               <i class="fa fa-home" data-pack="default" data-tags=""></i>
                               Dashboard
                           </a>
                       </li>
                       <li class="breadcrumb-item">
                           <a href="{{ url('category') }}">Category</a>
                       </li>
                     
                   </ol>
               </div>
           </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container forms">
            <div class="row">
                <div class="col">       

                    <div class="card">
                        <h5 style="background: #eaeaea;padding: 10px;">Category <a href="{{url('category/subcategory/'.$category->id)}}" class="btn btn-primary btn-sm" style="float: right;">Add subcategory</a></h5>
                        <div class="card-body">                            
                              <div class="row">
                                  <div class="col-sm-6 input_field_sections">
                                      <h5>Name</h5>
                                     <strong>{{$category->name}}</strong>                                       
                                 </div>     
                                  <div class="col-sm-6 input_field_sections">
                                      <h5>Image</h5>
                                      @if(isset($category->image) && $category->image!='')
                                      <img src="{{ url('storage/category/'.$category->image) }}" style="width:50px;height: 60px;">
                                      @else
                                      ---
                                      @endif                        
                                 </div>     
                              </div>
                              <br/>
                        </div>
                    </div>   

                    <div>&nbsp;</div>     
                    
                    @if(count($subcategory)>0)
                      @foreach($subcategory as $key => $c)
                        @if(Request::get('subcat')==$c->id)
                            @php $file = $c->certificates_uploaded; $file = explode(',', $file); @endphp
                            <form name="category_form" id="category_form" method="post" action="{{url('subcategory/update')}}" enctype="multipart/form-data">
                              {{ csrf_field() }} 
                              <input type="hidden" name="parent_id" value="{{Request::segment(3)}}">
                              <input type="hidden" name="hid_id" value="{{Request::get('subcat')}}">
                            <div class="card">   
                            <h5 style="background: #eaeaea;padding: 10px;">Sub category #{{++$key}}</h5>                    
                            <div class="card-body">                            
                                <div class="row">
                                  <div class="col-sm-3 input_field_sections">
                                      <h5>Name</h5>
                                      <input type="text" class="form-control" name="subcat_name" value="{{$c->name}}">
                                 </div>
                                  <div class="col-sm-3 input_field_sections">
                                      <h5>Image</h5>
                                      <strong>
                                      @if(isset($c->image) && $c->image!='')
                                      <input type="file" name="subcat_image">
                                      <img src="{{ url('storage/subcategory/'.$c->image) }}" style="width:40px;height: 50px;">
                                      @else
                                      <input type="file" name="subcat_image">
                                      @endif 
                                    </strong>  
                                 </div>
                                  <div class="col-sm-3 input_field_sections">
                                      <h5>Status</h5>
                                       <select class="form-control" name="subcat_status">
                                       <option value="">-Select Status-</option>
                                       <option value="1"  @if($c->status=="1") selected @endif>Active</option>
                                       <option value="0" @if($c->status=="0") selected @endif>Inactive</option>
                                      </select>
                                   </div>
                                   <div class="col-sm-3 input_field_sections">
                                      <h5>Minimum fee</h5>
                                      <input type="text" class="form-control" name="minimum_fees" value="{{$c->minimum_fees}}">
                                 </div>
                                </div>
                                <div class="row">                         
                                  <div class="col-sm-3 input_field_sections">
                                      <h5>Competent skills</h5>
                                      <input type="text" class="form-control" name="competent_skills" value="{{$c->competent_skills}}">
                                 </div>
                                  <div class="col-sm-3 input_field_sections">
                                      <h5>100pt ID check required</h5>
                                      <input type="radio" name="hundred_pt_check" value="1" @if($c->hundred_pt_check=='1') checked @endif>Yes
                                      <input type="radio" name="hundred_pt_check" value="0" @if($c->hundred_pt_check=='0') checked @endif>No                                      
                                 </div>
                                 <div class="col-sm-3 input_field_sections">
                                      <h5>Police Check required</h5>
                                      <input type="radio" name="poice_check" value="1" @if($c->police_check=='1') checked @endif>Yes
                                      <input type="radio" name="poice_check" value="0" @if($c->police_check=='0') checked @endif>No
                                 </div>
                                </div>      

                                 <div id="100_pt_check"><br/>
                                  <h5>100pt ID check</h5>
                                  <table class="table">
                                  <tr>
                                    <th style="width: 260px;">Name</th>
                                    <th>Primary</th>
                                    <th>Points</th>
                                    <th>Add Certificate</th>
                                  </tr>
                                  </tr>
                                  @foreach($view_document as $d)
                                  <tr>
                                    <td>{{$d->name}}</td>
                                     <td>
                                      @if($d->is_primary==1)
                                        Yes
                                      @else
                                        No
                                      @endif
                                    </td>
                                    <td>{{$d->points}}</td>
                                    <td><input type="checkbox" name="chk[]" @if(in_array($d->id, $file)) checked @endif value="{{$d->id}}"></td>
                                  </tr>
                                  @endforeach
                                </table>
                              </div>               

                                <div class="col-xl-12 text-center">
                                   <input type="submit" class="btn btn-primary" value="Submit">&nbsp;&nbsp;
                                    <input type="button" class="btn btn-default" value="Cancel" onclick="window.location='{{url('category/view')}}/{{Request::segment(3)}}'">
                                </div>
                                   
                            </div> 
                            </div> 
                            </form> 
                        @else
                            <div class="card">   
                                <h5 style="background: #eaeaea;padding: 10px;">Sub category #{{++$key}}
                                  <a href="{{url('category/view')}}/{{Request::segment(3)}}?subcat={{$c->id}}" class="btn btn-success btn-sm" style="float: right;">Edit</a></h5>                    
                                <div class="card-body">                            
                                    <div class="row">
                                      <div class="col-sm-3 input_field_sections">
                                          <h5>Name</h5>
                                          <strong>{{$c->name}}</strong>                                      
                                     </div>
                                      <div class="col-sm-3 input_field_sections">
                                          <h5>Image</h5>
                                          <strong>
                                          @if(isset($c->image) && $c->image!='')
                                          <img src="{{ url('storage/subcategory/'.$c->image) }}" style="width:50px;height: 60px;">
                                          @else
                                          ---
                                          @endif 
                                        </strong>  
                                     </div>
                                      <div class="col-sm-3 input_field_sections">
                                          <h5>Status</h5>
                                          <strong>@if($c->status==1) Active @else Inactive @endif</strong>
                                       </div>
                                       <div class="col-sm-3 input_field_sections">
                                          <h5>Minimum fee</h5>
                                          <strong>{{$c->minimum_fees}}</strong>
                                     </div>
                                    </div>
                                    <div class="row">                         
                                      <div class="col-sm-3 input_field_sections">
                                          <h5>Competent skills</h5>
                                          <strong>{{$c->competent_skills}}</strong>
                                     </div>
                                      <div class="col-sm-3 input_field_sections">
                                          <h5>100pt ID check required</h5>
                                          <strong>@if($c->hundred_pt_check==1) Yes @else No @endif</strong>
                                     </div>
                                     <div class="col-sm-3 input_field_sections">
                                          <h5>Police Check required</h5>
                                          <strong>@if($c->poice_check==1) Yes @else No @endif</strong>
                                     </div>
                                    </div>      
                                </div> 
                            </div>   
                        @endif
                        <br/>
                    @endforeach
                    @endif                  

                </div>
            </div>
           
        </div>
        <!-- /.outer -->
    </div>    

<style type="text/css">
.table th, .table td { padding: 0px; }
</style>

<script>
$(document).ready(function() {

    $(':input').change(function() {
        $(this).val($(this).val().trim());
    });

    jQuery.validator.addMethod(
        "money",
        function(value, element) {
            var isValidMoney = /^\d{0,4}(\.\d{0,2})?$/.test(value);
            return this.optional(element) || isValidMoney;
        },
        "Please enter a valid minimum fee"
    );

    // validate form on keyup and submit
    $("#category_form").validate({
        rules: {
            subcat_name: { 
                required: true
            },
            subcat_status: {
                required: true
            },
            minimum_fees: {
                required: true,
                money: true
            },
            hundred_pt_check: {
                required: true
            }
        },
        messages: {
            subcat_name: { 
                required: "Please enter the name"
            },
            subcat_status: {
                required: 'Please select the status'
            },
            minimum_fees: {
                required: "Please enter minimum fees"
            },
            hundred_pt_check: {
                required: 'Please select this field'
            }                          
        }
    });
});
</script> 
@endsection
