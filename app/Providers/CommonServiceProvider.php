<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use App\Http\Requests;
use Request;
use App\Models\UserPermissions;
use App\Models\Modules;


class CommonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    { 			
        view()->composer('*', function ($view)
        {   
            if (Auth::user() !='')
            {    
                //check user permission
                $user_id = Auth::user()->id;
                $user = UserPermissions::join('modules', 'modules.id', '=', 'user_permissions.module_id')->where('user_permissions.user_id', $user_id)->get(); 
                $segment = Request::segment(1);
                $modules = Modules::where('link', $segment)->first();
                $module_id = isset($modules->id)?$modules->id:'';
                $module_name = isset($modules->module_name)?$modules->module_name:'';
                $segment2 = Request::segment(2);

                foreach ($user as $key => $value) {

                    if($module_name=='Education / Resources') {
                        $module_name = str_replace('/', '', $module_name);
                    }

                    if(isset($segment2) && $segment2!='' && $segment2=='add') {
                        if($value->add!=1 && $value->module_id==$module_id) {
                           $url = url('');
                           header('Location: '.$url.'/info/add/'.$module_name.'');
                           exit;
                        }                   
                    }

                    if(isset($segment2) && $segment2!='' && $segment2=='edit') {
                        if($value->edit!=1 && $value->module_id==$module_id) {
                           $url = url('');
                           header('Location: '.$url.'/info/edit/'.$module_name.'');
                           exit;
                        }                   
                    }

                    if(($value->view==0&&$value->add==0&&$value->edit==0&&$value->delete==0) && $value->module_id==$module_id) {
                       $url = url('');
                       header('Location: '.$url.'/info/view/'.$module_name.'');
                       exit;
                    }
                }

                //redirect to login if admin blocked
                $user_type = Auth::user()->user_types_id;
                $blocked_status = Auth::user()->blocked_status;
                if($user_type==5 && $blocked_status==1) {
                    return redirect('/logout')->send();    
                }
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
