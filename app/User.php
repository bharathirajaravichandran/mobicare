<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','email','password','email_verify','verify_code','category','subcategory','fee','description','bio','join_date','location','photo','gender','age','verified_status','health_condition','blocked_status','phone','video','phone_country_code','latitude','longitude','address','user_types_id','qualification','experience','company','aphra_no','city_id','state_id','countries_id','is_certified','status','fcm_token','login_type','facebook_id','gplus_id','subscription_id','selected_skills','availability','created_at','updated_at','deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function user_types()
    {
        return $this->belongsTo('App\Models\UserTypes');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'industry');
    }

    public function user_documents()
    {
        return $this->hasMany('App\Models\UserDocuments');
    }

    public function job_conversations()
    {
        return $this->hasMany('App\Models\JobConversations');
    }

    public function user_permissions()
    {
        return $this->hasMany('App\Models\UserPermissions');
    }

    public function job_assigned()
    {
        return $this->hasMany('App\Models\JobAssigned');
    }

    public function getAvailabilityAttribute($availability)
    {        
       
            $working_days = explode(',',$availability);   
            if(count(array_filter($working_days)) ==0)
                $working_days = array(7);
                $days = array(0, 1, 2, 3, 4, 5, 6);
                foreach ($days as $key=>$value) {                              
                  if(in_array($value,$working_days))
                     $availability_days[] = 1 ;
                  else
                    $availability_days[]  = 0 ;
                      
                }
                return $availability_days;
        
    }
}
