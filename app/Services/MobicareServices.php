<?php 
namespace App\Services; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Category;
use App\Models\Jobs;
use App\Models\Notes;
use App\Models\JobConversations;
use App\Models\JobPaymentsLogs;
use App\Models\JobAssigned;
use App\Models\JobReviews;
use App\Models\JobPayments;
use App\Models\Help;
use App\Models\Skills;
use App\Models\Resources;
use App\Models\Subscription;
use App\Models\Questions;
use App\Models\Cms;
use App\Models\UserSettings;
use App\Models\UserSubscription;
use App\Models\Sitesetting;
use App\Models\UserDocuments;
use App\Models\JobProposals;
use App\Models\UserBankaccounts;
use App;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;
use curl;

class MobicareServices {

   /* error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/

  /**
   * [getAuthUser description]
   * @param  [type] $token [description]
   * @return [type]        [description]
   */
  public function getAuthUser($token){
    $user = JWTAuth::toUser($token); 
    return $user;
  }

  /**
   * Refresh the JWT token using the expired one
   * @param  Request $request [description]
   * @return [type]           [description] 
   */
  public function refreshToken(Request $request) {
    $token = $request->token;
    try {
      if (! $user = JWTAuth::parseToken()->authenticate()) {
        return response()->json([config('resource.user_not_found')],config('resource.status_404'));
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
      $refreshedToken = JWTAuth::refresh($token);
      $token = $refreshedToken;
      return response()->json(compact('token'));
    }
    return response()->json([config('resource.user_not_found')],config('resource.status_404'));
  }

  /**
   * this function used to get the user id from the Token
   * @param  [type] $token [description]
   * @return [type]        [description]
   */
  private function getUserId($token) {
    $user = $this->getAuthUser($token);   
  }


public function createProfile(Request $request) {
     $questions = Questions::where('type','signup')->where('deleted_at','=',null)->where('status',1)->get();               
     if(!$questions)
        return response()->json(['status' => 204, 'message' => 'No Question'], 200);
     foreach ($questions as $key => $value) {
      if($value->answer_type =='textbox')
        $answer_id = 1;
      elseif($value->answer_type =='radio')
        $answer_id = 2;
      elseif($value->answer_type =='selectbox')
        $answer_id = 3;
      elseif($value->answer_type == 'textarea')
        $answer_id = 4;
      elseif($value->answer_type == 'checkbox')
        $answer_id = 5;
      else
        $answer_id = 0 ;

       $value->answer_id = $answer_id;
       $value->user_type = $value->user_type;
     }
     return response()->json(['status' => 200,'message' => 'List Questions', 'data' => array_null_values_replace($questions->toArray())]);
}

  
public function register(Request $request){
      $input = $request->all();
      $validator = Validator::make($request->all(), [
          'first_name'      => 'required|max:255',
          'last_name'       => 'required|max:255',            
          'email'           => 'required|max:255',            
          'login_type'      => 'required|max:255',
          'user_types_id'   => 'required|max:255',                      
      ]);
      if ($validator->fails()) {
          return response()->json(['status' => 500, 'message' => 'Values missing'], 200);
          /*return response()->json(['status' => 500, 'message' => $validator->messages()], 200);*/
      }

      $email_exists = count(User::where('email','=',$input['email'])->get());
      if($email_exists>0) {
          return response()->json(['status' => 500, 'message' => 'Email already exists'], 200);
      } else {

            if(isset($input['subscription_id']) && $input['subscription_id']!='') {
                $checksubscription = UserSubscription::where('id', $input['subscription_id'])->count();
                if($checksubscription==0) {
                    return response()->json(['status' => 500, 'message' => 'Invalid Subscription Id'], 200);
                }
            }

           if($input['login_type'] == 2){
                     $user = new User();
                     $exclude_inputs = array('photo');
                     foreach($request->all() as $key=>$value) {
                          if(!in_array($key,$exclude_inputs))
                              $user->$key = $value;
                      } 
                     if($request->filled('photo')){                         
                        $user_photo = SocialImageCopy($request->input('photo'),'fb_profilepic.jpg'); 
                    }  
                    $user->save();
           }elseif($input['login_type'] == 3){
                     $user = new User();
                     $exclude_inputs = array('photo');
                     foreach($request->all() as $key=>$value){
                          if(!in_array($key,$exclude_inputs))
                              $user->$key = $value;
                      } 
                     if($request->filled('photo')) {                    
                       $user_photo = SocialImageCopy($request->input('photo'),'gplus_profilepic.jpg');                                            
                     }  
                    $user->save();
           }else{
                    $user = User::create([
                    'first_name'    => ($input['first_name']) ? $input['first_name'] : '',
                    'last_name'     => ($input['last_name']) ? $input['last_name'] : '',                
                    'email'         => ($input['email']) ? $input['email'] : '',
                    'password'      => '',
                    'age'           => ($request->input('age')) ? $request->input('age') : 0,
                    'user_types_id' => $input['user_types_id'],
                    'login_type'    => 1,
                    'gender'        => ($request->input('gender')) ? $request->input('gender') : 0,
                    'latitude'      => ($request->input('latitude')) ? $request->input('latitude') : '0',
                    'longitude'     => ($request->input('longitude')) ? $request->input('longitude') : '0',
                    'address'       => ($request->input('address')) ? $request->input('address') : '',
                    'company'       => ($request->input('organization')) ? $request->input('organization') : '',
                    'experience'    => ($request->input('experience')) ? $request->input('experience') : '0',
                    'subscription_id' => ($request->input('subscription_id')) ? $request->input('subscription_id') : '0',
                    'status'        => 1,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'update_at'     => date('Y-m-d H:i:s')
                   ]);
           }          

          //send mail
          $password = randomPassword();
          $token = JWTAuth::fromUser($user);  
          $user_data = $this->getAuthUser($token);        
          $data = array('name'=>$user->first_name, 'email'=>$user->email, 'password'=>$password);
          $mail_send = Mail::send('emails.userlogin', $data, function($message) use($user) {
               $message->to($user->email, $user->first_name)->subject('Mobicare - User Login');
               $message->from(config('mail.username'),'Admin');
          });
          //update verify code
          $verify_code = '';
          $user_email_data = DB::table('user_email_verification')->where('email',$input['email'])->count();
                  if($user_email_data > 0 ){
                    $verify_code = DB::table('user_email_verification')->where('email',$input['email'])->first()->verification_code;
                  }
          //update verify code
          User::where('id', $user->id)->update(['password' => bcrypt($password),'verify_code' =>$verify_code]);
          // check for failures
          if (!Mail::failures()){
               User::where('id', $user->id)->update(['email_verify' => 1]);
            }
          $user_data->token = $token ;              
          return response()->json(['status' => 200,'message' => 'User created sucessfully.','data' => array_null_values_replace($user_data->toArray())])->header('jwt_token', $token);
      }                
  }

public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [            
            'email'           => 'required',
            'login_type'      => 'required',                    
        ]);
        if ($validator->fails()){              
            return response()->json(['status' => 500, 'message' => 'parameter missing'], 200);
        }  

         $email        = $request->input('email');
         $checkuser    = User::where('email',$email)->first();
         if(!$checkuser)
                 return response()->json(['status' => 500, 'message' => 'Email account does not exist']);

        if($request->input('login_type') == 2)
        {   

             //facebook              
              $facebook_id  = $request->input('facebook_id');
              $fbuser = User::where('email',$email)->orWhere('facebook_id','!=','')->orWhere('gplus_id','!=','')->first();
              if(!$fbuser)
                 return response()->json(['status' => 500, 'message' => 'The user has not been granted the requested login type']);
              $token = JWTAuth::fromUser($fbuser);
              $Logedin_user = JWTAuth::toUser($token);              
              $fbuser->facebook_id  = ($facebook_id) ?$facebook_id : '';
              $fbuser->gplus_id     = '';
              if($request->filled('photo')){                         
                    $fbuser->photo = SocialImageCopy($request->input('photo'),'fb_profilepic.jpg'); 
              }
              $fbuser->save();
              $currentUser = User::find($fbuser->id);       
              if($currentUser->status == null || $currentUser->status == 0 ){
                 return response()->json(['status' => 500, 'message' => 'User Inactive'], 200);
              }        
              User::where('id', $currentUser->id)->update(['fcm_token' => $request->post('fcm'), 'login_type' => $request->post('login_type')]);
              $currentUser->city_id         = "".$currentUser->city_id."";  $currentUser->state_id        = "".$currentUser->state_id."";
              $currentUser->countries_id    = "".$currentUser->countries_id.""; $currentUser->experience      = "".$currentUser->experience."";
              $currentUser->subscription_id = "".$currentUser->subscription_id."";                 
              return response()->json(['status' => 200,'message' => 'User logged in sucessfully.','data' => array_null_values_replace($currentUser->toArray()),'jwt_token'=>$token])->header('jwt_token', $token);             
            //facebook
        }else if($request->input('login_type') == 3)
        {   
         
        //googleplus              
              $gplus_id     = $request->input('gplus_id');
              $gplususer = User::where('email',$email)->orWhere('gplus_id','!=','')->orWhere('facebook_id','!=','')->first();
              if(!$gplususer)
                 return response()->json(['status' => 500, 'message' => 'The user has not been granted the requested login type']);
              $token = JWTAuth::fromUser($gplususer);
              $Logedin_user = JWTAuth::toUser($token);              
              $gplususer->facebook_id  = '';
              $gplususer->gplus_id     = ($gplus_id) ? $gplus_id : '';
              if($request->filled('photo')){                         
                    $gplususer->photo = SocialImageCopy($request->input('photo'),'gplus_profilepic.jpg'); 
              }  
              $gplususer->save();
              $currentUser = User::find($gplususer->id);        
              if($currentUser->status == null || $currentUser->status == 0 ){
                 return response()->json(['status' => 500, 'message' => 'User Inactive'], 200);
              }        
              User::where('id', $currentUser->id)->update(['fcm_token' => $request->post('fcm'), 'login_type' => $request->post('login_type')]);
              $currentUser->city_id         = "".$currentUser->city_id."";  $currentUser->state_id        = "".$currentUser->state_id."";
              $currentUser->countries_id    = "".$currentUser->countries_id.""; $currentUser->experience      = "".$currentUser->experience."";
              $currentUser->subscription_id = "".$currentUser->subscription_id."";                
              return response()->json(['status' => 200,'message' => 'User logged in sucessfully.','data' => array_null_values_replace($currentUser->toArray()),'jwt_token'=>$token])->header('jwt_token', $token);  
            //googleplus
        }
     
        // noraml user login               
       $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 500, 'message' => 'Invalid credentials']);
            } else {
                $currentUser = Auth::user();
                /*dd($currentUser->status);*/
                if($currentUser->status == null || $currentUser->status == 0 ){
                   return response()->json(['status' => 500, 'message' => 'User Inactive'], 200);
                }        
                User::where('id', $currentUser->id)->update(['fcm_token' => $request->post('fcm'), 'login_type' => $request->post('login_type')]);
                $currentUser->city_id         = "".$currentUser->city_id."";  $currentUser->state_id        = "".$currentUser->state_id."";
                $currentUser->countries_id    = "".$currentUser->countries_id.""; $currentUser->experience      = "".$currentUser->experience."";
                $currentUser->subscription_id = "".$currentUser->subscription_id."";                 
                return response()->json(['status' => 200,'message' => 'User logged in sucessfully.','data' => array_null_values_replace($currentUser->toArray()),'jwt_token'=>$token])->header('jwt_token', $token);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => 500, 'message' => 'could not create token']);
        }        
}

  public function LogOut(Request $request) 
   {
      JWTAuth::invalidate();
      return response(['status' => 200, 'message' => 'Logged out Successfully.'], 200);
   }

  public function email_verify(Request $request)
    {
        $email = $request->post('email');
        if($email !='')
        {
            $verification_code = $request->post('verification_code');
            if(isset($email) && isset($verification_code))
              {       
                    //check data
                    $user_email_data = DB::table('user_email_verification')->where('verification_code',$verification_code)->where('email',$email)->count();
                    if($user_email_data > 0 ){
                      return response()->json(['status' => 200, 'message' => 'Email has been verified Successfully.']);
                    }else{
                      return response()->json(['status' => 500, 'message' => 'Verification code is Incorrect.']);
                    }
                    //check data

              } else if(isset($email)) {
                    $code_check = User::where('email',$email)->count();           
                    if($code_check>0) {
                        return response()->json(['status' => 500,'message' => 'Email address already exists.'],200);
                    }            
                    $code = generateNumericOTP();
                    $data = array('email'=>$email, 'code'=>$code);
                    $mail_send = Mail::send('emails.emailverify', $data, function($message) use($data) {
                         $message->to($data['email'])->subject('Mobicare - Email Verify');
                         $message->from(config('mail.username'),'Admin');
                    });
                    // check for failures
                    if (Mail::failures()) {
                        return response()->json(['status' => 500,'message' => 'We\'ve got email errors!.'], 200);
                    }   
                    //save data
                    $user_email_data = DB::table('user_email_verification')->where('email',$email)->count();
                    if($user_email_data > 0 ){
                      DB::table('user_email_verification')->where('email',$email)->update(['verification_code' =>$code,'created_at' =>Carbon::now(),'updated_at' =>Carbon::now()]);
                    }else{
                      DB::table('user_email_verification')->insert(['email' => $email, 'verification_code' =>$code,'created_at' =>Carbon::now(),'updated_at' =>Carbon::now()]);
                    }
                    //save data
                    return response()->json(['status' => 200, 'message' => 'Email verification code sent Successfully.']);

                    } else {
                        return response()->json(['status' => 500, 'message' => 'Parameter Wrong']);
                    }   

         }else{
                 return response()->json(['status' => 500, 'message' => 'Email is required']);
              }
    }


public function GetProfile(Request $request) 
    {                     
          $user_data = JWTAuth::toUser();                           
          if(!$user_data){
               return response()->json(['status' => 204, 'message' => 'No User']);
             }else {
                $user_data->photo = isset($user_data->photo) && $user_data->photo != '' ? url('storage/user/'.$user_data->photo) : url('storage/user/noprofile.png');              
               return response()->json(['status' => 200, 'message' => 'User Data', 'data' => array_null_values_replace($user_data->toArray())]);
            }        
    }

public function ProfileUpdate(Request $request) 
    {
           $user = JWTAuth::toUser();       
           if(!$user)
                return response()->json(['status' => 204, 'message' => 'No User']);  
            if(User::where('email',$request->email)->where('id','!=',$user->id)->count() > 0){
              return response()->json(['status' => 500, 'message' => 'Email Already exists']);
            }
           $user_data = User::find($user->id);
           $user_data->first_name    = $request->input('first_name') ? $request->input('first_name') : $user_data->first_name ;
           $user_data->last_name     = $request->input('last_name') ? $request->input('last_name') : $user_data->last_name ;
           $user_data->email         = $request->input('email') ? $request->input('email') : $user_data->email ;
           $user_data->phone         = $request->input('phone') ? $request->input('phone') : $user_data->phone ;
           $user_data->latitude      = $request->input('latitude') ? $request->input('latitude') : $user_data->latitude ;
           $user_data->longitude     = $request->input('longitude') ? $request->input('longitude') : $user_data->longitude ;
           $user_data->address       = $request->input('address') ? $request->input('address') : $user_data->address ;
           $user_data->location      = $request->input('location') ? $request->input('location') : $user_data->location ;          
           /*$exclude_inputs = array('photo');
           foreach($request->all() as $key=>$value) {
                if(!in_array($key,$exclude_inputs))
                    $user_data->$key = $value;
            } */

           if(!empty($_FILES)) {            
             // $photo_name = file_upload($request->input('photo'),'public/user',$user_data->photo);             
             // $user_photo = array('photo' => $photo_name);
             // $user_data->location  = $user_photo ;
             /*User::where('id',$user->id)->update($user_photo);*/

              $file = $request->file('photo');
              $file->store('user', ['disk' => 'public']);
              $filename = $request->photo->hashName();
              $user_photo = array('photo' => $filename);
              $user_data->photo = $filename ;

              /*User::where('id', $user->id)->update($user_photo);*/
           }  

          $user_data->save();
          $user_data = User::find($user_data->id); 
          $user_data->photo = isset($user_data->photo) && $user_data->photo != '' ? url('storage/user/'.$user_data->photo) : url('storage/user/noprofile.png');  
        return response()->json(['status' => 200, 'message' => 'Updated sucessfully', 'data' => array_null_values_replace($user_data->toArray())]);                 
    }

public function category(Request $request) 
    {  
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;        
        $search = $request->has('search') ? $request->get('search') : null;     
        $category = Category::where('parent_category', 0)->where('name','LIKE','%'.$search.'%')
                            ->limit($limit)->offset(($page - 1) * $limit)->get(); 
        if(count($category)>0) {
            foreach($category as $key=>$row) {   
                      if(!$row->minimum_fees) { $row->minimum_fees = 0 ; }  
                      if($row->police_check) { $row->police_check = "".$row->police_check.""; }  
                      if($row->hundred_pt_check) { $row->hundred_pt_check = "".$row->hundred_pt_check.""; }                        
                      if($row->competent_skills) { $row->competent_skills = "".$row->competent_skills.""; }                        
                      $row->image_path = url('storage/category/'.$row->image) ;              
            }
            $totalpages = Category::where('parent_category', 0)->where('name','LIKE','%'.$search.'%')->count();
            $numberOfPages = (int) ceil($totalpages / $limit);
            return response()->json(['status' => 200,'total'=>$numberOfPages,'data' => array_null_values_replace($category->toArray())]);
        } else { 
                 return response()->json(['status' => 204, 'message' => 'No category found']);
        }
    }

public function subcategory(Request $request) 
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $search = $request->has('search') ? $request->get('search') : null;
        $id = $request->input('category_id');
        if(isset($id)) {
            $category = Category::find($id);
             if(!$category)
                return response()->json(['status' => 204, 'message' => 'No category found']);
            $subcategory = Category::where('parent_category', $id)->where('name','LIKE','%'.$search.'%')
                                   ->limit($limit)->offset(($page - 1) * $limit)->get(); 
            if(count($subcategory)>0) {            
                $subcat['category'][] = array(
                    'category_id' => $category->id,
                    'category_name' => $category->name,
                    'category_image' => url('storage/category/'.$category->image),
                    'description' => $category->description
                );
                foreach($subcategory as $row) {  
                    if($row->image=='') 
                        $image = ""; 
                    else 
                        $image = url('storage/subcategory/'.$row->image);    

                    $subcat['subcategory'][] = array(
                        'subcategory_id' => $row->id,
                        'subcategory_name' => $row->name,
                        'subcategory_image' => $image
                    );
                }
                $totalpages = Category::where('parent_category',$id)->where('name','LIKE','%'.$search.'%')->count();
                $numberOfPages = (int) ceil($totalpages / $limit);
                return response()->json(['status' => 200,'total'=> $numberOfPages, 'data' => array_null_values_replace($subcat)]);
            }else{ 
                return response()->json(['status' => 204, 'message' => 'No sub category found']);
            }
        } else {
            return response()->json(['status' => 500, 'message' => 'Category id is required']);
        }
    }

    //screen 62
public function getAllproviders(Request $request) 
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $search = $request->has('search') ? $request->get('search') : null;
        $user = JWTAuth::toUser(); 
        if(!$user)
          return response()->json(['status' => 204, 'message' => 'No User']);  

        $user_id = $user->id; 
        $users = User::where('users.user_types_id',3)
                      ->where('users.status',1)
                      ->where('first_name','LIKE','%'.$search.'%')
                      ->limit($limit)->offset(($page - 1)*$limit)->get();   
        if(count($users)>0) {
            $data = array();
            foreach($users as $row) {          
                   $row->image_path =  isset($row->photo) && $row->photo != '' ? url('storage/user/'.$row->photo) : '';  
                   $rating = JobReviews::where('user_id',$row->id)->get()->sum('ratings');                           
                   $row->rating = $rating/5;                    
            }
            $totalpages = User::where('users.user_types_id',3)->where('users.status',1)->where('first_name','LIKE','%'.$search.'%')->count();
            $numberOfPages = (int) ceil($totalpages / $limit);
            return response()->json(['status' => 200, 'message' => 'provider data', 'total'=> $numberOfPages, 'data' => array_null_values_replace($users->toArray())]);
        }else{
            return response()->json(['status' => 204, 'message' => 'No provider found']);
        }
    }

public function providers(Request $request) 
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;

        $user = JWTAuth::toUser(); 
        if(!$user)
          return response()->json(['status' => 204, 'message' => 'No User']);  

        $user_id = $user->id; 
        $users = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                    ->join('users', 'job_assigned.user_id', '=', 'users.id')
                    ->join('industry', 'industry.id', '=', 'users.category')                         
                    ->selectRaw('job_assigned.id, users.photo, users.first_name, users.last_name, industry.name, job_assigned.amount, users.location, jobs.id as jobid, jobs.description, jobs.start_date, jobs.start_time, jobs.end_date, jobs.end_time')
                    ->where('users.id',$user_id)
                    ->limit($limit)->offset(($page - 1)*$limit)->get();

        if(count($users)>0) {
            $data = array();
            foreach($users as $row) {                  
                $data[] = array(
                  'id' => $row->id,
                  'photo' => isset($row->photo) && $row->photo != '' ? url('storage/user/'.$row->photo) : '',
                  'name' => $row->first_name." ".$row->last_name,
                  'category' => $row->name,
                  'amount' => $row->amount,
                  'location' => $row->location,
                  'job_id' => $row->jobid,
                  'description' => $row->description,
                  'start_date' => date('d M', strtotime($row->start_date)),
                  'start_time' => date('h:i', strtotime($row->start_time)),
                  'end_date' => date('d M', strtotime($row->end_date)),
                  'end_time' => date('h:i', strtotime($row->end_time))
                );               
            }
            $totalpages = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                              ->join('users', 'job_assigned.user_id', '=', 'users.id')
                              ->join('industry', 'industry.id', '=', 'users.category')                         
                              ->selectRaw('job_assigned.id, users.photo, users.first_name, users.last_name, industry.name, job_assigned.amount, users.location, jobs.id as jobid, jobs.description, jobs.start_date, jobs.start_time, jobs.end_date, jobs.end_time')
                              ->where('users.id',$user_id)->count();
            $numberOfPages = (int) ceil($totalpages / $limit);
            return response()->json(['status' => 200, 'message' => 'provider data', 'total'=> $numberOfPages, 'data' => array_null_values_replace($data)]);
        }else{
            return response()->json(['status' => 204, 'message' => 'No provider found']);
        }
    }

public function provider_details(Request $request) 
    {        
               $provider_id = $request->post('provider_id');        
        if(isset($provider_id)) {
             


                $category_list = JobAssigned::join('jobs', 'jobs_id', '=', 'jobs.id')
                                        ->join('industry', 'jobs.category_id', '=', 'industry.id')
                                        ->where('job_assigned.user_id',$provider_id)
                                        ->groupBy('jobs.category_id')
                                        ->select('industry.id','industry.name')
                                        ->get();

                $category_id = $request->has('category_id') ? $request->get('category_id') : $category_list->first()->id;


                $users = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                  ->join('users', 'job_assigned.user_id', '=', 'users.id')                                        
                  ->selectRaw('job_assigned.id, users.photo,users.video,users.first_name, users.last_name,job_assigned.amount, users.location, jobs.id as jobid, jobs.description,users.availability,jobs.start_date, jobs.start_time, jobs.end_date, jobs.end_time, users.id as userid')
                    ->where('jobs.category_id',$category_id)
                    ->where('job_assigned.user_id',$provider_id)
                    ->first();

               /* return response()->json(['status' => 204, 'message' => $category_list ]);*/

            if(count($users)>0) {
                $JobReviews = JobReviews::with('user')->where('user_id',$provider_id)->get();                 
                    if($JobReviews){
                        $ratings = JobReviews::where('user_id',$provider_id)->groupBy('jobs_id')->get()->sum('ratings'); 
                        $users->rating = $ratings/5;    
                    $users->photo =  isset($users->photo) && $users->photo != '' ? url('storage/user/'.$users->photo) : url('storage/user/noprofile.png'); 
                    $users->video =  isset($users->video) && $users->video != '' ? url('storage/user/video/'.$users->video) : "";    
                             
                             //working days add
                             $working_days = explode(',',$users->availability);   
                            if(count(array_filter($working_days)) ==0)
                                $working_days = array(7);
                                $days = array(0, 1, 2, 3, 4, 5, 6);
                                foreach ($days as $key=>$value) {                              
                                  if(in_array($value,$working_days))
                                     $availability_days[] = 1 ;
                                  else
                                    $availability_days[]  = 0 ;                                      
                                }
                              $users->availability = $availability_days;
                              //working days add

                              $users->categorylist = $category_list;
                       

                        $users->comments     = array();
                        foreach ($JobReviews as $key => $value) {                                                          
                          $rating = JobReviews::where('user_id',$value->id)->where('jobs_id',$users->jobid)->get()->sum('ratings'); 
                           $comments[] =  array(
                              'id' => $value->id,
                              'photo' => isset($value->user->photo) && $value->user->photo != '' ? url('storage/user/'.$value->user->photo) : url('storage/user/noprofile.png'),
                              'name' => $value->user->first_name." ".$value->user->last_name,
                              'comment' => $value->feedback,
                              'rating'  => $rating/5,                               
                              'date' => Carbon::createFromTimeStamp(strtotime($value->created_at))->diffForHumans()
                            );     
                           $users->comments = array_filter($comments);

                        }
                    }              
                return response()->json(['status' => 200, 'message' => 'provider details', 'data' => array_null_values_replace($users->toArray())]);
            } else {
                return response()->json(['status' => 204, 'message' => 'No Provider data']);
            }
        } else {
            return response()->json(['status' => 500, 'message' => 'Provider id is required']);
        }  
  }
 
public function Createjob(Request $request) {
  $postTypeAmount = DB::table('job_types_amount')->where('status',1)->select('text','value')->get();             
  if($request->input('type') == 'get'){
      //get data 
          if($request->filled('category_id') && $request->filled('subcate_id')){
                   $category = Category::find($request->input('category_id'));
          if(!$category)
             return response()->json(['status' => 500, 'message' => 'No Category']);

          $subcategory = Category::where('id',$request->input('subcate_id')) 
                                 ->where('parent_category',$request->input('category_id'))                                  
                                 ->get(); 
          if(count($subcategory)==0)
             return response()->json(['status' => 500, 'message' => 'No subCategory']);

          foreach ($subcategory as $key => $value) {
                 $value->category_id     = $category->id;
                 $value->category_name   = $category->name;
                 $value->subcate_id      = $value->id;
                 $value->subcate_name    = $value->name;
                 $value->dropdownOptionValue  = $category->id.'/'.$value->id;
                 $value->dropdownOptionName  = $category->name.'/'.$value->name;
          }
          $questions = Questions::where('type','services')->where('answer_type','!=','selectbox')->where('answer_type','!=','radio')->where('deleted_at','=',null)->where('status',1)->get();               
               if(!$questions)
                  return response()->json(['status' => 500, 'message' => 'No Question'], 200);
          foreach ($questions as $key => $value) {

              if($value->answer_type == 'category')
               {
                   $value->category_id = $category->id;
                   $value->category_name = $category->name;

                   $value->category_subcategory = array_null_values_replace($subcategory->toArray());
              }else{
                $value->category_subcategory = array();
              }             

          }
       }else{          
          $questions = Questions::where('type','services')->where('answer_type','!=','selectbox')->where('answer_type','!=','radio')->where('deleted_at','=',null)->where('status',1)->get();                
               if(!$questions)
                  return response()->json(['status' => 500, 'message' => 'No Question'], 200);
           $category = Category::where('parent_category',0)->first();
           if(!$category)
             return response()->json(['status' => 500, 'message' => 'No Category']);
           $subcategory = Category::where('parent_category',$category->id)                                  
                                  ->get(); 
           if(count($subcategory)==0)
             return response()->json(['status' => 500, 'message' => 'No subCategory']);
          foreach ($subcategory as $key => $value) {
                 $value->category_id     = $category->id;
                 $value->category_name   = $category->name;
                 $value->subcate_id      = $value->id;
                 $value->subcate_name    = $value->name;
                 $value->dropdownOptionValue  = $category->id.'/'.$value->id;
                 $value->dropdownOptionName  = $category->name.'/'.$value->name;
          }

          $answerid = array('textbox'=>1, 'textarea'=>2, 'radio'=>3, 'selectbox'=>4, 'checkbox'=>5, 'category'=>6, 'date'=>7);

          foreach ($questions as $key => $value) {
              if($value->answer_type == 'category'){
                   $value->category_id = $category->id;
                   $value->category_name = $category->name;
                   $value->category_subcategory = array_null_values_replace($subcategory->toArray());
                 }else{
                $value->category_subcategory = array();
              }               
              $value->answer_id = $answerid[$value->answer_type];
          }
       }
       return response()->json(['status' => 200, 'message' => 'create job' ,
        'normal'=> isset($postTypeAmount) && $postTypeAmount[0]->value != '' ? $postTypeAmount[0]->value : 0 ,
        'match'=>isset($postTypeAmount) && $postTypeAmount[1]->value != '' ? $postTypeAmount[1]->value : 0 ,
        'urgent'=>isset($postTypeAmount) && $postTypeAmount[2]->value != '' ? $postTypeAmount[2]->value : 0 ,
        'data' => array_null_values_replace($questions->toArray())]);
       //get data  
     }elseif($request->input('type') == 'save')
     { //save data
                  $job_id = Jobs::count();
                  $jobs = new Jobs();
                  $jobs->type           =$request->input('type') ? $request->input('type') : '';
                  $jobs->job_id         = 'CXV'.$job_id;
                  $jobs->category_id    =$request->input('category_id') ? $request->input('category_id') : 0;
                  $jobs->subcategory_id =$request->input('subcategory_id') ? $request->input('subcategory_id') : 0;
                  $jobs->name           =$request->input('name') ? $request->input('name') : '';
                  $jobs->request_type   =$request->input('request_type') ? $request->input('request_type') : ''; 
                  if($request->filled('photo')) {            
                     $photo_name = file_upload($request->input('photo'),'public/jobs',null);             
                     $user_photo = array('photo' => $photo_name);
                     $jobs->job_file = $user_photo ;
                   }                              
                  $jobs->save();

                 //question answer save
                if($request->input('QuestionAnswer')){
                  foreach ($request->input('QuestionAnswer') as $key => $value) {
                    $answer = ""; $question = "";
                    $data = explode('/',$value); if($data){ $answer = $data[1] ; $question = $data[0]; }
                    $QuestionAnswer = new UserQuestionsAnswer();
                    $QuestionAnswer->type          = 'job';
                    $QuestionAnswer->question_id   = $question;
                    $QuestionAnswer->answer        = $answer;
                    $QuestionAnswer->user_id       = $request->input('user_id');
                    $QuestionAnswer->jobs_id       = 'CXV'.$job_id;
                    $QuestionAnswer->status        = 1 ;
                    $QuestionAnswer->created_at    = date('Y-m-d H:i:s');
                    $QuestionAnswer->updated_at    = date('Y-m-d H:i:s');
                    $QuestionAnswer->save();
                  }
                }//question answer save
                return response()->json(['status' => 200,'message'=>'create job sucessfully']);
       //save data
     }else{
        return response()->json(['status' => 500, 'message' => 'Parameter missing']);
     }

}

public function Appointments(Request $request)
    { 
        $from = $request->fromdate;          
        $to   = $request->todate;          
        $currentUser = JWTAuth::toUser();
        $id   =  $currentUser->id;   
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $search = $request->has('search') ? $request->get('search') : null;
        
        if($request->input('type') == 'all'){
          $jobs = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                        ->join('users', 'job_assigned.user_id', '=', 'users.id')                                              
                        ->select('jobs.id', 'users.first_name', 'users.last_name', 'users.photo','jobs.job_id', 'jobs.description', 'jobs.start_date', 'jobs.end_date', 'jobs.start_time', 'jobs.end_time','jobs.category_id','jobs.subcategory_id')
                        ->where('users.id',$id)                                        
                        ->limit($limit)->offset(($page - 1) * $limit)
                        ->get();
          $totalpages = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                        ->join('users', 'job_assigned.user_id', '=', 'users.id')                                              
                        ->select('jobs.id', 'users.first_name', 'users.last_name', 'users.photo','jobs.job_id', 'jobs.description', 'jobs.start_date', 'jobs.end_date', 'jobs.start_time', 'jobs.end_time','jobs.category_id','jobs.subcategory_id')
                        ->where('users.id',$id)                                            
                        ->count();
        }elseif($request->input('type') == 'filter'){
           if(isset($from) && isset($to)) { 
                  $jobs = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                        ->join('users', 'job_assigned.user_id', '=', 'users.id')                                              
                        ->select('jobs.id', 'users.first_name', 'users.last_name', 'users.photo','jobs.job_id', 'jobs.description', 'jobs.start_date', 'jobs.end_date', 'jobs.start_time', 'jobs.end_time','jobs.category_id','jobs.subcategory_id')
                        ->where('users.id',$id)                        
                        ->whereBetween('end_date',[$from, $to])                        
                        ->Where('users.first_name','LIKE','%'.$search.'%')                        
                        ->limit($limit)->offset(($page - 1) * $limit)
                        ->get();
                  $totalpages = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                        ->join('users', 'job_assigned.user_id', '=', 'users.id')                                              
                        ->select('jobs.id', 'users.first_name', 'users.last_name', 'users.photo','jobs.job_id', 'jobs.description', 'jobs.start_date', 'jobs.end_date', 'jobs.start_time', 'jobs.end_time','jobs.category_id','jobs.subcategory_id')
                        ->where('users.id',$id)                        
                        ->whereBetween('end_date',[$from, $to])                        
                        ->Where('users.first_name','LIKE','%'.$search.'%')                        
                        ->count();
             }else{
                return response()->json(['status' => 500, 'message' => 'Please select from and to date']);
            }
        }elseif($request->input('type') == 'search'){
           $jobs = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                        ->join('users', 'job_assigned.user_id', '=', 'users.id')                                              
                        ->select('jobs.id', 'users.first_name', 'users.last_name', 'users.photo','jobs.job_id', 'jobs.description', 'jobs.start_date', 'jobs.end_date', 'jobs.start_time', 'jobs.end_time','jobs.category_id','jobs.subcategory_id')
                        ->where('users.id',$id)                                        
                        ->Where('users.first_name','LIKE','%'.$search.'%')                        
                        ->limit($limit)->offset(($page - 1) * $limit)
                        ->get();
            $totalpages = Jobs::join('job_assigned', 'jobs_id', '=', 'jobs.id')
                        ->join('users', 'job_assigned.user_id', '=', 'users.id')                                              
                        ->select('jobs.id', 'users.first_name', 'users.last_name', 'users.photo','jobs.job_id', 'jobs.description', 'jobs.start_date', 'jobs.end_date', 'jobs.start_time', 'jobs.end_time','jobs.category_id','jobs.subcategory_id')
                        ->where('users.id',$id)                                        
                        ->Where('users.first_name','LIKE','%'.$search.'%')               
                        ->count();
        }else{
             return response()->json(['status' => 500, 'message' => 'Type is wrong']);
        }                  
         
            if(count($jobs)>0) {              
                foreach($jobs as $row) {  
                    if($row->photo=='') 
                        $photo = url('storage/user/noprofile.png');
                    else 
                        $photo = url('storage/user/'.$row->photo);    

                    $data[] = array(
                        'id' => $row->id,
                        'name' => $row->first_name." ".$row->last_name,
                        'photo' => $photo,                       
                        'category_name' => $this->GetCategoryData($row->category_id,'name'),                       
                        'category_id'   => $row->category_id,                       
                        'subcategory_name' => $this->GetSubcategoryData($row->subcategory_id,'name'),                     
                        'subcategory_id' => $row->subcategory_id,                    
                        'request_id' => $row->job_id,
                        'description' => $row->description,
                        'start_date' => date('d-M-Y', strtotime($row->start_date)),
                        'end_date' => date('d-M-Y', strtotime($row->end_date)),
                        'time' => date('H:i', strtotime($row->start_time)).'-'.date('H:i', strtotime($row->end_time))
                    );
                }
                
                $numberOfPages = (int) ceil($totalpages / $limit);
                return response()->json(['status' => 200,'message' => 'job data','total' => $numberOfPages,'data' => $data]);
              }else{
                return response()->json(['status' => 204, 'message' => 'No job found']);
              }
       
}


  public function AppointmentSingleData(Request $request)
    { 
          $id = $request->job_id;       
          if(isset($id) && is_numeric($id)) {
            $jobs = Jobs::with('job_conversation_list')->where('id',$id)->first();
            if(count($jobs)>0) {                
              $jobs->image = url('');              
              $jobs->date       = Carbon::parse($jobs->start_date)->formatLocalized('%d %b').'-'.Carbon::parse($jobs->end_date)->formatLocalized('%d %b');  
              $jobs->time       = Carbon::parse($jobs->start_time)->format('H:i').'-'.Carbon::parse($jobs->end_time)->format('H:i');   
              $jobs->conversation_count       = count($jobs->job_conversation_list);

              //conversation list
                 if($jobs->job_conversation_list){
                   foreach ($jobs->job_conversation_list as $key => $value) {      
                      $value->image = url('');     
                      $jobs->time       = Carbon::parse($jobs->start_time)->format('H:i').'-'.Carbon::parse($jobs->end_time)->format('H:i');    
                      $value->message_posted_time = Carbon::createFromTimeStamp(strtotime($value->created_at))->diffForHumans(); 
                   }
                 }
              //conversation list
              return response()->json(['status' => 200,'message' => 'single job data','data' => array_null_values_replace($jobs->toArray())]);
            }else{
              return response()->json(['status' => 204, 'message' => 'No job found']);
            }
        }else{
              return response()->json(['status' => 500, 'message' => 'Job Id Missing']);
        }
    }


public function GetCategoryData($id,$column_name)
  { 
            $Category = Category::find($id);
            if($Category)
             {
               return $data = $Category->$column_name;
             }else{
               return $data = '';
             }
  }
public function GetSubcategoryData($id,$column_name)
  { 
           $Category = Category::find($id);
            if($Category)
             {
               return $data = $Category->$column_name;
             }else{
               return $data = '';
             }
  }


 public function GetAllJobs(Request $request)
    {        
            $page   = $request->has('page')   ? $request->get('page') : 1;
            $limit  = $request->has('limit')  ? $request->get('limit') : 10; 
            $search = $request->has('search') ? $request->get('search') : null;
            $jobs   = Jobs::orderBy('id','desc')->limit($limit)->offset(($page - 1) * $limit)->where('name','LIKE','%'.$search.'%')->get();
            if(count($jobs)>0) {
                foreach($jobs as $row){ 
                      $row->image_url    = url('storage');
                      $row->BookedStatus = JobAssigned::where('jobs_id',$row->id)->count() ;                     
                }
                return response()->json(['status' => 200,'message' => 'job data','data' => array_null_values_replace($jobs->toArray())]);
            } else {
                return response()->json(['status' => 204, 'message' => 'No job found']);
            }        
    }

    public function job_details(Request $request)
    { 
        $job_id = $request->job_id;        
        if(isset($job_id)) {
            $jobs = Jobs::find($job_id);            
              if(!$jobs)
                 return response()->json(['status' => 500, 'message' => 'Job id Wrong']);
              $jobs->image_paths = url('storage/user/'.$jobs->photo); 
              $jobs->start_dates = date('d-M-Y', strtotime($jobs->start_date)); 
              $jobs->end_dates   = date('d-M-Y', strtotime($jobs->end_date)); 
              $jobs->times       = date('H:i', strtotime($jobs->start_time)).'-'.date('H:i', strtotime($jobs->end_time));              
              if(count($jobs->working_days)>0){ 
                array_filter($jobs->working_days);                           
                $jobs->working_days =/* array_diff(range(1,7),$jobs->working_days);*/ $jobs->working_days;
              }else{
                $jobs->working_days = ["0"];
              }
              $JobReviews = JobReviews::with('user')->where('jobs_id',$job_id)->groupBy('user_id')->get();
              if($JobReviews){
                  foreach ($JobReviews as $key => $value) { 
                             $value->image_path = '';
                             if($value->user !=''){
                                $value->user->image_path = '';                            
                             }
                  }
              }
             return response()->json(['status' => 200,'message' => 'job data','data' => array_null_values_replace($jobs->toArray()),'JobReviews'=>
              array_null_values_replace($JobReviews->toArray())]);
             
        }else{
               return response()->json(['status' => 500, 'message' => 'Job id is required']);
        }      
    }

   public function reviews(Request $request)
    {
        $jobs_id  = $request->jobs_id; 
        $user_id  = $request->user_id;
        $ratings  = $request->ratings;
        $feedback = $request->feedback;
        if(isset($jobs_id) && isset($user_id) && isset($ratings)) {
            $data = array(
                'jobs_id' => $jobs_id,
                'user_id' => $user_id,
                'ratings' => ($ratings) ? $ratings : '',
                'is_dispute' => 0,
                'feedback' => ($feedback) ? $feedback : '',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            JobReviews::create($data);
            return response()->json(['status' => 200, 'message' => 'Rating added']);
        } else {
            return response()->json(['status' => 500, 'message' => 'Rating field is required']);
        }
    }

   public function PaymentHistroy(Request $request)
    {  
        
        $currentUser = JWTAuth::toUser();
        $user_id     = $currentUser->id;
        $data = JobPayments::with('user')->with('jobs')->where('user_id',$user_id)->where('status',1)->orderBy('created_at','desc')->get();
        if(count($data)>0)
        {
              $TotalPayment = JobPayments::where('status',1)->where('user_id',$user_id)->sum('amount');          
              foreach ($data as $key => $value) {            
                        $value->image_path   = url('storage/user/');  
                        $value->total_amount = $TotalPayment;
              }
             $data = array_null_values_replace($data->toArray());
             return response()->json(['status' => 200, 'message' => 'PaymentHistroy','TotalPayment' => $TotalPayment,'data'=>$data ]);
        }else
        {
          return response()->json(['status' => 204, 'message' => 'No data' ]);
        }
       
    }


public function Dashboard(Request $request)
    {
        $currentUser = JWTAuth::toUser();
        $user_id     = $currentUser->id;  
        $data = JobPayments::with('user')->where('user_id',$user_id)->where('status',1)->first();
        if(count($data)>0)
        {
              $SpentAmount = JobPayments::where('status',1)->where('user_id',$user_id)->get()->sum('amount');          
              $jobs_count  = JobPayments::where('status',1)->where('user_id',$user_id)->groupBy('jobs_id')->get();                       
              /*foreach ($data as $key => $value) {   */         
                        $data->image_path   = isset($data->user->photo) && $data->user->photo != '' ? url('storage/user/'.$data->user->photo) : url('storage/user/noprofile.png'); 
                        $data->spent_amount = $SpentAmount;
                        $data->jobs_count   = count($jobs_count);
                        $data->first_name   = $data->user ? $data->user->first_name : '';
                        $data->last_name    = $data->user ? $data->user->last_name  : '';
                        $data->location    = $data->user ? $data->user->location  : '';
                        $data->address    = $data->user ? $data->user->address  : '';
                        $data->latitude    = $data->user ? $data->user->latitude  : '';
                        $data->longitude    = $data->user ? $data->user->longitude  : '';
             /* }*/
             $data = array_null_values_replace($data->toArray());
             return response()->json(['status' => 200, 'message' => 'Dashboard', 'SpentAmount' => $SpentAmount,'data'=>$data ]);
        }else
        {
          return response()->json(['status' => 204, 'message' => 'No data' ]);
        }
    }

    public function GetMyallChat(Request $request)
    {
        /*$user_id    = $request->input('user_id');*/
        $currentUser = JWTAuth::toUser();
        $user_id     = $currentUser->id;  

        $touser_id  = $request->input('touser_id');
        $type    = $request->input('type');
        if(isset($user_id) && isset($type)){
              if($type== 'all'){
                       //get all chat
                      $data = JobConversations::with('user')->where('user_id',$user_id)->groupBy('touser_id')->get();                      
                      if(count($data) ==0)
                        return response()->json(['status' => 204, 'message' => 'No chat']);
                      foreach ($data as $key => $value) {            
                        $value->image_path   = url('storage/user/');
                        $date = Carbon::createFromTimeStamp(strtotime($value->created_at))->diffForHumans();
                          if (strpos($date, 'year') !== false || strpos($date, 'days') !== false || strpos($date, 'months') !== false ) {
                              $date = Carbon::parse($value->created_at)->format('d/m/Y');  
                          }
                          $value->chat_time    = $date ;   
                       }
                      $data = array_null_values_replace($data->toArray());
                      return response()->json(['status' => 200, 'message' => 'all chat', 'data'=>$data ]);
                      //get all chat
              }elseif($type == 'single'){
                    if($touser_id ==''){
                       return response()->json(['status' => 500, 'message' => 'To User Id missing' ]);
                    }
                    //get individual chat
                      $data = JobConversations::with('user')->where('user_id',$user_id)->where('touser_id',$touser_id)
                                             ->orderBy('created_at','desc')->get();                                         
                      if(count($data) == 0)
                          return response()->json(['status' => 204, 'message' => 'No chat' ]); 
                                               
                      foreach ($data as $key => $value) {            
                        $value->image_path   = url('storage/user/');  
                         $date = Carbon::createFromTimeStamp(strtotime($value->created_at))->diffForHumans(); 
                          if (strpos($date, 'year') !== false || strpos($date, 'days') !== false || strpos($date, 'months') !== false ) {
                              $date = Carbon::parse($value->created_at)->format('d/m/Y');  
                          }
                            $value->chat_time    = $date ;   
                                                  
                       }
                      $data = array_null_values_replace($data->toArray());
                      return response()->json(['status' => 200, 'message' => 'all chat', 'data'=>$data ]);
                    //get individual chat
              }else{
                return response()->json(['status' => 500, 'message' => 'Type method wrong' ]);
              }
        }else{
             return response()->json(['status' => 500, 'message' => 'Parameter missing' ]);
        }         
    }


   public function SaveMyChat(Request $request)
      {
         /*$user_id     = $request->input('user_id');*/
         $currentUser = JWTAuth::toUser();
         $user_id     = $currentUser->id;  

         $touser_id  = $request->input('touser_id');
         if(is_numeric($user_id) && is_numeric($touser_id)){  
                $touser_data =  User::find($touser_id);
                $ToUserName = ''; 
                if($touser_data)
                {
                  
                   if($touser_data->user_types_id == 1)
                    $ToUserName = 'Super Admin';
                   elseif($touser_data->user_types_id == 2)
                    $ToUserName = 'User';
                   elseif($touser_data->user_types_id == 3)
                    $ToUserName = 'Provider';
                   elseif($touser_data->user_types_id == 4)
                    $ToUserName = 'Both';
                   elseif($touser_data->user_types_id == 5)
                    $ToUserName = 'Admin';
                   else
                    $ToUserName = '';                                     
                }
                 $data = new JobConversations();
                 $data->jobs_id   = ($request->input('jobs_id')) ? $request->input('jobs_id') : '';
                 $data->user_id   = $user_id;
                 $data->touser_id = $touser_id;
                 $data->to_user_name = $ToUserName;
                 $data->messages  = ($request->input('message')) ? $request->input('message') : '';
                 $data->status    = 1 ;
                 if($request->filled('photo')) {            
                     $photo_name   = file_upload($request->input('photo'),'public/jobsconversations',null);
                     $data->image  = ($photo_name) ? $photo_name : '';                    
                 }  
                 $data->save();
               return response()->json(['status' => 200, 'message' => 'Message Saved sucessfully']);
          }else{
               return response()->json(['status' => 500, 'message' => 'Id is not number']);
         }
      }

    public function skills(Request $request)
    {
        $subcat_id = $request->subcat_id; 
        if(isset($subcat_id)) {
            $skills = Skills::where('subcategory_id', $subcat_id)->get();
            if(count($skills)>0) {
                foreach($skills as $row) {  
                    $data[] = array(
                        'id' => $row->id,
                        'name' => $row->name
                    );
                }
                return response()->json(['status' => 200,'message' => 'skill data','data' => $data]);
            }else{
              return response()->json(['status' => 500, 'message' => 'Invalid Subcategory Id.']);
            }                        
        }else{
            return response()->json(['status' => 500, 'message' => 'Sub category Id is Required']);
        }
    }
 
 public function Makebooking(Request $request)
    {      
      
            $job_id = $request->job_id; 
            //$user_id = $request->user_id;        
            $start_date = $request->start_date;        
            $start_time = $request->start_time;        
            $end_date = $request->end_date;        
            $end_time = $request->end_time;        
            if(isset($job_id)) {  
                     //jobs create
                     $Job = new Jobs();
                     $exclude_inputs = array('photo');
                     foreach($request->all() as $key=>$value) {
                          if(!in_array($key,$exclude_inputs))
                              $Job->$key = $value;
                      }
                     $Job->save(); 
                     //jobs create

                     //job payment create
                    //  $JobPayments = new JobPayments();
                    //  $exclude_inputs = array('photo');
                    //  foreach($request->all() as $key=>$value) {
                    //       if(!in_array($key,$exclude_inputs))
                    //           $JobPayments->$key = $value;
                    //   } 
                    //  if($request->filled('photo')){                         
                    //     $JobPayments->photo = SocialImageCopy($request->input('photo'),'fb_profilepic.jpg'); 
                    //    }  
                    // $JobPayments->updated_at = Carbon::now();
                    // $JobPayments->created_at = Carbon::now();
                    // $JobPayments->status     = 1 ;
                    // $JobPayments->save();

                    $currentUser = JWTAuth::toUser();
                    $user_id     = $currentUser->id;  

                    $JobPayments = new JobPayments();
                    $JobPayments->jobs_id   = $job_id;
                    $JobPayments->user_id   = $user_id;
                    $JobPayments->payment_type  = 'credit' ;
                    $JobPayments->initiated_by  = 'client' ;
                    $JobPayments->amount  = isset($request->price)?$request->price:0;
                    $JobPayments->updated_at        = Carbon::now();
                    $JobPayments->created_at        = Carbon::now();
                    $JobPayments->status = 1 ;                   
                    $JobPayments->save();

                    //job payment create

                    //job payment logs
                    $JobPaymentsLogs = new JobPaymentsLogs();
                    $JobPaymentsLogs->job_payments_id   = $JobPayments->id;
                    $JobPaymentsLogs->payment_method_id = 1 ;                   
                    $JobPaymentsLogs->status            = 1 ;
                    $JobPaymentsLogs->updated_at        = Carbon::now();
                    $JobPaymentsLogs->created_at        = Carbon::now();
                    $JobPaymentsLogs->save();
                    //job payment logs

                    //Nodification process 

                    //Nodification process 

                return response()->json(['status' => 200, 'message' => 'Thank you for requesting job', 'date'=>str_replace('-','at',Carbon::now()->format('F d, Y - h:m A'))]);
            } else {
                return response()->json(['status' => 500, 'message' => 'Parameter missing']);
            }
    }

       // 6 and 7 ui design
    public function JobBookingList(Request $request)
    {        
             /*$user_id = $request->input('user_id');*/
             $currentUser = JWTAuth::toUser();
             $user_id     = $currentUser->id;  

             $type    = $request->input('type');
             if($type !='previous' && $type !='upcoming'){
                return response()->json(['status' => 204, 'message' => 'Parameter missing && Type wrong']);
             }
             $page   = $request->has('page') ? $request->get('page') : 1;
             $limit  = $request->has('limit') ? $request->get('limit') : 10; 
             $search = $request->has('search') ? $request->get('search') : null; 
             if(!$user_id)
                 return response()->json(['status' => 500, 'message' => 'User Id missing']);
             if($type == 'upcoming'){
                   $JobPayments = DB::table('job_payments')
                                 ->join('jobs', 'job_payments.jobs_id', '=','jobs.id')
                                 ->join('users', 'job_payments.user_id', '=','users.id')                               
                                 ->where('jobs.status','=',1)
                                 ->where('users.first_name','LIKE','%'.$search.'%')
                                 ->whereDate('job_payments.issued_on', '>=', Carbon::now())
                                 ->where('job_payments.user_id','=',$user_id)
                                 ->select('jobs.id as jobid','jobs.job_id as jobunique','jobs.name as jobname','jobs.description as description',
                                   'jobs.budget as budget', 'jobs.quoted_amount as quoted_amount','users.first_name as provider_name',
                                   'users.photo as photo','job_payments.issued_on as issued_on',
                                   'job_payments.status','users.latitude','users.longitude','users.address','users.location'  
                                 )->limit($limit)->offset(($page - 1) * $limit)->get();  
                  $totalpages    = DB::table('job_payments')
                                 ->join('jobs', 'job_payments.jobs_id', '=','jobs.id')
                                 ->join('users', 'job_payments.user_id', '=','users.id')                               
                                 ->where('jobs.status','=',1)
                                 ->where('users.first_name','LIKE','%'.$search.'%')
                                 ->whereDate('job_payments.issued_on', '>=', Carbon::now())
                                 ->where('job_payments.user_id','=',$user_id)                             
                                 ->count();    

             }else{
                   
                 $JobPayments = DB::table('job_payments')
                                 ->join('jobs', 'job_payments.jobs_id', '=','jobs.id')
                                 ->join('users', 'job_payments.user_id', '=','users.id')                               
                                 ->where('jobs.status','=',1)
                                 ->where('users.first_name','LIKE','%'.$search.'%')
                                 ->whereDate('job_payments.issued_on', '<=', Carbon::now())
                                 ->where('job_payments.user_id','=',$user_id)
                                 ->select('jobs.id as jobid','jobs.job_id as jobunique','jobs.name as jobname','jobs.description as description',
                                   'jobs.budget as budget', 'jobs.quoted_amount as quoted_amount','users.first_name as provider_name',
                                   'users.photo as photo','job_payments.issued_on as issued_on',
                                   'job_payments.status','users.latitude','users.longitude','users.address','users.location'                 
                                 )->limit($limit)->offset(($page - 1) * $limit)->get(); 

                 $totalpages    = DB::table('job_payments')
                                 ->join('jobs', 'job_payments.jobs_id', '=','jobs.id')
                                 ->join('users', 'job_payments.user_id', '=','users.id')                               
                                 ->where('jobs.status','=',1)
                                 ->where('users.first_name','LIKE','%'.$search.'%')
                                 ->whereDate('job_payments.issued_on', '<=', Carbon::now())
                                 ->where('job_payments.user_id','=',$user_id)
                                 ->count();            

             }             
             if(count($JobPayments)==0)
                return response()->json(['status' => 204, 'message' => 'No Jobs']);
              
              foreach ($JobPayments as $key => $value) {                    
                         $status = '';
                        if($value->status==1) {
                          $status = 'Awaiting Confirmation';
                        }
                        else if($value->status==2) {
                          $status = 'Confirmed';
                        }
                        else if($value->status==3) {
                          $status = 'Cancelled';
                        }
                  $value->status    = $status ;
                  $value->location  = isset($value->location) && $value->location != '' ? $value->location : '' ;
                  $value->address  = isset($value->address) && $value->address != '' ? $value->address : '' ;
                  $value->issued_on = Carbon::parse($value->issued_on)->format('d F Y,g:i A');  
                  $value->photo = isset($value->photo) && $value->photo != '' ? url('storage/user/'.$value->photo) : url('storage/user/noprofile.png') ;                   //user review add                
                  if($value->jobid !=''){                          
                    $rating   = JobReviews::where('jobs_id',$value->jobid)->groupBy('jobs_id')->get()->sum('ratings');  
                    $comments = JobReviews::where('jobs_id',$value->jobid)->groupBy('jobs_id')->count();  
                    $offers   = JobAssigned::where('jobs_id',$value->jobid)->groupBy('jobs_id')->count();  
                    $value->rating   = $rating/5 ; 
                    $value->comments = $comments;
                    $value->offers   = $offers;
                   }                      
                 // user review add   
                           
              }              
              $numberOfPages = (int) ceil($totalpages / $limit);   
              return response()->json(['status' => 200,'message' =>'List Booking','total'=>$numberOfPages, 'data' => array_null_values_replace($JobPayments->toArray())]);
    }


    /*public function MycareJobs(Request $request)
    {
        $page   = $request->has('page')   ? $request->get('page') : 1;
        $limit  = $request->has('limit')  ? $request->get('limit') : 10; 
        $search = $request->has('search') ? $request->get('search') : null;
        $user_id = $request->input('user_id');

        if(!$user_id)
          return response()->json(['status' => 500, 'message' => 'User id missing']);

        $jobs   = JobPayments::with(['jobs','user'])->where('user_id', $user_id)->orderBy('id','desc')->limit($limit)->offset(($page - 1) * $limit)->get();

        $data = array();
        if(count($jobs)>0) {
            $currentdate = date('Y-m-d');
            foreach($jobs as $row){ 
                $stat = '';
                if($row->status==1) {
                  $stat = 'Awaiting Confirmation';
                }
                else if($row->status==2) {
                  $stat = 'Confirmed';
                }
                else if($row->status==3) {
                  $stat = 'Cancelled';
                }

                if($row->issued_on>=$currentdate) {
                    $data['upcoming'][] = array(
                      'jobname' => $row->jobs->name,
                      'jobdate' => date("d M'y", strtotime($row->issued_on)),
                      'jobtime' => date('h:i a', strtotime($row->issued_on)),
                      'username' => $row->user->first_name.' '.$row->user->last_name,
                      'amount' => $row->amount,
                      'status' => $stat
                    );
                } else {
                    $data['previous'][] = array(
                      'jobname' => $row->jobs->name,
                      'jobdate' => date("d M'y", strtotime($row->issued_on)),
                      'jobtime' => date('h:i a', strtotime($row->issued_on)),
                      'username' => $row->user->first_name.' '.$row->user->last_name,
                      'amount' => $row->amount,
                      'status' => $stat
                    );
                }                
            }
            return response()->json(['status' => 200, 'message' => 'my care jobs', 'data'=>$data ]);
        } else {
            return response()->json(['status' => 500, 'message' => 'No my care jobs found']);
        }        
    }*/

  public function PricingGuide(Request $request)
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $search = $request->has('search') ? $request->get('search') : null; 
        $Questions = Help::where('type', '=', 'pricing_guide')->where('question','LIKE','%'.$search.'%')->limit($limit)->offset(($page - 1) * $limit)->get();
        if(!$Questions)
            return response()->json(['status' => 204, 'message' => 'No Questions']);
    
        
        $data = array(); $datas = array();
        foreach ($Questions as $key => $value) {               
              $data[$value->title]['title']   = $value->title ;
              $data[$value->title]['response'][] = $value ;                    
        }
        foreach ($data as $keys => $val) {               
              unset($keys);   
              $datas[] = $val;
        }

        $totalpages    = Help::where('type', '=', 'pricing_guide')->where('question','LIKE','%'.$search.'%')->count();
        $numberOfPages = (int) ceil($totalpages / $limit);        
        return response()->json(['status' => 200, 'message' => 'Questions data','total'=>$numberOfPages,'data' => $datas]);
    }

    public function Educationlist(Request $request)
    {
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        $resources = Resources::with('resourcefiles')->where('status', 1)->orderBy('id', 'desc')->limit($limit)->offset(($page - 1) * $limit)->get();
        if(count($resources)>0){
            foreach($resources as $row){  
                $data[] = array(
                    'id' => $row->id,
                    'title' => $row->title,
                    'description' => $row->description,
                    'date' => date('d/m/Y', strtotime($row->created_at)),
                    'files' => $row->resourcefiles,
                    'imagepath' => url('storage/resources/')
                );                
            }
            $totalpages = Resources::with('resourcefiles')->where('status', 1)->count();
            $numberOfPages = (int) ceil($totalpages / $limit);
            return response()->json(['status' => 200, 'total' => $numberOfPages, 'message' => 'education data','data' => $data]);
        }else{
            return response()->json(['status' => 204, 'message' => 'No education details found']);
        }       
    }

    public function subscription(Request $request)
    {
        $setting = Sitesetting::find(1);
        $string = $setting->subscription_benefits;

        $d = new \DOMDocument();
        $d->loadHTML($string);
        $key = array();
        foreach($d->getElementsByTagName('h3') as $item){
            $key[] = $item->textContent;
        }

        $val = array();
        foreach($d->getElementsByTagName('p') as $item){
            $val[] = $item->textContent;
        }

        $c=array_combine($key,$val);

        $imgarr = array('location.png','speedometer.png','noads.png');
        $arr=array(); 
        $i=0;
        foreach ($c as $key => $value) {            
            $arr[] = array(
                'image' => url('img/'.$imgarr[$i]),
                'title' => $key,
                'content' => $value
            );
            $i++;
        }

        $subscription = Subscription::where('status', 1)->orderBy('id', 'desc')->get();
        if(count($subscription)>0) {
            foreach($subscription as $row) {  
                $duration = array('1'=>'1 Month','2'=>'2 Months','3'=>'3 Months','6'=>'6 Months','12'=>'1 year','24'=>'2 years');
                $data[] = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'description' => $row->description,
                    'duration' => $duration[$row->duration],
                    'amount' => $row->amount,
                    'date' => date('Y-m-d H:i:s', strtotime($row->created_at))
                );                
            }
            return response()->json(['status' => 200, 'message' => 'subscription data', 'subscription_benefits' => $arr,'data' => $data]);
        } else {
            return response()->json(['status' => 204, 'message' => 'No education details found']);
        }       
    }

    public function CommentsReview(Request $request)
    {
        $page   = $request->has('page')   ? $request->get('page') : 1;
        $limit  = $request->has('limit')  ? $request->get('limit') : 10; 
        $search = $request->has('search') ? $request->get('search') : null;
        /*$user_id = $request->input('user_id');*/
        $currentUser = JWTAuth::toUser();
        $user_id     = $currentUser->id;  

        if(!$user_id)
          return response()->json(['status' => 500, 'message' => 'User id missing']);

       /* $jobs = JobAssigned::with(['user','job'])->where('user_id', $user_id)->orderBy('id','desc')
                           ->limit($limit)->offset(($page - 1) * $limit)->get();*/


         $jobs = JobAssigned::with(['job'])->where('user_id',$user_id)
                 ->orderBy('id','desc')
                 ->with(['user' => function ($query) use ($search){
                     $query->where('first_name','LIKE','%'.$search.'%');                            
          }])->limit($limit)->offset(($page - 1) * $limit)->get();

         $jobs_count = JobAssigned::with(['job'])->where('user_id',$user_id)
                 ->orderBy('id','desc')
                 ->with(['user' => function ($query) use ($search){
                     $query->where('first_name','LIKE','%'.$search.'%');                            
          }])->count();

                           
        if(count($jobs)>0) {
            $data = array();
            foreach($jobs as $row){ 
               if(!empty($row->job) && !empty($row->user)){
                if($row->user->photo=='') 
                    $photo = url('storage/user/noprofile.png'); 
                else 
                    $photo = url('storage/user/'.$row->photo);    

                $data[] = array(
                   'id' => isset($row->job)?$row->job->id:'',
                   'photo' => $photo,
                   'first_name' => $row->user->first_name,
                   'last_name' => $row->user->last_name,
                   'category' => isset($row->job)?$row->job->getcategory->name:'',
                   'job_id' => isset($row->jobs_id)? $row->jobs_id: 0 ,
                   'job_description' => isset($row->job)?$row->job->description:'',
                   'start_date' => isset($row->job)?date("d M y", strtotime($row->job->start_date)):'',
                   'start_time' => isset($row->job)?date("h:i a", strtotime($row->job->start_time)):'',
                   'end_date' => isset($row->job)?date('d M y', strtotime($row->job->end_date)):'',
                   'end_time' => isset($row->job)?date("h:i a", strtotime($row->job->end_time)):''
                );  
                }          
            }
            /*$totalpages = JobAssigned::with(['user','job'])->where('user_id', $user_id)->count();  */          
            $numberOfPages = (int) ceil($jobs_count / $limit);
              if(count($data) == 0)
                return response()->json(['status' => 204, 'message' => 'No Users']);
            return response()->json(['status' => 200,'message' => 'comments and reviews','total'=>$numberOfPages,'data' => $data]);
        } else {
            return response()->json(['status' => 204, 'message' => 'No comments and reviews found']);
        }
    }

    public function ReviewPost(Request $request)
    {
    }

    public function Help(Request $request)
    {
      $page = $request->has('page') ? $request->get('page') : 1;
      $limit = $request->has('limit') ? $request->get('limit') : 10;
      $search = $request->has('search') ? $request->get('search') : null; 
      $Questions = Help::where('type', '=', 'help')->where('question','LIKE','%'.$search.'%')->limit($limit)->offset(($page - 1) * $limit)->get()->toArray();

      if(!$Questions)
          return response()->json(['status' => 204, 'message' => 'No Questions']);

       $totalpages    = Help::where('type', '=', 'help')->where('question','LIKE','%'.$search.'%')->count();
       $numberOfPages = (int) ceil($totalpages / $limit);
       return response()->json(['status' => 200, 'message' => 'Questions data','total'=>$numberOfPages,'data' => array_null_values_replace($Questions)]);
    }


    public function Notifications(Request $request)
    {
    }

    public function settings(Request $request)
    {
        $settings = UserSettings::get();
        if(count($settings)>0) {            
            return response()->json(['status' => 200, 'message' => 'setting data','data' => $settings]);
        } else {
            return response()->json(['status' => 204, 'message' => 'No setting found']);
        }       
    }

    public function setting_update(Request $request)
    {
        $setting_id  = $request->setting_id; 
        $status  = $request->status; 
        if(isset($setting_id) && isset($status)) {
            $setting = UserSettings::find($setting_id);            
            if(!$setting)
              return response()->json(['status' => 500, 'message' => 'Setting id Wrong']);

            $data = array('status' => $status);
            UserSettings::where('id', $setting_id)->update($data);
            return response()->json(['status' => 200, 'message' => 'Setting status updated']);
        } else {
            return response()->json(['status' => 500, 'message' => 'Setting id and status is required']);
        }
    }
    
    public function verify_providers(Request $request)
    {
        $experience  = $request->experience; 
        $selectedskills  = $request->selectedskills; 
        $phone  = $request->photo_name;  
        $bio  = $request->bio;  
        $fee  = $request->fee;  
        $description  = $request->description;  
        $availability  = $request->availability;  
        $attach_document  = $request->attach_document;  
        $category_id  = $request->category_id;  

        $user = JWTAuth::toUser(); 
        if(!$user)
          return response()->json(['status' => 204, 'message' => 'No User']);  

        $user_id = $user->id;

        if(isset($experience) || isset($selectedskills) || isset($phone) || isset($fee) || isset($description) || isset($availability)) 
        {
            $data = array(
                'experience' => isset($experience)?$experience:'',
                'selected_skills' => isset($selectedskills)?$selectedskills:'',
                'phone' => isset($phone)?$phone:'',
                'bio' => isset($bio)?$bio:'',
                'fee' => isset($fee)?$fee:'',
                'description' => isset($description)?$description:'',
                'availability' => isset($availability)?$availability:'',
                'updated_at' => date('Y-m-d H:i:s')
            );   
            User::where('id', $user_id)->update($data);   

            if($attach_document=='yes') {
                $user_id  = $user_id; 
                $industry_id  = $category_id; 
                $document_id  = 1;
                //upload certificate check
                if($request->hasfile('100PointCheck'))
                {
                    foreach($request->file('100PointCheck') as $key => $file)
                    {   
                        $filename= rand().'_'.$file->getClientOriginalName();
                        $file->storeAs('certificates', $filename, ['disk' => 'public']);
                        $doc_data = array(
                            'user_id' => $user_id,
                            'industry_id' => $industry_id,
                            'documents_id' => $document_id,
                            'file_path' => $filename, 
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );                
                        UserDocuments::create($doc_data);        
                    }           
                }   
                if($request->hasfile('aphra'))
                {
                    foreach($request->file('aphra') as $key => $file)
                    {   
                        $filename= rand().'_'.$file->getClientOriginalName();
                        $file->storeAs('certificates', $filename, ['disk' => 'public']);
                        $doc_data = array(
                            'user_id' => $user_id,
                            'industry_id' => $industry_id,
                            'documents_id' => $document_id,
                            'file_path' => $filename, 
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );                
                        UserDocuments::create($doc_data);        
                    }           
                } 
                if($request->hasfile('policeCheck'))
                {
                    foreach($request->file('policeCheck') as $key => $file)
                    {   
                        $filename= rand().'_'.$file->getClientOriginalName();
                        $file->storeAs('certificates', $filename, ['disk' => 'public']);
                        $doc_data = array(
                            'user_id' => $user_id,
                            'industry_id' => $industry_id,
                            'documents_id' => $document_id,
                            'file_path' => $filename, 
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );                
                        UserDocuments::create($doc_data);        
                    }           
                }
                if($request->hasfile('qualification'))
                {
                    foreach($request->file('qualification') as $key => $file)
                    {   
                        $filename= rand().'_'.$file->getClientOriginalName();
                        $file->storeAs('certificates', $filename, ['disk' => 'public']);
                        $doc_data = array(
                            'user_id' => $user_id,
                            'industry_id' => $industry_id,
                            'documents_id' => $document_id,
                            'file_path' => $filename, 
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );                
                        UserDocuments::create($doc_data);        
                    }           
                }  
                //upload certificate check           
            }

            return response()->json(['status' => 200, 'message' => 'Provider has been verified']);         
        } else {
            return response()->json(['status' => 500, 'message' => 'Parameter Missing']);
        }
    }

    public function apply_jobs(Request $request)
    {
        $jobs_id = $request->jobs_id; 
        $user_id = $request->user_id; 
        $my_offer = $request->my_offer; 
        $my_cover_letter = $request->my_cover_letter; 
        if(isset($jobs_id) && isset($user_id) && isset($my_offer) && isset($my_cover_letter)) {
            $data = array(
                'jobs_id' => $jobs_id,
                'user_id' => $user_id,
                'my_offer' => isset($my_offer) ? $my_offer : '',
                'my_cover_letter' => isset($my_cover_letter) ? $my_cover_letter : '',
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            JobProposals::create($data);                 
            return response()->json(['status' => 200, 'message' => 'Job has been applied']);        
        }else{
            return response()->json(['status' => 500, 'message' => 'Parameter Missing']);
        }
    }

    public function DeleteDocument(Request $request)
    {
       $document_id = $request->input('document_id');
       if(!$document_id || !is_numeric($document_id))
          return response()->json(['status' => 500, 'message' => 'Parameter Missing && Invalid id']);
        $document = UserDocuments::find($document_id);        
        if(!$document){
          return response()->json(['status' => 204, 'message' => 'No record']);
        }        
        $file_path = storage_path('app/public/certificates/'.$document->file_path);
        @unlink($file_path);
        UserDocuments::find($document_id)->delete();
        return response()->json(['status' => 200, 'message' => 'Document deleted Successfully']);         
    }

   public function send_push_notification($fcm,$type,$record_id) {  
                $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
                $msg = $msg;
                $title = $title;
                $token = $fsm;
                $sound="1";
                $fields = array(
                                'to' => $token,
                                'priority' => "high",
                                'content_available' => true,
                                'notification' => array("body" => $msg, 'title' => $title, 'sound' => ($sound==1)? "":NULL, )
                                   );

                $headers = array('Authorization:key=AAAAu-p8U2E:APA91bEA7KcS_36AQu0coYjqKRaOkPVwBnXZK8I0h6P0QssboqC2iMMDL2tolACqqFfH80Hcrmn9PE_m0ADk8ihA7qJxlsPBu2ctTrA556Ol58pT_4N8AANKNeiDdCOfcYFLDydVRdz5',
                                'Content-Type:application/json'
                                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);
                curl_close($ch);

}

public function send_sms($to_num,$msg) {
                if(!empty($to_num))
                {
                                $curl = CURL_INIT();
                                curl_setopt_array($curl, array(
                                CURLOPT_URL => "http://api.msg91.com/api/v2/sendsms",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS => "{ \"sender\": \"MSGIND\", \"route\": \"4\", 
                                \"country\": \"91\", \"sms\": [ { \"message\": \"{$msg}\", \"to\": 
                                [ \"{$to_num}\"] } ] }",
                                CURLOPT_SSL_VERIFYHOST => 0,
                                CURLOPT_SSL_VERIFYPEER => 0,
                                CURLOPT_HTTPHEADER => array(
                                        "authkey:233686AIlNLHO6BPb5b81384a",
                                        "content-type: application/json" 
                                ),
                        ));

                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        /*dd($response);*/
                        curl_close($curl);
                        /*if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }*/

                }
        }

public function AddNotes(Request $request)
{
      
       $currentUser = JWTAuth::toUser();
       $user_id     = $currentUser->id;
       $job_id  = $request->input('job_id');
       if(!$user_id || !is_numeric($user_id) || !$job_id || !is_numeric($job_id))
            return response()->json(['status' => 500, 'message' => 'Parameter Missing && Invalid id format']);
       $jobs_data = Jobs::find($job_id);
       $user_data = User::find($user_id);
       if(!$jobs_data ||  !$user_data )
            return response()->json(['status' => 500, 'message' => 'Invalid id']);
       $notes = new Notes(); 
       $notes->jobs_id        = $request->input('job_id')     ?  $request->input('job_id')      : 0 ;
       $notes->user_id        = $currentUser->id    ? $currentUser->id      : 0 ;
       $notes->first_vist     = $request->input('first_vist') ?  $request->input('first_vist')  : '' ;
       $notes->repeated_count = $request->input('repeated_count') ?  $request->input('repeated_count')  : '' ;
       $notes->created_at     = date('Y-m-d H:i:s');
       $notes->updated_at     = date('Y-m-d H:i:s');
       $notes->save();
       return response()->json(['status' => 200, 'message' => 'Notes Added Successfully']);
}

public function Myoffers(Request $request)
    {

        if($request->input('type') == 'offer'){
                     $myoffer = DB::table('jobs')
                               ->join('job_assigned', 'jobs.id', '=','job_assigned.jobs_id')
                               ->where('jobs.status','=',1)
                               ->where('job_assigned.user_id','=',$request->input('user_id'))
                               ->select('job_assigned.id as job_assigned_id','jobs.*','job_assigned.*')
                               ->get();
                    if(count($myoffer) ==0)
                             return response()->json(['status' => 204, 'message' => 'No offer']);
                        //add object
                           foreach ($myoffer as $key => $value) {
                             $value->job_image = 'sample image';
                           }
                        //add object
            return response()->json(['status' => 200, 'message' => 'offer data','data' => array_null_values_replace($myoffer->toArray())]);   
         }elseif($request->input('type') == 'myoffer'){
                    $myofferme = DB::table('jobs')
                               ->join('job_assigned', 'jobs.id', '=','job_assigned.jobs_id')
                               ->where('jobs.status','=',0)
                               ->where('job_assigned.user_id','=',$request->input('user_id'))
                               ->select('job_assigned.id as job_assigned_id','jobs.*','job_assigned.*')
                               ->get();
                    if(count($myofferme) ==0)
                             return response()->json(['status' => 204, 'message' => 'No My offer']);
                    //add object
                           foreach ($myofferme as $key => $value) {
                             $value->job_image = 'sample image';
                           }
                        //add object
            return response()->json(['status' => 200, 'message' => 'my offer data','data' => array_null_values_replace($myofferme->toArray())]);
         }else{
          return response()->json(['status' => 500, 'message' => 'type is wrong']);
         }
    }

 public function CommanUpdate(Request $request)
    {
        error_log(print_r($request->all(),true),3,'/var/www/html/error.log');        
        $user = User::all();
        foreach ($user as $key => $value) {
          /* User::where('id',$value->id)->where('subcategory',NULL)->update(['subcategory'=>0]);*/
          # code...
        }
      return response()->json(['status' => 200, 'message' => 'updated sucessfully']);
        
    }

    public function AddUserBankDetails(Request $request)
    {
         $currentUser = JWTAuth::toUser();
         $user_id     = $currentUser->id;
         if(!$user_id)
            return response()->json(['status' => 500, 'message' => 'User not found']);
         $count = UserBankaccounts::where('user_id',$user_id)->count();
       if($count ==0){
            $bank_detail = new UserBankaccounts();
            $bank_detail->user_id             = $user_id ;
            $bank_detail->bank_name           = $request->input('bank_name') ?  $request->input('bank_name')   : '' ; 
            $bank_detail->ifsc_code           = $request->input('ifsc_code') ?  $request->input('ifsc_code')   : '' ;
            $bank_detail->branch_name         = $request->input('branch_name') ?  $request->input('branch_name') : '' ;
            $bank_detail->account_no          = $request->input('account_no') ?  $request->input('account_no') : '' ;
            $bank_detail->account_holder_name = $request->input('account_holder_name') ?  $request->input('account_holder_name') : '';
            $bank_detail->status              = 1 ;
            $bank_detail->updated_at = Carbon::now();
            $bank_detail->created_at = Carbon::now();
            $bank_detail->save();
            return response()->json(['status' => 200, 'message' => 'Added sucessfully']);
       }else{
            $bank_detail = UserBankaccounts::where('user_id',$user_id)->first();
            $bank_detail->user_id             = $user_id ;
            $bank_detail->bank_name           = $request->input('bank_name') ?  $request->input('bank_name')   : $bank_detail->bank_name ; 
            $bank_detail->ifsc_code           = $request->input('ifsc_code') ?  $request->input('ifsc_code')   : $bank_detail->ifsc_code ;
            $bank_detail->branch_name         = $request->input('branch_name') ?  $request->input('branch_name') : $bank_detail->branch_name ;
            $bank_detail->account_no          = $request->input('account_no') ?  $request->input('account_no') : $bank_detail->account_no ;
            $bank_detail->account_holder_name = $request->input('account_holder_name')?$request->input('account_holder_name') : $bank_detail->account_holder_name;    
            $bank_detail->updated_at = Carbon::now();  
            $bank_detail->save();
            return response()->json(['status' => 200, 'message' => 'updated sucessfully']);
       }

    }

    public function GetUserBankDetails(Request $request)
    {
         $currentUser = JWTAuth::toUser();
         $user_id     = $currentUser->id;
         if(!$user_id)
            return response()->json(['status' => 500, 'message' => 'User not found']);
         $bank_detail = UserBankaccounts::where('user_id',$user_id)->first();
         if(count($bank_detail) == 0)
            return response()->json(['status' => 500, 'message' => 'No bank data']);

         return response()->json(['status' => 200, 'message' => 'User bank data','data' => array_null_values_replace($bank_detail->toArray())]);
   
    }

    public function payment(Request $request)
    {
         $currentUser = JWTAuth::toUser();
         $user_id     = $currentUser->id;
         if(!$user_id)
            return response()->json(['status' => 500, 'message' => 'User not found']);

         $job_id = $request->input('job_id');
         $issued_on = $request->input('issued_on');
         $amount = $request->input('amount');
         $currency = "AUD";
         $description = "test charge";
         $email = $request->input('email');
         $ip_address = $_SERVER['REMOTE_ADDR'];
         $card_number = $request->input('card_number');
         $card_expiry_month = $request->input('card_expiry_month');
         $card_expiry_year = $request->input('card_expiry_year');
         $card_cvv = $request->input('card_cvv');
         $card_name = $request->input('card_name');
         $card_address_line1 = $request->input('card_address_line1');
         $card_address_line2 = $request->input('card_address_line2');
         $card_address_city = $request->input('card_address_city');
         $card_address_postcode = $request->input('card_address_postcode');
         $card_address_state = $request->input('card_address_state');
         $card_address_country = $request->input('card_address_country');

         if(isset($job_id) && isset($issued_on) && isset($amount) && isset($email) && isset($card_number) && isset($card_expiry_month) && isset($card_expiry_year) && isset($card_cvv) && isset($card_name) && isset($card_address_line1) && isset($card_address_line2) && isset($card_address_city) && isset($card_address_postcode) && isset($card_address_state) && isset($card_address_country)) {

            //Get pinpayment secret key
            $setting = Sitesetting::where('id', 1)->first();
            $secretkey = isset($setting->pin_payment_secret)?$setting->pin_payment_secret:'';

            //Authentication
            $test = true;
            $auth['u'] = (($test === true) ? $secretkey : $secretkey); //Private Key from PIN
            $auth['p'] = ''; //API calls use empty password

            //Set URL
            $url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/charges';

            //Fields to Post
            $post = array();
            $post['amount'] = $amount*100;
            $post['currency'] = $currency;
            $post['description'] = "test charge";
            $post['email'] = $email;
            $post['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $post['card[number]'] = $card_number;
            $post['card[expiry_month]'] = $card_expiry_month;
            $post['card[expiry_year]'] = $card_expiry_year;
            $post['card[cvc]'] = $card_cvv;
            $post['card[name]'] = $card_name;
            $post['card[address_line1]'] = $card_address_line1;
            $post['card[address_line2]'] = $card_address_line2;
            $post['card[address_city]'] = $card_address_city;
            $post['card[address_postcode]'] = $card_address_postcode;
            $post['card[address_state]'] = $card_address_state;
            $post['card[address_country]'] = $card_address_country;


            // Create a curl handle
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
            curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json

            // Execute
            $response = curl_exec($ch);
            $jsonArray = array();
            $jsonArray = json_decode($response,true);

            //Success? 
            if(!empty($jsonArray['response']['success']) && $jsonArray['response']['success'] == true)
            {
                $job_payments = array(
                  'jobs_id' => $job_id,
                  'user_id' => $user_id,
                  'payment_notes' => 'success',
                  'payment_type' => 'credit',
                  'initiated_by' => 'client',
                  'amount' => $amount,
                  'issued_on' => $issued_on,
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s'),
                  'status' => 2
                );
                $job_payments_id = JobPayments::create($job_payments)->id;

                $job_payment_logs = array(
                  'job_payments_id' => $job_payments_id,
                  'payment_method_id' => 1,
                  'transaction_log' => $jsonArray['response']['token'],
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s'),
                  'status' => 1
                );
                JobPaymentsLogs::create($job_payment_logs);

                return response()->json(['status' => 200, 'message' => 'Pin payment Success!']);
            }
            else
            {      
                $msgs = '';
                if(!empty($jsonArray['messages'])) {
                  foreach ($jsonArray['messages'] as $key => $value) {
                    $msgs .= $value['message'].'\n';
                  }
                }
                return response()->json(['status' => 500, 'error_description' => $jsonArray['error_description'], 'message' => $msgs]);
            }


            // Check if any error occurred
            if(!curl_errno($ch)){$info = curl_getinfo($ch);}

            // Close handle
            curl_close($ch);

         } else {
            return response()->json(['status' => 500, 'message' => 'Parameter missing']);
         }   
    }


}