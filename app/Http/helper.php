<?php

use App\User;
use App\Models\UserPermissions;
use App\Models\Modules;
use App\Models\Category;

function checkSidebar()
{
    /*$user_id = \Auth::user()->id;
    $sidebar = UserPermissions::join('modules', 'modules.id', '=', 'user_permissions.module_id')->where('user_permissions.user_id', $user_id)->get(); 
    return $sidebar;*/
    $user_id = \Auth::user()->id;
    $user_type = \Auth::user()->user_types_id;
    $sidebar = UserPermissions::join('modules', 'modules.id', '=', 'user_permissions.module_id')->where('user_permissions.user_id', $user_id)->get()->toArray();
    foreach($sidebar as $list){
        if($list['parent'] == 0){
            $sidebarlist[$list['id']] = $list;
        }
        if($user_type==1) {
          if($list['parent'] != 0){
              $sidebarlist[$list['parent']]['subcategory'][] = $list;
          }
        }
        if($user_type==5) {
          if($list['parent'] != 0 && ($list['view']==1||$list['add']==1||$list['edit']==1||$list['delete']==1)){
              $sidebarlist[$list['parent']]['scategory'][] = $list;
          }
        }
    }
    return $sidebarlist;
}

function checkAdminPermission()
{
    $user_id = \Auth::user()->id;
    $user = UserPermissions::join('modules', 'modules.id', '=', 'user_permissions.module_id')->where('user_permissions.user_id', $user_id)->get();

    $segment = \Request::segment(1);
    $modules = Modules::where('link', $segment)->first();
    $module_id = $modules->id;

    $access = array();
    foreach ($user as $key => $value) {
        if($value->view==1 && $value->module_id==$module_id)
          $access[] = 'view';
        if($value->add==1 && $value->module_id==$module_id)
          $access[] = 'add';
        if($value->edit==1 && $value->module_id==$module_id)
          $access[] = 'edit';
        if($value->delete==1 && $value->module_id==$module_id)
          $access[] = 'delete';
    }
    return $access;
}

function getUserNameById($id) {
    $user = User::find($id);
    return $user->first_name.' '.$user->last_name;
}

function randomPassword() {
    /*$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';*/
    $alphabet = '1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 6; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function generateNumericOTP($n = 6) 
{       
    $generator = "1357902468";  
    $result = ""; 
  
    for ($i = 1; $i <= $n; $i++) { 
        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
    }   
    return $result; 
} 

function getCategoryName($id) {
    $cat = Category::find($id);
    if(count($cat) ==0){
     return '';
    }
    return $cat->name;
}

function array_null_values_replace($result)
    {
          if(is_array($result))
          { 
            array_walk_recursive($result, function (&$item, $key)
             {  
               $item = null === $item ? '' : $item;
             });
            return $result;
          }                 
          else
           {  
                $r['Status'] = 'Failed';
                $r['message'] = "Failed";
                return response()->json($r);
           }
    }

function file_upload($data,$type,$old_image){ 
  $ProfileImagedate = md5(date('Y-m-d H:i:s'));
  $ProfileImageData = base64_decode($data);
  $profileImageFilename = $ProfileImagedate.rand().".png";
  $location = $type.'/'.$profileImageFilename; 
  if(file_exists(storage_path('app/'.$type.'/'.$old_image)))
    {          
       @unlink(storage_path('app/'.$type.'/'.$old_image));
    }      
  $path = \Storage::put($location, $ProfileImageData);
  return $profileImageFilename;
 }


function SocialImageCopy($data,$filename){ 
        //copy image adn storage local         
        $url = $data;
        $data = file_get_contents($url);
        $fileName = $filename;
        $file = fopen($fileName, 'w+');
        fputs($file, $data);
        fclose($file);
        $renamefileName = time().$fileName ;
        /*$newfile = public_path('public/uploads/user_image/'.$renamefileName);*/
        $newfile = storage_path('app/public/user/'.$renamefileName);
        if (!copy($url, $newfile)) {
            $renamefileName = 'noprofile.png' ;
        }
        return true;
        //copy image adn storage local  
 }

?>