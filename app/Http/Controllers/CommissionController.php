<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Commission;
use Session;
use DataTables;

class CommissionController extends Controller
{
    public function index()
    {
        return view('adminconfig.commission');        
    }

    public function get_commission()
    {
        $query = Commission::select(['*']);  

        $datatables =  Datatables::of($query);              

        return $datatables
        ->addColumn('action', function ($comm) {
            return '<a href="commission/edit/'.$comm->id.'" class="btn btn-primary btn-xs" title="Edit">
                    <i class="fa fa-pencil"></i>
                    </a>
                    <a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$comm->id.');">
                    <i class="fa fa-trash-o "></i>
                    </a>';
        })
        ->make(true);
    }

    public function add()
    {
        return view('adminconfig.commission_add');        
    }

    public function store(Request $request)
    {              
        //store bank accounts
    	$inputs = $request->all();        

        $data = array(
        	'name' => $inputs['name'],
            'commission_value' => $inputs['commission_value'],
            'commission_type' => $inputs['commission_type'],
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Commission::create($data);

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/commission');
    }

    public function edit($id)
    {
        $commission = Commission::find($id);
        return view('adminconfig.commission_edit', compact('commission'));          	
    }

    public function update(Request $request)
    {              
        //store customer
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  

        $data = array(
        	'name' => $inputs['name'],
            'commission_value' => $inputs['commission_value'],
            'commission_type' => $inputs['commission_type'],
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        );
        Commission::where('id', $id)->update($data);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/commission');
    }

    public function delete($id)
    {
    	$user = Commission::where('id',$id)->delete();
        Session::flash('message', 'Commission has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function commission_name_check(Request $request) {
    	$name =  $request->get('name');
    	$id =  $request->get('id');
    	$item_exists = Commission::where('name', $name)->where('id', '!=', $id)->count();
    	if($item_exists>0)
    		return 'false';
    	else
    		return 'true';
    }
}
