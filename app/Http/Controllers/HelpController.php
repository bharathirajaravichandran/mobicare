<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Help;
use Session;
use DataTables;

class HelpController extends Controller
{
    public function index()
    {
        return view('help.list');           
    }

    public function get_cms()
    {
        $query = Help::select('id','type','question', 'answer', 'status', 'created_at');    

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="help/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="help/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        return view('help.add');        
    }

    public function store(Request $request)
    {              
        //store content
        $inputs = $request->all();        
        $data = array(
            'question' => $inputs['question'],
            'answer'   => base64_encode($inputs['answer']),           
            'status'   => $inputs['status'],
            'type'     => isset($inputs['type']) ? $inputs['type'] :'',           
            'title'    => isset($inputs['title']) ? $inputs['title'] :'', 
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        Help::create($data);

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('help');
    }

    public function edit($id)
    {
        $help = Help::find($id);
        return view('help.edit', compact('help'));       
    }

    public function update(Request $request)
    {              
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  
        $inputs = $request->all();      
        $id = $inputs['hid_id'];  
        if($request->input('type') == 'help'){
           $title = '';
        }else{
           $title = $inputs['title'];  
        }
        $data = array(
            'question' => $inputs['question'],
            'answer'   => base64_encode($inputs['answer']),           
            'type'     => isset($inputs['type']) ? $inputs['type'] :'',           
            'title'    => isset($inputs['title']) ? $inputs['title'] :'',    
            'status'   => $inputs['status'],            
            'updated_at' => date('Y-m-d H:i:s')
        );
        Help::where('id', $id)->update($data);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('help');
    }

    public function view($id)
    {
        $help = Help::find($id);
        return view('help.view', compact('help'));            
    }

    public function delete($id)
    {
        $user = Help::where('id',$id)->delete();
        Session::flash('message', 'help has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->back();
    }
}

