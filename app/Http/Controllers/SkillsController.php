<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Skills;
use App\Models\Category;
use Session;
use DataTables;

class SkillsController extends Controller
{
    public function index()
    {
        return view('skill.list');        
    }

    public function get_list(){
    	$query = Skills::with('category')->select(['*']);        

        if (request('status')!='') {
            $query->where('skills.status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('skills.created_at', '>=', $from);
            $query->whereDate('skills.created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="skills/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="skills/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add(){
        $industry = Category::where(['parent_category'=> '0','status'=> 1])->get(); 
        $subcategory = array();
        return view('skill.add', compact('industry','subcategory'));        
    }
    public function store(Request $request){
    	$inputs = $request->all(); 
    	$skills = Skills::create($inputs);

    	Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('skills');
    }
    public function edit($id){
        $skill = Skills::find($id);
        $industry = Category::where(['parent_category'=> '0','status'=> 1])->get(); 
        $subcategory = Category::where(['parent_category'=> $skill->category_id,'status'=> 1])->get(); 
        return view('skill.edit', compact('skill','industry','subcategory'));        
    }

    public function update($id,Request $request){
    	$inputs = $request->all();
    	$skills = Skills::find($id);
    	$skills->update($inputs);
    	Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('skills');
    }

    public function show($id){
        $skill = Skills::find($id);
        return view('skill.view', compact('skill'));        
    }

    public function delete($id){
        $Skills = Skills::where('id',$id)->delete();
        Session::flash('message', 'Skills deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();        
    }

    public function namecheck(Request $request) {
        $name =  $request->get('name');
        $id =  $request->get('id');
        $hid_id =  $request->get('hid_id');
        $skill_exists = Skills::where('name', $name)->where('category_id', $id)->where('id', '<>', $hid_id)->count();
        if($skill_exists>0)
            return 'false';
        else
            return 'true';
    }
}
