<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Documents;
use App\Models\Category;
use Session;
use DataTables;

class CertificateController extends Controller
{
    public function index()
    {
        return view('certificate.list');        
    }

    public function get_certificate()
    {
        $query = Documents::select(['id', 'name', 'points', 'status', 'created_at']);        

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="certificate/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="certificate/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        $industry = Category::where('status', 1)->get(); 
        return view('certificate.add', compact('industry'));        
    }

    public function store(Request $request)
    {              
        //store customer
    	$inputs = $request->all();     

        if(isset($inputs['is_mandatory']) && $inputs['is_mandatory']!='') 
            $is_mandatory = $inputs['is_mandatory'];
        else 
            $is_mandatory = 0;

        if(isset($inputs['is_primary']) && $inputs['is_primary']!='') 
            $is_primary = $inputs['is_primary'];
        else 
            $is_primary = 0;

        if(isset($inputs['is_graduate']) && $inputs['is_graduate']!='') 
            $is_graduate = $inputs['is_graduate'];
        else
            $is_graduate = 0;

        $data = array(
        	'name' => $inputs['name'],
            'points' => $inputs['points'],
            'industry_id' => $inputs['industry'],
            'is_mandatory' => $is_mandatory,
            'is_primary' => $is_primary,
            'is_graduate' => $is_graduate,
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Documents::create($data);

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('certificate');
    }

    public function edit($id)
    {
        $docs = Documents::find($id);
        $industry = Category::where('status', 1)->get(); 
        return view('certificate.edit', compact('docs', 'industry'));              	
    }

    public function update(Request $request)
    {              
        //store customer
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  

        if(isset($inputs['is_mandatory']) && $inputs['is_mandatory']!='') 
            $is_mandatory = $inputs['is_mandatory'];
        else 
            $is_mandatory = 0;

        if(isset($inputs['is_primary']) && $inputs['is_primary']!='') 
            $is_primary = $inputs['is_primary'];
        else 
            $is_primary = 0;

        if(isset($inputs['is_graduate']) && $inputs['is_graduate']!='') 
            $is_graduate = $inputs['is_graduate'];
        else
            $is_graduate = 0;

        $data = array(
            'name' => $inputs['name'],
            'points' => $inputs['points'],
            'industry_id' => $inputs['industry'],
            'is_mandatory' => $is_mandatory,
            'is_primary' => $is_primary,
            'is_graduate' => $is_graduate,
            'status' => $inputs['status'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ); //dd($data);
        Documents::where('id', $id)->update($data);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('certificate');
    }

    public function view($id)
    {
        $docs = Documents::with(['industry'])->where('id', $id)->first();
        return view('certificate.view', compact('docs'));       
    }

    public function delete($id)
    {
    	$user = Documents::where('id',$id)->delete();
        Session::flash('message', 'Certificate has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }
}
