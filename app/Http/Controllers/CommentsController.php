<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\JobReviews;
use DataTables;

class CommentsController extends Controller
{
    public function index()
    {
        $comments = JobReviews::with(['jobs', 'user'])->selectRaw('*')->groupBy('job_reviews.jobs_id')->get();
        return view('comments.list', compact('comments'));        
    }

    public function view($id)
    {
        $jobs = Jobs::where('id', $id)->first();
        $comments = JobReviews::with(['jobs', 'user'])->where('job_reviews.jobs_id', $id)->get();
        return view('comments.view', compact('jobs', 'comments'));             
    }

    public function reply(Request $request)
    {
        $jobs_id = $request->get('jobs_id');
        $id = $request->get('id');
        $reply_text = $request->get('reply_text');
        JobReviews::where('id', $id)->update(['reply_text' => $reply_text, 'reply_status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
        return redirect()->to('comments/view/'.$jobs_id);
    }
}