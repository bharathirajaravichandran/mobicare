<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Bankaccounts;
use Session;
use DataTables;

class BankaccountController extends Controller
{
    public function index()
    {
        return view('adminconfig.bank_accounts');       
    }

    public function get_bank_account()
    {
        $query = Bankaccounts::select(['*']);  

        $datatables =  Datatables::of($query);              

        return $datatables
        ->addColumn('action', function ($acc) {
            return '<a href="bank_accounts/edit/'.$acc->id.'" class="btn btn-primary btn-xs" title="Edit">
                    <i class="fa fa-pencil"></i>
                    </a>
                    <a href="bank_accounts/view/'.$acc->id.'" class="btn btn-info btn-xs" title="View">
                    <i class="fa fa-eye"></i>
                    </a>
                    <a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$acc->id.');">
                    <i class="fa fa-trash-o "></i>
                    </a>';
        })
        ->make(true);
    }

    public function add()
    {
        return view('adminconfig.bank_accounts_add');        
    }

    public function store(Request $request)
    {              
        //store bank accounts
    	$inputs = $request->all();        

        if(isset($inputs['is_primary']) && $inputs['is_primary']!='') {
            $is_primary = $inputs['is_primary'];
        } else {
            $is_primary = 0;
        }

        $data = array(
        	'bank_name' => $inputs['bank_name'],
            'display_name' => $inputs['display_name'],
            'account_no' => $inputs['account_no'],
            'ifsc_code' => $inputs['ifsc_code'],
            'branch_name' => $inputs['branch_name'],
            'is_primary' => $is_primary,
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Bankaccounts::create($data);

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/bank_accounts');
    }

    public function edit($id)
    {
        $bankaccounts = Bankaccounts::find($id);
        return view('adminconfig.bank_accounts_edit', compact('bankaccounts'));           	
    }

    public function update(Request $request)
    {              
        //store customer
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  

        if(isset($inputs['is_primary']) && $inputs['is_primary']!='') {
            $is_primary = $inputs['is_primary'];
        } else {
            $is_primary = 0;
        }

        $data = array(
        	'bank_name' => $inputs['bank_name'],
            'display_name' => $inputs['display_name'],
            'account_no' => $inputs['account_no'],
            'ifsc_code' => $inputs['ifsc_code'],
            'branch_name' => $inputs['branch_name'],
            'is_primary' => $is_primary,
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        );
        Bankaccounts::where('id', $id)->update($data);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/bank_accounts');
    }

    public function view($id)
    {
        $bankaccounts = Bankaccounts::find($id);
        return view('adminconfig.bank_accounts_view', compact('bankaccounts'));             
    }

    public function delete($id)
    {
    	$user = Bankaccounts::where('id',$id)->delete();
        Session::flash('message', 'Bank account has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function bank_name_check(Request $request) {
    	$bank_name =  $request->get('bank_name');
    	$id =  $request->get('id');
    	$bank_exists = Bankaccounts::where('bank_name', $bank_name)->where('id', '!=', $id)->count();
    	if($bank_exists>0)
    		return 'false';
    	else
    		return 'true';
    }

    public function bank_number_check(Request $request) {
        $account_no =  $request->get('account_no');
        $id =  $request->get('id');
        $acc_exists = Bankaccounts::where('account_no', $account_no)->where('id', '!=', $id)->count();
        if($acc_exists>0)
            return 'false';
        else
            return 'true';
    }
}
