<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Models\Modules;
use App\Models\UserPermissions;
use App\Models\Countries;
use DataTables;
use Mail;
use Session;

class AdminuserController extends Controller
{
    public function index()
    {
        return view('adminconfig.adminusers');        
    }

    public function get_users(Request $request)
    {
        $query = User::with('user_documents')->whereIn('users.user_types_id', array(5))->orderBy('users.created_at', 'DESC'); 

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);      

        $action = '';

        return $datatables
        ->addColumn('action', function ($user) use($action) {
            $action .='<a href="users/edit/'.$user->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            $action .='<a href="user/view/'.$user->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            if($user->user_types_id!=1) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$user->id.');"><i class="fa fa-trash-o "></i></a>';
                if($user->blocked_status==0)
                    $action .='<a href="users/lock/'.$user->id.'" class="btn btn-success btn-xs" title="Lock"><i class="fa fa-ban"></i></a>';
                else
                    $action .='<a href="users/unlock/'.$user->id.'" class="btn btn-warning btn-xs" title="Unlock"><i class="fa fa-unlock"></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        $whereData = [
            ['module_name', '<>', 'Category'],
            ['module_name', '<>', 'Administration'],
            ['module_name', '<>', 'Commission settings'],
            ['module_name', '<>', 'Bank account'],
            ['module_name', '<>', 'Admin Users'],
            ['module_name', '<>', 'User Types'],
            ['module_name', '<>', 'Site Settings']
        ];
        $modules = Modules::where('status', 1)->where($whereData)->get();
        $countries = Countries::where('status', 1)->groupBy('phonecode')->get();
        return view('adminconfig.adminuser_add', compact('modules','countries'));              
    }

    public function store(Request $request)
    {              
        $inputs = $request->all();

        $data = array(
            'first_name' => $inputs['first_name'],
            'last_name' => $inputs['last_name'],
            'phone_country_code' => $inputs['phone_country_code'],
            'phone' => $inputs['phone'],
            'email' => $inputs['email'],
            'password' => '',
            'user_types_id' => 5,
            'status' => $inputs['status'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        $userid = User::create($data)->id;  

        if(!empty($inputs['view'])) {
            $view = $inputs['view'];
            $add = $inputs['add'];
            $edit = $inputs['edit'];
            $delete = $inputs['delete'];
            $module_id = $inputs['module_id'];

            $length = count($view);
            for ($i = 0; $i < $length; $i++) {
                $data = array(
                    'user_id' => $userid,
                    'module_id' => $module_id[$i],
                    'view' => isset($view[$i])?$view[$i]:0,
                    'add' => isset($add[$i])?$add[$i]:0,
                    'edit' => isset($edit[$i])?$edit[$i]:0,
                    'delete' => isset($delete[$i])?$delete[$i]:0
                );
                UserPermissions::create($data);
            }
        }

        //send mail
        $password = $this->randomPassword();
        $data = array('name'=>$inputs['first_name'], 'email'=>$inputs['email'], 'password'=>$password);
        $mail_send = Mail::send('emails.userlogin', $data, function($message) use($inputs) {
             $message->to($inputs['email'], $inputs['first_name'])->subject('Mobicare - Admin User Login');
             $message->from(config('mail.username'),'Admin');
        });
        User::where('id', $userid)->update(['password' => bcrypt($password)]);

        if($mail_send) {
            User::where('id', $userid)->update(['email_verify' => 1]);
        }

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/users');
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function edit($id)
    {
        $whereData = [
            ['module_name', '<>', 'Category'],
            ['module_name', '<>', 'Administration'],
            ['module_name', '<>', 'Commission settings'],
            ['module_name', '<>', 'Bank account'],
            ['module_name', '<>', 'Admin Users'],
            ['module_name', '<>', 'User Types'],
            ['module_name', '<>', 'Site Settings']
        ];
        $modules = Modules::where('status', 1)->where($whereData)->get();
        $user = User::find($id);
        $permission = UserPermissions::join('modules', 'modules.id', '=', 'user_permissions.module_id')
            ->where('user_permissions.user_id', '=', $id)
            ->get();
        foreach($permission as $userlist){
            $user_permission[$userlist->module_id] = $userlist;
        }
        //echo "<pre>"; print_r($user); die;
        $countries = Countries::where('status', 1)->groupBy('phonecode')->get();
        return view('adminconfig.adminuser_edit', compact('user', 'user_permission', 'modules', 'countries'));           
    }

    public function update(Request $request)
    {              
        $inputs = $request->all(); 
        $id = $inputs['hid_id']; 
        $user = User::find($id);
        $type =  $user->user_types_id;

        $data = array(
            'first_name' => $inputs['first_name'],
            'last_name' => $inputs['last_name'],
            'phone_country_code' => $inputs['phone_country_code'],
            'phone' => $inputs['phone'],
            'email' => $inputs['email'],
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        User::where('id', $id)->update($data);  


        if(!empty($inputs['view'])) {
            $view = $inputs['view'];
            $add = $inputs['add'];
            $edit = $inputs['edit'];
            $delete = $inputs['delete'];
            $module_id = $inputs['module_id'];

            UserPermissions::where('user_id', $id)->delete();

            $length = count($view);
            for ($i = 0; $i < $length; $i++) {
                $data = array(
                    'user_id' => $id,
                    'module_id' => $module_id[$i],
                    'view' => isset($view[$i])?$view[$i]:0,
                    'add' => isset($add[$i])?$add[$i]:0,
                    'edit' => isset($edit[$i])?$edit[$i]:0,
                    'delete' => isset($delete[$i])?$delete[$i]:0
                );
                UserPermissions::create($data);
            }
        }

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/users');
    }

    public function view($id)
    { 
        $modules = Modules::where('status', 1)->get();
        $user = User::find($id);
        $user_permission = UserPermissions::join('modules', 'modules.id', '=', 'user_permissions.module_id')
            ->where('user_permissions.user_id', '=', $id)
            ->get();
        return view('adminconfig.adminuser_view', compact('user', 'user_permission', 'modules'));       
    }

    public function delete($id) {
        $user = User::find($id);
        $user->delete();
        UserPermissions::where('user_id', $id)->delete();
        Session::flash('message', 'Admin user has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->back();
    }

    public function lock(Request $request) {
        $lock = $request->segment(3);
        $id = $request->segment(4);

        if($lock=='lock') {
            $data = array('blocked_status' => 1); 
            User::where('id', $id)->update($data);  
        }
        else if($lock=='unlock') {
            $data = array('blocked_status' => 0); 
            User::where('id', $id)->update($data);  
        }

        return redirect()->back();
    }
}