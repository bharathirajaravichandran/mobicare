<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Documents;
use App\Models\Skills;
use Session;
use DataTables;

class CategoryController extends Controller
{
    public function index()
    {
        return view('category.list');        
    }

    public function get_category() 
    {
        $query = Category::select(['id', 'name', 'status', 'created_at'])->where('parent_category', 0);        

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($cat) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="category/edit/'.$cat->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="category/view/'.$cat->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$cat->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        $category = Category::get();
        return view('category.add', compact('category'));              
    }

    public function store(Request $request)
    {              
    	$inputs = $request->all();        
        $data = array(
        	'name' => $inputs['name'],
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $id = Category::create($data)->id;

        if($_FILES['image']['name']!='') {  
            $file = $request->file('image');
            $file->store('category', ['disk' => 'public']);
            $filename = $request->image->hashName();
            $image = array('image' => $filename);
            Category::where('id', $id)->update($image);
        }
        return redirect()->to('category');
    }

    public function subcategoryindex(){
        return view('category.sublist');        
    }

    public function getsubdata(){
        $query = Category::select(['id', 'parent_category', 'name', 'status', 'created_at'])->where('parent_category', '!=', 0)->orderBy('created_at', 'desc');

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->editColumn('parent_category',  function ($subcat) {
                  return Category::where(['id'=>$subcat->parent_category,'parent_category' => 0])->pluck('name')->first();
           })
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="subcategory/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="subcategory/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function subcategory(Request $request) {
        //$id = $request->segment(2);
        $category = Category::where(['parent_category' => 0,'status' => 1])->whereNull('deleted_at')->get();
        $view_document = Documents::where('status',1)->get();
        //$skills = Skills::where('status',1)->get();
        return view('category.createsubcategory ', compact('category', 'view_document','skills'));        
    }

    public function subcategory_check(Request $request) {
        $name =  $request->get('subcat_name');
        $id =  $request->get('id');
        $hid_id =  $request->get('hid_id');
        $catgory_exists = Category::where('name', $name)->where('parent_category', $id)
        ->where('id', '<>', $hid_id)->count();
        if($catgory_exists>0)
            return 'false';
        else
            return 'true';
    }

    public function subcategory_store(Request $request)
    {              
        $inputs = $request->all();      

        if(isset($inputs['hundred_pt_check']) && $inputs['hundred_pt_check']=='1')
            $hundred_pt_check = 1;
        else
            $hundred_pt_check = 0;

        if(isset($inputs['police_check']) && $inputs['police_check']=='1')
            $police_check = 1;
        else
            $police_check = 0;

        if(isset($inputs['parent_category']) && $inputs['parent_category']=='1')
            $parent_category = $inputs['hid_id'];
        else
            $parent_category = $inputs['parent_cat'];

        if(!empty($inputs['chk']))
            $ids = implode(',', $inputs['chk']);
        else
            $ids = '';

        $data = array(
            'name' => $inputs['subcat_name'],
            'status' => $inputs['subcat_status'],
            'parent_category' => $parent_category,
            'minimum_fees' => $inputs['minimum_fees'],
            'competent_skills' => isset($inputs['competent_skills'])?implode(',',$inputs['competent_skills']):'',
            'hundred_pt_check' => $hundred_pt_check,
            'certificates_uploaded' => $ids,
            'police_check' => $police_check,
            'created_at' => date('Y-m-d H:i:s')
        ); 
        $id = Category::create($data)->id;

        if($_FILES['subcat_image']['name']!='') {  
            $file = $request->file('subcat_image');
            $file->store('subcategory', ['disk' => 'public']);
            $filename = $request->subcat_image->hashName();
            $image = array('image' => $filename);
            Category::where('id', $id)->update($image);
        }
        
        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('subcategory');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $allcategory = Category::get();
        return view('category.edit', compact('category', 'allcategory'));       
    }

    public function subedit($id)
    {
        $subcategory = Category::find($id);
        $category = Category::where(['parent_category' => 0,'status' => 1])->whereNull('deleted_at')->get();
        $view_document = Documents::where('status',1)->get();
        $skills = Skills::where(['category_id'=>$subcategory->parent_category,'status'=>1])->get();
        return view('category.editsubcategory', compact('subcategory', 'category', 'view_document','skills'));        
    }

    public function update(Request $request)
    {              
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  

        $data = array(
        	'name' => $inputs['name'],
        	'status' => $inputs['status'],
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Category::where('id', $id)->update($data);

        if($_FILES['image']['name']!='') {   
            $cat = Category::where('id', $id)->first();
            $file = $cat->image;
            if(isset($file) && $file!='')
            unlink(storage_path('app/public/category/'.$file));
            Category::where('id', $id)->update(['image' => '']);

            $file = $request->file('image');
            $file->store('category', ['disk' => 'public']);
            $filename = $request->image->hashName();
            $image = array('image' => $filename);
            Category::where('id', $id)->update($image);
        }
 
        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('category');
    }

    public function view($id)
    {
        $category = Category::find($id);
        $subcategory = Category::where('parent_category',$id)->get();
        $view_document = Documents::get();
        return view('category.view', compact('category','subcategory', 'view_document'));                
    }

    public function subview($id)
    {
        $subcategory = Category::find($id);
        $subcategory->categoryname = Category::where('id',$subcategory->parent_category)->pluck('name')->first();
        $skilllist = explode(',',$subcategory->competent_skills);
        foreach($skilllist as $skill){
            $skillslisting[] = Skills::where(['id'=>$skill])->pluck('name')->first();
        }
        $subcategory->skills =$skillslisting;
        return view('category.subview', compact('subcategory'));              
    }

    public function subcat_update($id,Request $request)
    {              
        $inputs = $request->all();     
        //$parent_id = $inputs['parent_id']; 
        //$id = $inputs['hid_id'];

        if(isset($inputs['hundred_pt_check']) && $inputs['hundred_pt_check']=='1')
            $hundred_pt_check = 1;
        else
            $hundred_pt_check = 0;

        if(isset($inputs['police_check']) && $inputs['police_check']=='1')
            $police_check = 1;
        else
            $police_check = 0;

        if(!empty($inputs['chk']))
            $ids = implode(',', $inputs['chk']);
        else
            $ids = '';

        $data = array(
            'name' => $inputs['subcat_name'],
            'status' => $inputs['subcat_status'],
            'minimum_fees' => $inputs['minimum_fees'],
            'competent_skills' => isset($inputs['competent_skills'])?implode(',',$inputs['competent_skills']):'',
            'hundred_pt_check' => $hundred_pt_check,
            'certificates_uploaded' => $ids,
            'police_check' => $police_check,
            'updated_at' => date('Y-m-d H:i:s')
        );
        Category::where('id', $id)->update($data);

        if(isset($_FILES['subcat_image']['name']) && $_FILES['subcat_image']['name']!='') {  
            $cat = Category::where('id', $id)->first();
            $file = $cat->image;
            if(isset($file) && $file!='')
            unlink(storage_path('app/public/subcategory/'.$file));
            Category::where('id', $id)->update(['image' => '']);

            $file = $request->file('subcat_image');
            $file->store('subcategory', ['disk' => 'public']);
            $filename = $request->subcat_image->hashName();
            $image = array('image' => $filename);
            Category::where('id', $id)->update($image);
        }
        Session::flash('message', 'Subcategory has been updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('subcategory');
    }

    public function delete($id)
    {
    	Category::where('id',$id)->delete();
        Session::flash('message', 'Category has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function subcat_delete($id){
        Category::where('id',$id)->delete();
        Session::flash('message', 'Subcategory has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->back();
    }

    public function subcat_edit()
    {
        return redirect()->back();
    }

    public function category_check(Request $request) {
    	$name =  $request->get('name');
    	$id =  $request->get('id');
    	$catgory_exists = Category::where('name', $name)->where('id', '!=', $id)->count();
    	if($catgory_exists>0)
    		return 'false';
    	else
    		return 'true';
    }

    public function getsubcategory($id){
        $subcategory = Category::where('parent_category',$id)->get();
        $data = "";
        if(isset($subcategory) && count($subcategory) > 0){
            $data .='<option value="">Select subcategory</option>';
            foreach($subcategory as $val){
                $data .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
        }else{
            $data .='<option value="">No subcategory</option>';
        }
        return $data;
    }

    public function getskills($id){
        $skills = Skills::where('category_id',$id)->get();
        $data = "";
        if(isset($skills) && count($skills) > 0){
            foreach($skills as $val){
                $data .= '<input type="checkbox" name="competent_skills[]" value="'.$val->id.'">'.$val->name.'<br>';
            }
        }else{
            $data .= '-';
        }
        return $data;
    }
}
