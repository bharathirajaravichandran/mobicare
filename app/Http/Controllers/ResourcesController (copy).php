<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Resources;
use App\Models\ResourceFiles;
use DataTables;
use Session;
use File;

class ResourcesController (copy) extends Controller
{
    public function index()
    {
        return view('resources.list');        
    }

    public function get_resources(Request $request)
    {
        $query = Resources::orderBy('created_at', 'DESC'); 

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="resources/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="resources/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    { 
        return view('resources.add');   
    }

    public function storefiles(Request $request)
    {              
        $inputs = $request->all();

        $time = time();
        foreach($_FILES as $index => $file)
        {
            $originalfile = $file['name'];
            $fileName     = $time.'***'.$file['name'];
            $fileTempName = $file['tmp_name'];
            // check if there is an error for particular entry in array
            if(!empty($file['error'][$index]))
            {
                return false;
            }

            $fileexists = ResourceFiles::where('original_file', $originalfile)->count();
            if($fileexists>0) {
                echo 'filefound';
                exit;
            }

     
            if(!empty($fileTempName) && is_uploaded_file($fileTempName))
            {
                move_uploaded_file($fileTempName, "storage/resources/" . $fileName);
            }            
        }

        if(!empty($_FILES))
        {        
            $time = time();
            $html = '';
            foreach($_FILES as $index => $file)
            {
                $originalfile = $file['name'];
                $fileName     = $time.'***'.$file['name'];
                $fileTempName = $file['tmp_name'];
                // check if there is an error for particular entry in array
                if(!empty($file['error'][$index]))
                {
                    return false;
                }

                move_uploaded_file($fileTempName, "storage/resources/" . $fileName);
                $extension = pathinfo($fileName, PATHINFO_EXTENSION);
                $changefile = preg_replace("/[\-_ .]/", "", $fileName);

                if($extension=='pdf') {
                    $html .= '<li id="'.$changefile.'" class="list-group-item list-pdf">
                                <a href="/storage/resources/'.$fileName.'" target="_blank"><div class="checkbox">
                                    <label for="checkbox2">
                                        '.$originalfile.'
                                    </label>
                                </div></a>
                               <div class="pull-right action-buttons">                                            
                                    <a href="javascript:" class="trash del" data-file="'.$fileName.'" data-changefile="'.$changefile.'"><span class="fa fa-trash"></span></a>                     
                               </div>
                            </li>';
                }
                else if($extension=='mp4') {
                    $html .= '<li id="'.$changefile.'" class="list-group-item list-mp4">
                                <a href="/storage/resources/'.$fileName.'" target="_blank"><div class="checkbox">
                                    <label for="checkbox2">
                                        '.$originalfile.'
                                    </label>
                                </div></a>
                               <div class="pull-right action-buttons">                                            
                                    <a href="javascript:" class="trash del" data-file="'.$fileName.'" data-changefile="'.$changefile.'"><span class="fa fa-trash"></span></a>                     
                               </div>
                            </li>';
                }
                else if($extension=='doc') {
                    $html .= '<li id="'.$changefile.'" class="list-group-item list-doc">
                                <a href="/storage/resources/'.$fileName.'" target="_blank"><div class="checkbox">
                                    <label for="checkbox2">
                                        '.$originalfile.'
                                    </label>
                                </div></a>
                               <div class="pull-right action-buttons">                                            
                                    <a href="javascript:" class="trash del" data-file="'.$fileName.'" data-changefile="'.$changefile.'"><span class="fa fa-trash"></span></a>                     
                               </div>
                            </li>';
                } else {
                    $html .= '<li id="'.$changefile.'" class="list-group-item list-img">
                                <div class="checkbox">
                                    <label for="checkbox2">
                                        <a href="/storage/resources/'.$fileName.'" target="_blank"><img src="/storage/resources/'.$fileName.'" style="width:100px;height:100px;"></a>
                                    </label>
                                </div>
                               <div class="pull-right action-buttons">                                            
                                    <a href="javascript:" class="trash del" data-file="'.$fileName.'" data-changefile="'.$changefile.'"><span class="fa fa-trash"></span></a>                     
                               </div>
                            </li>';
                }
            }
            $html .= '';       
            $arr = array('time' => $time, 'html' => $html);
            echo json_encode($arr);     
        }
    }

    public function store(Request $request)
    {              
    	$inputs = $request->all();     

        $data = array(
        	'title' => $inputs['title'],
        	'description' => $inputs['description'],
            'type' => $inputs['type'],
            'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );  
        $id = Resources::create($data)->id;

        $str = $inputs['time'];
        $str = substr(trim($str), 0, -1);
        $f = explode(",",$str);
        foreach ($f as $key => $value) {
            $filename = storage_path()."/app/public/resources/".$value."*";
            foreach (glob($filename) as $filefound) {
                $filename = basename($filefound);
                $ff = explode("***",$filename);
                ResourceFiles::create([
                    'resource_id' => $id,
                    'original_file' => $ff[1],
                    'file' => $filename
                ]);                
            }
        }

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('resources');
    }

    public function edit($id)
    {
        $resources = Resources::with('resourcefiles')->find($id);
        return view('resources.edit', compact('resources'));            
    }

    public function update(Request $request)
    {              
        $inputs = $request->all();   
        $id = $inputs['hid_id'];  

        $data = array(
            'title' => $inputs['title'],
            'description' => $inputs['description'],
            'type' => $inputs['type'],
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        Resources::where('id', $id)->update($data);

        $str = $inputs['time'];
        if($str!='') {
            $str = substr(trim($str), 0, -1);
            $f = explode(",",$str);
            foreach ($f as $key => $value) {
                $filename = storage_path()."/app/public/resources/".$value."*";
                foreach (glob($filename) as $filefound) {
                   $filename = basename($filefound);
                    ResourceFiles::create([
                        'resource_id' => $id,
                        'file' => $filename
                    ]);                
                }
            }
        }
 
        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('resources');
    }

    public function view($id)
    {
        $resources = Resources::with('resourcefiles')->find($id);
        return view('resources.view', compact('resources'));          
    }

    public function delete($id)
    {
        Resources::where('id', $id)->delete();
        Session::flash('message', 'Education / Resources deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->back();
    }

    public function image_delete($id)
    {
        $res = ResourceFiles::where('id', $id)->first();
        $file = $res->file;
        unlink(storage_path('app/public/'.$file));
        ResourceFiles::where('id', $id)->delete();
        return redirect()->back();
    }

    public function title_check(Request $request) {
    	$title =  $request->get('title');
    	$id =  $request->get('id');
    	$title_exists = Resources::where('title', $title)->where('id', '!=', $id)->count();
    	if($title_exists>0)
    		return 'false';
    	else
    		return 'true';
    }

    public function deleteimage(Request $request){        
        $id = $request->get('key');
        $res = ResourceFiles::where('id', $id)->first();
        $filename = $res->file;

        //remove file
        unlink(storage_path().'/app/public/'.$filename);
        ResourceFiles::where('id', $id)->delete();
        return json_encode(1);
    }

    public function deletefile(Request $request){        
        $file = $request->get('file');
        unlink(storage_path().'/app/public/resources/'.$file);
        return 'success';
    }

    public function deletefileInedit(Request $request){        
        $file = $request->get('file');
        $id = $request->get('id');        
        unlink(storage_path().'/app/public/resources/'.$file);
        ResourceFiles::where('id', $id)->delete();
        return 'success';
    }
}