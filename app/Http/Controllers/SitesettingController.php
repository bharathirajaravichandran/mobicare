<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Sitesetting;
use Session;
use DataTables;

class SitesettingController extends Controller
{
    public function index()
    {
        return view('adminconfig.sitesetting');        
    }

    public function get_sitesetting()
    {
        $query = Sitesetting::select(['*'])->orderBy('id', 'desc');  
        $datatables =  Datatables::of($query);              

        return $datatables
        ->addColumn('action', function ($setting) {
            return '<a href="site_settings/edit/'.$setting->id.'" class="btn btn-primary btn-xs" title="Edit">
                    <i class="fa fa-pencil"></i>
                    </a>';
        })
        ->make(true);
    }

    public function edit($id)
    {
        $setting = Sitesetting::find($id);
        return view('adminconfig.sitesetting_edit', compact('setting'));          	
    }

    public function update(Request $request)
    {              
    	$inputs = $request->all();
        $id = $inputs['hid_id'];

        $array = [
            'site_name'=>$inputs['site_name'],
            'address'=>$inputs['address'],
            'phone'=>$inputs['phone'],
            'mobile'=>$inputs['mobile'],
            'email'=>$inputs['email'],
            'alter_email'=>$inputs['alter_email'],
            'copyright'=>$inputs['copyright'],
            'subscription_benefits'=>$inputs['subscription_benefits'],
            'pin_payment_secret'=>$inputs['pin_payment_secret'],
            'updated_at'=>date('Y-m-d H:i:s')
        ];
        Sitesetting::where('id', 1)->update($array);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/site_settings');
    }
}
