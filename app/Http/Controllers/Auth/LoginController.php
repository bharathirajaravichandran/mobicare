<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {   
        if($this->guard()->validate($this->credentials($request))) {

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_types_id' => 2])) {
                Auth::logout();
                Session::flash('message', 'Only admin can able to login here.'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->to('login');          
            }
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_types_id' => 3])) {
                Auth::logout();
                Session::flash('message', 'Only admin can able to login here.'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->to('login');          
            }
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_types_id' => 4])) {
                Auth::logout();
                Session::flash('message', 'Only admin can able to login here.'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->to('login');          
            }
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'blocked_status' => 1])) {
                Auth::logout();
                Session::flash('message', 'This account is blocked.'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->to('login');          
            }

            $existing_user = User::where('email', $request->email)->first();
            if ($existing_user !== null && is_null($existing_user->deleted_at) == false) {
                Session::flash('message', 'Invalid email or password.'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->to('login');
            }

            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1])) {
                return redirect()->intended('dashboard');
            }  else {
                Session::flash('message', 'This account is not activated.'); 
                Session::flash('alert-class', 'alert-danger');
                return redirect()->to('login');
            }
        } else {
            Session::flash('message', 'Invalid email or password.'); 
            Session::flash('alert-class', 'alert-danger');
            return redirect()->to('login');          
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }
}
