<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Documents;
use App\Models\UserDocuments;
use App\Models\Category;
use App\Models\Subscription;
use App\Models\UserSubscription;
use App\Models\Countries;
use App\Models\UserTypes;
use App\User;
use Illuminate\Support\Facades\Storage;
use Session;
use DataTables;
use Mail;
use Illuminate\Contracts\Auth\Guard;

class UserController extends Controller
{
    public function list_users()
    {
        return view('user.user_list');         	
    }

    public function get_users(Request $request)
    {
        $query = User::with('user_documents')->where('users.user_types_id', '!=', 1)->where('users.user_types_id', '!=', 5); 

        if (request('type')!='') {
            $query->where('users.user_types_id', request('type'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('users.created_at', '>=', $from);
            $query->whereDate('users.created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();           
        

        return $datatables
        ->addColumn('action', function ($user) use($action, $access) {
            if(in_array('view', $access)) {
                if($user->user_types_id==3||$user->user_types_id==4)
                $action .='<a href="javascript:" onclick="return fnViewdoc('.$user->id.');" class="btn btn-success btn-xs" title="View Document">
                        <i class="fa fa-file"></i></a>';
            }
            if(in_array('edit', $access)) {
                $action .='<a href="users/edit/'.$user->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="users/view/'.$user->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$user->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function user_add()
    {
        $docs = Documents::where('status', 1)->get();
        //$category = Category::where('status', 1)->where('parent_category', 0)->get();
        $category = Category::where('status', 1)->where('parent_category', 0)->get();
        $subArray = Category::where('status', 1)->where('parent_category', '!=','0')->get();
        $subcategory = array();
        foreach($subArray as $val){
            $subcategory[$val->parent_category][] = $val;
        }
        $subscription = Subscription::where('status','1')->pluck('name','id')->all();
        $countries = Countries::where('status', 1)->groupBy('phonecode')->get();
        $usertypes = UserTypes::where('status', 1)->where('name', '<>', 'Super Admin')->where('name', '<>', 'Admin')->get();
        return view('user.user_add', compact('industry', 'docs', 'category','subcategory','subscription','countries','usertypes'));              
    }

    public function user_store(Request $request)
    {              
    	$inputs = $request->all();      

        if(isset($inputs['blocked_status']) && $inputs['blocked_status']=='1')
            $blocked_status = 1;
        else
            $blocked_status = 0;

        $catArray = $subArray = $categoryArray = array();
        if(isset($inputs['industry'])){
            foreach($inputs['industry'] as $value){
                $splitcategory = explode('_', $value);
                $catArray[] = $splitcategory[0];
                if(isset($splitcategory[1])){
                    $subArray[] = $splitcategory[1];
                }
            }
        }
        $categoryArray = array_unique($catArray);
        
        $data = array(
        	'first_name' => $inputs['first_name'],
        	'last_name' => $inputs['last_name'],
            'gender' => $inputs['gender'],
            'age' => $inputs['age'],        	
        	'phone_country_code' => $inputs['phone_country_code'],
        	'phone' => $inputs['phone'],
        	'email' => $inputs['email'],
            'password' => '',
            /*'category' => implode(',', $categoryArray),
            'subcategory' => implode(',', $subArray),*/
            'fee' => $inputs['fee'],
            'description' => $inputs['description'],
            'experience' => $inputs['experience'],
            'join_date' => date('Y-m-d', strtotime($inputs['join_date'])),
            'location'  => $inputs['location'],
            'latitude'  => isset($inputs['address_latitude']) ?  $inputs['address_latitude'] : '' ,
            'longitude' => isset($inputs['address_longitude']) ? $inputs['address_longitude'] : '',
            'availability' =>  isset($inputs['availability']) ? implode(',',$inputs['availability']) : '' , 
        	'user_types_id' => $inputs['type'],
            'health_condition' => $inputs['health_condition'],
            'blocked_status' => $blocked_status,
            'subscription_id' => $inputs['subscription_id'],
        	'status' => $inputs['status'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        $id = User::create($data)->id;

        if($inputs['subscription_id'] !=''){
            $plandetails = Subscription::find($inputs['subscription_id']);
            $enddate = date('Y-m-d', strtotime("+".$plandetails->duration." months"));
            $plan = ['user_id'=>$id,
                    'plan_id' =>$inputs['subscription_id'], 
                    'plan_name' => $plandetails->name, 
                    'start_date' => date('Y-m-d'), 
                    'end_date' => $enddate, 
                    'amount_paid' =>'', 
                    'status' =>'1', 
                    'expiry_status'=>'1'];
            $checkplan = UserSubscription::where(['user_id'=>$id, 'plan_id' =>$inputs['subscription_id'],'status' =>'1', 'expiry_status'=>'1' ])->first();
            if(!isset($checkplan)){
                UserSubscription::create($plan);
            }

        }

        if($_FILES['photo']['name']!='') {  
            $file = $request->file('photo');
            $file->store('user', ['disk' => 'public']);
            $filename = $request->photo->hashName();
            $user_photo = array('photo' => $filename);
            User::where('id', $id)->update($user_photo);
        }

        if($_FILES['video']['name']!='') {  
            $file = $request->file('video');
            $file->store('user/video', ['disk' => 'public']);
            $filename = $request->video->hashName();
            $video = array('video' => $filename);
            User::where('id', $id)->update($video);
        }

        if($request->hasfile('cert'))
        {
            foreach($request->file('cert') as $key => $file)
            {   
                $filename=time().'_'.$file->getClientOriginalName();
                $file->storeAs('certificates', $filename, ['disk' => 'public']);
                $doc_data = array(
                    'user_id' => $id,
                    'industry_id' => 13,
                    'documents_id' => $key,
                    'file_path' => $filename, 
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );                
                UserDocuments::create($doc_data);        
            }            
        }

        if($request->hasfile('cert'))
        {
            User::where('id', $id)->update(['verified_status' => 1]);
        }

        //send mail
        $password = $this->randomPassword();
        $data = array('name'=>$inputs['first_name'], 'email'=>$inputs['email'], 'password'=>$password);
        $mail_send = Mail::send('emails.userlogin', $data, function($message) use($inputs) {
             $message->to($inputs['email'], $inputs['first_name'])->subject('Mobicare - User Login');
             $message->from(config('mail.username'),'Admin');
        });
        User::where('id', $id)->update(['password' => bcrypt($password)]);

        if($mail_send) {
            User::where('id', $id)->update(['email_verify' => 1]);
        }

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('users');
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function user_edit($id)
    {
        $user = User::find($id);
        $view_document = UserDocuments::with(['documents'])->where('user_id', $id)->get(); 
        $category = Category::where('status', 1)->where('parent_category', 0)->get();
        $subArray = Category::where('status', 1)->where('parent_category', '!=','0')->get();
        $subcategory = array();
        foreach($subArray as $val){
            $subcategory[$val->parent_category][] = $val;
        }
        $subscription = Subscription::where('status','1')->pluck('name','id')->all();
       
        $plan = UserSubscription::where(['user_id'=>$id, 'plan_id'=>$user->subscription_id, 'status'=>'1'])->orderBy('id','DESC')->first();
        $countries = Countries::where('status', 1)->groupBy('phonecode')->get();
        $usertypes = UserTypes::where('status', 1)->where('name', '<>', 'Super Admin')->where('name', '<>', 'Admin')->get();
        return view('user.user_edit', compact('industry', 'user', 'view_document', 'category', 'subcategory','subscription','plan','countries','usertypes'));
    }

    public function user_update(Request $request)
    {              
    	$inputs = $request->all(); 
    	$id = $inputs['hid_id']; 

        if(isset($inputs['blocked_status']) && $inputs['blocked_status']=='1')
            $blocked_status = 1;
        else
            $blocked_status = 0;

        $catArray = $subArray = $categoryArray = array();
        if(isset($inputs['industry'])){
            foreach($inputs['industry'] as $value){
                $splitcategory = explode('_', $value);
                $catArray[] = $splitcategory[0];
                if(isset($splitcategory[1])){
                    $subArray[] = $splitcategory[1];
                }
            }
        }
        $categoryArray = array_unique($catArray);

        $data = array(
            'first_name' => $inputs['first_name'],
            'last_name' => $inputs['last_name'],
            'gender' => $inputs['gender'],
            'age' => $inputs['age'],            
            'phone_country_code' => $inputs['phone_country_code'],
            'phone' => $inputs['phone'],
            'email' => $inputs['email'],            
            /*'category' => implode(',', $categoryArray),
            'subcategory' => implode(',', $subArray),*/
            'fee' => $inputs['fee'],
            'description' => $inputs['description'],
            'experience' => $inputs['experience'],
            'join_date' => date('Y-m-d', strtotime($inputs['join_date'])),
            'location' => $inputs['location'],
            'latitude'  => isset($inputs['address_latitude']) ?  $inputs['address_latitude'] : '' ,
            'longitude' => isset($inputs['address_longitude']) ? $inputs['address_longitude'] : '',
            'availability' =>  isset($inputs['availability']) ? implode(',',$inputs['availability']) : '' , 
            'user_types_id' => $inputs['type'],
            'health_condition' => $inputs['health_condition'],
            'blocked_status' => $blocked_status,
            'subscription_id' => $inputs['subscription_id'],
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        //echo "<pre>";
        //print_r($data); die;
        User::where('id',$id)->update($data);

        if($inputs['subscription_id'] !=''){
            $plandetails = Subscription::find($inputs['subscription_id']);
            $enddate = date('Y-m-d', strtotime("+".$plandetails->duration." months"));
            $plan = ['user_id'=>$id,
                    'plan_id' =>$inputs['subscription_id'], 
                    'plan_name' => $plandetails->name, 
                    'start_date' => date('Y-m-d'), 
                    'end_date' => $enddate, 
                    'amount_paid' =>'', 
                    'status' =>'1', 
                    'expiry_status'=>'1'];
            $checkplan = UserSubscription::where(['user_id'=>$id, 'plan_id' =>$inputs['subscription_id'],'status' =>'1', 'expiry_status'=>'1' ])->first();
            if(!isset($checkplan)){
                UserSubscription::create($plan);
            }

        }

        if($_FILES['photo']['name']!='') {   
            //remove old file
            $user = User::where('id', $id)->first();
            $file = $user->photo;
            if(isset($file) && $file!='')
            unlink(storage_path('app/public/user/'.$file));
            User::where('id', $id)->update(['photo' => '']);

            $file = $request->file('photo');
            $file->store('user', ['disk' => 'public']);
            $filename = $request->photo->hashName();
            $user_photo = array('photo' => $filename);
            User::where('id', $id)->update($user_photo);
        }

        if($_FILES['video']['name']!='') {  
            $user = User::where('id', $id)->first();
            $file = $user->video;
            if(isset($file) && $file!='')
            unlink(storage_path('app/public/user/video/'.$file));
            User::where('id', $id)->update(['video' => '']);

            $file = $request->file('video');
            $file->store('user/video', ['disk' => 'public']);
            $filename = $request->video->hashName();
            $video = array('video' => $filename);
            User::where('id', $id)->update($video);
        }

        if($request->hasfile('cert'))
        {
            UserDocuments::where('user_id', $id)->delete();
            foreach($request->file('cert') as $key => $file)
            {   
                $filename=time().'_'.$file->getClientOriginalName();
                $doc_data = array(
                    'user_id' => $id,
                    'industry_id' => 1,
                    'documents_id' => $key,
                    'file_path' => $filename, 
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ); 
                $file->storeAs('certificates', $filename, ['disk' => 'public']);
                UserDocuments::create($doc_data);        
            }
            User::where('id', $id)->update(['verified_status' => 1]);
        }

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('users');
    }

    public function user_view($id) {
        $user = User::with(['user_types', 'category', 'user_documents'])->find($id);
        $doc = UserDocuments::with(['documents'])->where('user_id',$id)->get();
        $plan = UserSubscription::where(['user_id'=>$id,'status'=>'1'])->orderBy('id','DESC')->get();
        return view('user.user_view', compact('user', 'doc','plan'));
    }

    public function user_delete($id) {
    	$user = User::find($id);
        $user->delete();
        Session::flash('message', 'User has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function email_check(Request $request) {
    	$email =  $request->get('email');
    	$id =  $request->get('id');
    	$email_exists = User::where('email', $email)->where('id', '!=', $id)->where('deleted_at', NULL)->count();
    	if($email_exists>0)
    		return 'false';
    	else
    		return 'true';
    }

    public function phone_check(Request $request) {
    	$phone =  $request->get('phone');
    	$id =  $request->get('id');
    	$phone_exists = User::where('phone', $phone)->where('id', '!=', $id)->count();
    	if($phone_exists>0)
    		return 'false';
    	else
    		return 'true';
    }

    public function view_document(Request $request)
    {
        $id = $request->get('id');
        $userdoc = UserDocuments::where('user_id', $id)->get();
        $html = '';

        if(count($userdoc)>0) {
            $html = '<table style="width:100%;">
                      <tr>
                        <th>Name</th>
                        <th>Action</th>
                      </tr>';
            foreach ($userdoc as $key => $value) {
                  $html .= '<tr>
                    <td>'.$value->documents->name.'</td>
                    <td><a href="'.url('storage/certificates').'/'.$value->file_path.'" target="_blank" style="color:crimson;">Download</a></td>
                  </tr>';
            }
            $html .= '</table>';
        } else {
            $html = '<p class="error">No document added</p>';
        }

        echo $html;
    }

    public function subcategory(Request $request)
    {
        $cat_id = $request->get('cat_id');
        if($cat_id!='') {
            $cat = Category::where('parent_category', $cat_id)->get();
            $html = '';
            if(count($cat)>0) {
                $html = '<select name="subcategory" class="form-control"><option value="">Select</option>';
                foreach ($cat as $key => $value) {
                      $html .= '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                $html .= '</select>';
            } else {
                $html = '<select name="subcategory" class="form-control"><option value="">Select</option></select>';
            }
            echo $html;
        } else {
            echo '<select name="subcategory" class="form-control"><option value="">Select</option></select>';
        }
    }
}
