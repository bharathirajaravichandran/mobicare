<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\UserTypes;
use Session;
use DataTables;

class UsertypesController extends Controller
{
    public function index()
    {
        return view('adminconfig.type_list');        
    }

    public function get_usertypes() 
    {
        $query = UserTypes::where('name', '<>', 'Super Admin')->where('name', '<>', 'Admin')->select(['*']);        

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($type) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="user_types/edit/'.$type->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            // if(in_array('delete', $access)) {
            //     $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$type->id.');"><i class="fa fa-trash-o "></i></a>';
            // }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        return view('adminconfig.type_add');              
    }

    public function store(Request $request)
    {              
    	$inputs = $request->all();        
        $data = array(
        	'name' => $inputs['name'],
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        UserTypes::create($data)->id;
        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/user_types');
    }

    public function edit($id)
    {
        $types = UserTypes::find($id);
        return view('adminconfig.type_edit', compact('types'));       
    }

    public function update(Request $request)
    {              
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  

        $data = array(
        	'name' => $inputs['name'],
        	'status' => $inputs['status'],
        	'updated_at' => date('Y-m-d H:i:s')
        );
        UserTypes::where('id', $id)->update($data);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('admin/user_types');
    }

    public function delete($id)
    {
    	UserTypes::where('id',$id)->delete();
        Session::flash('message', 'User type has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function name_check(Request $request) {
    	$name =  $request->get('name');
    	$id =  $request->get('id');
    	$name_exists = UserTypes::where('name', $name)->where('id', '!=', $id)->count();
    	if($name_exists>0)
    		return 'false';
    	else
    		return 'true';
    }
}
