<?php

namespace App\Http\Controllers\App;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Category;
use App\Models\Jobs;
use App\Models\JobConversations;
use App\Models\JobReviews;
use App;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;
use App\Services\MobicareServices as MobicareServices;

class APIController extends Controller
{
    private $request;
    private $mobicareservices;
    public function __construct(Request $request)
    {   
        $this->request  = $request;
        $this->mobicareservices = new MobicareServices();        
        $this->user = new User;
    }

   public function register()
    {
       return $this->mobicareservices->register($this->request);
    }

   public function createProfile()
    {
       return $this->mobicareservices->createProfile($this->request);
    }

  public function login()
    {
       return $this->mobicareservices->login($this->request);
    }

  public function LogOut()
    {
       return $this->mobicareservices->LogOut($this->request);
    }

  public function email_verify()
    {
       return $this->mobicareservices->email_verify($this->request);
    }

  public function GetProfile()
    {
       return $this->mobicareservices->GetProfile($this->request);
    }

 public function getAllproviders()
    {
       return $this->mobicareservices->getAllproviders($this->request);
    }

 public function ProfileUpdate()
    {
       return $this->mobicareservices->ProfileUpdate($this->request);
    }

  public function category()
    {
       return $this->mobicareservices->category($this->request);
    }

   public function subcategory()
    {
       return $this->mobicareservices->subcategory($this->request);
    }

   public function providers()
    {
       return $this->mobicareservices->providers($this->request);
    }

  public function provider_details()
    {
       return $this->mobicareservices->provider_details($this->request);
    }

  public function Createjob()
    {
       return $this->mobicareservices->Createjob($this->request);
    }

  public function Appointments()
    {
       return $this->mobicareservices->Appointments($this->request);
    }
   public function AppointmentSingleData()
    {
       return $this->mobicareservices->AppointmentSingleData($this->request);
    }
    
  public function GetAllJobs()
    {
       return $this->mobicareservices->GetAllJobs($this->request);
    }

  public function job_details()
    {
       return $this->mobicareservices->job_details($this->request);
    }

  public function reviews()
    {
       return $this->mobicareservices->reviews($this->request);
    }

 public function GetMyallChat()
     {
        return $this->mobicareservices->GetMyallChat($this->request);
     }

 public function SaveMyChat()
     {
        return $this->mobicareservices->SaveMyChat($this->request);
     }
    
  public function skills()
     {
        return $this->mobicareservices->skills($this->request);
     }


  public function Dashboard()
    {
       return $this->mobicareservices->Dashboard($this->request);
    }

    
  public function Makebooking()
    {
       return $this->mobicareservices->Makebooking($this->request);
    }


  public function subscription()
    {
      return $this->mobicareservices->subscription($this->request);
    }

   public function JobBookingList()
    {
       return $this->mobicareservices->JobBookingList($this->request);
    }

   // public function MycareJobs()
   //   {
   //      return $this->mobicareservices->MycareJobs($this->request);
   //   }

   public function PaymentHistroy()
   {
     return $this->mobicareservices->PaymentHistroy($this->request);
   }

   public function PricingGuide()
   {
      return $this->mobicareservices->PricingGuide($this->request);
   }

   public function Educationlist()
   {
      return $this->mobicareservices->Educationlist($this->request);
   } 

   public function CommentsReview()
   {
      return $this->mobicareservices->CommentsReview($this->request);
   }

    public function ReviewPost()
   {
      return $this->mobicareservices->ReviewPost($this->request);
   }

   public function Help()
   {
      return $this->mobicareservices->Help($this->request);
   }

   public function Notifications()
   {
      return $this->mobicareservices->Notifications($this->request);
   }

   public function settings()
   {
      return $this->mobicareservices->settings($this->request);
   }

   public function setting_update()
   {
      return $this->mobicareservices->setting_update($this->request);
   }

   public function verify_providers()
   {
      return $this->mobicareservices->verify_providers($this->request);
   }

   public function apply_jobs()
   {
      return $this->mobicareservices->apply_jobs($this->request);
   }

    public function DeleteDocument()
    {
      return $this->mobicareservices->DeleteDocument($this->request);
    } 

    public function AddNotes()
    {
      return $this->mobicareservices->AddNotes($this->request);
    }

    public function Myoffers()
    {
      return $this->mobicareservices->Myoffers($this->request);
    } 

    public function CommanUpdate()
    {
       return $this->mobicareservices->CommanUpdate($this->request);
    } 

     public function AddUserBankDetails()
    {
       return $this->mobicareservices->AddUserBankDetails($this->request);
    }
    
    public function GetUserBankDetails()
    {
       return $this->mobicareservices->GetUserBankDetails($this->request);
    } 

    public function payment()
    {
       return $this->mobicareservices->payment($this->request);
    }  
}