<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Questions;
use App\Models\Category;
use Session;
use DataTables;

class QuestionsController extends Controller
{
    public function index()
    {
        return view('questions.list');      
    }

    public function get_list()
    {
        $query = Questions::select(['id', 'type', 'question', 'answer_type', 'status', 'created_at']);        

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->editColumn('type',  function ($row) {
                  return ucfirst($row->type);
           })
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="questions/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="questions/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        $industry = Category::where(['status' => 1, 'parent_category'=>'0'])->get(); 
        return view('questions.add', compact('industry'));       
    }

    public function store(Request $request){
    	$inputs = $request->all();  
        if($inputs['answer_option'])
        {
          $inputs['answer_option'] = implode(',',$inputs['answer_option']);
        }    	

    	$inputs['is_mandatory'] = isset($inputs['is_mandatory']) && $inputs['is_mandatory']==1 ?'1':'0' ;
    	//echo "<pre>";print_r($inputs);die;
    	$question = Questions::create($inputs);
    	Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('questions');
    }

    public function edit($id)
    {
    	$questions = Questions::find($id);
        $industry = Category::where(['status' => 1, 'parent_category'=>'0'])->get(); 
        if($questions->subcategory != ''){
        	$subcategory = Category::where(['status' => 1, 'parent_category'=>$questions->category])->get(); 
    	}
        return view('questions.edit', compact('industry', 'questions','subcategory'));        
    }

    public function update($id, Request $request){
    	$question = Questions::find($id);
    	$inputs = $request->all();  
        if($inputs['answer_option'])
        {
          $inputs['answer_option'] = implode(',',$inputs['answer_option']);
        } 
    	$inputs['is_mandatory'] = isset($inputs['is_mandatory']) && $inputs['is_mandatory']==1 ?'1':'0' ;
    	//echo "<pre>";print_r($inputs);die;
    	$question->update($inputs);
    	Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('questions');
    }

    public function show($id){
        $questions = Questions::find($id);
        return view('questions.view', compact('questions'));        
    }

    public function delete($id){
        $Skills = Questions::where('id',$id)->delete();
        Session::flash('message', 'Question deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();      
    }
}
