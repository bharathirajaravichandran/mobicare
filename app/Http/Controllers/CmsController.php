<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Cms;
use Session;
use DataTables;

class CmsController extends Controller
{
    public function index()
    {
        return view('cms.list');          	
    }

    public function get_cms()
    {
        $query = CMS::select('id', 'pagename', 'pagelink', 'pagecontent', 'status', 'created_at');    

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="cms/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="cms/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        return view('cms.add');        
    }

    public function store(Request $request)
    {              
        //store content
    	$inputs = $request->all();        
        $data = array(
        	'pagename' => $inputs['pagename'],
            'pagelink' => $inputs['pagelink'],
            'pagecontent' => $inputs['pagecontent'],
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Cms::create($data);

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('cms');
    }

    public function edit($id)
    {
        $cms = Cms::find($id);
        return view('cms.edit', compact('cms'));       
    }

    public function update(Request $request)
    {              
        //store customer
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  

        $data = array(
        	'pagename' => $inputs['pagename'],
            'pagelink' => $inputs['pagelink'],
            'pagecontent' => $inputs['pagecontent'],
            'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Cms::where('id', $id)->update($data);

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('cms');
    }

    public function view($id)
    {
        $cms = Cms::find($id);
        return view('cms.view', compact('cms'));            
    }

    public function delete($id)
    {
    	$user = Cms::where('id',$id)->delete();
        Session::flash('message', 'Cms has been deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }
}
