<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subscription;
//use DataTables;
use Session;

class SubscriptionController extends Controller
{
	public function index(){
    	$subscription = Subscription::orderBy('created_at', 'DESC')->get(); 
    	//print_r($subscription); die;
        return view('subscription.list', compact('subscription'));        
	}

    public function add(){
        $subscription = Subscription::orderBy('created_at','DESC')->pluck('id')->first();
        $subscription_id = $subscription + 1;
        return view('subscription.add', compact('subscription_id'));        
    }

    public function store(Request $request)
    { 
    	$inputs = $request->all();
    	$subscription = Subscription::create($inputs);

    	Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('subscription');
    }

    public function edit($id){
        $subscription = Subscription::find($id);
        return view('subscription.edit', compact('subscription'));         
    }

    public function update($id, Request $request){
    	$inputs = $request->all();
    	$subscription = Subscription::find($id);
    	$subscription->update($inputs);
    	Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('subscription');
    }

    public function show($id){
        $subscription = Subscription::find($id);
        return view('subscription.view', compact('subscription'));
    }

    public function delete($id){
        $subscription = Subscription::where('id',$id)->delete();
        Session::flash('message', 'Subscription deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function subscription_name_check(Request $request) {
    	$name =  $request->get('name');
    	$id =  $request->get('id');
    	$plan_exists = Subscription::where('name', $name)->where('id', '!=', $id)->count();
    	if($plan_exists>0)
    		return 'false';
    	else
    		return 'true';
    }
}
