<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Resources;
use App\Models\ResourceFiles;
use DataTables;
use Session;
use File;

class ResourcesController extends Controller
{
    public function index()
    {
        return view('resources.list');        
    }

    public function get_resources(Request $request)
    {
        $query = Resources::select(['*']); 

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('created_at', '>=', $from);
            $query->whereDate('created_at', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if(in_array('edit', $access)) {
                $action .='<a href="resources/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="resources/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    { 
        return view('resources.add');   
    }

    public function store(Request $request)
    {              
    	$inputs = $request->all();     

        $data = array(
        	'title' => $inputs['title'],
        	'description' => $inputs['description'],
            'type' => $inputs['type'],
            'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );  
        $id = Resources::create($data)->id;
        
        if($request->hasfile('upfile'))
        {
            foreach ($request->upfile as $photo) 
            {
                $filename = $photo->store('resources', ['disk' => 'public']);
                ResourceFiles::create([
                    'resource_id' => $id,
                    'file' => $filename
                ]);
            }
        }

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('resources');
    }

    public function edit($id)
    {
        $resources = Resources::with('resourcefiles')->find($id);
        return view('resources.edit', compact('resources'));            
    }

    public function update(Request $request)
    {              
        $inputs = $request->all();   
        $id = $inputs['hid_id'];  

        $data = array(
            'title' => $inputs['title'],
            'description' => $inputs['description'],
            'type' => $inputs['type'],
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        Resources::where('id', $id)->update($data);

        if($inputs['image_delete']=='yes') {
            $res = ResourceFiles::where('resource_id', $id)->first();
            $file = $res->file;
            unlink(storage_path('app/public/'.$file));
            ResourceFiles::where('resource_id', $id)->delete();
        }

        if($request->hasfile('upfile'))
        {
            $res = ResourceFiles::where('resource_id', $id)->first();
            $file = isset($res->file)?$res->file:'';
            if(isset($file) && $file!='') {
                unlink(storage_path('app/public/'.$file));
                ResourceFiles::where('resource_id', $id)->delete();
            }
            foreach ($request->upfile as $photo) 
            {
                $filename = $photo->store('resources', ['disk' => 'public']);
                ResourceFiles::create([
                    'resource_id' => $id,
                    'file' => $filename
                ]);
            }
        }
 
        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('resources');
    }

    public function view($id)
    {
        $resources = Resources::with('resourcefiles')->find($id);
        return view('resources.view', compact('resources'));          
    }

    public function delete($id)
    {
        Resources::where('id', $id)->delete();
        Session::flash('message', 'Education / Resources deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
        return redirect()->back();
    }

    public function title_check(Request $request) {
    	$title =  $request->get('title');
    	$id =  $request->get('id');
    	$title_exists = Resources::where('title', $title)->where('id', '!=', $id)->count();
    	if($title_exists>0)
    		return 'false';
    	else
    		return 'true';
    }
}