<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Models\Jobs;
use Auth;

class AdminController extends Controller
{
    public function dashboard()
    {
    	$customers = User::where('user_types_id', '!=', 1)->where('user_types_id', '!=', 5)->count();
        $carerequests = Jobs::count();
        return view('adminhome', compact('customers', 'carerequests'));
    }

    public function logout()
    {
    	Auth::logout();
  		return redirect('/login');
    }
}