<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Jobs;
use App\Models\Category;
use App\User;
use App\Models\JobAssigned;
use App\Models\JobSuggestion;
use DataTables;
use Session;
use DB;

class CareRequestController extends Controller
{
    public function index()
    {
        return view('carerequest.list');        
    }

    public function get_care_request(Request $request)
    {
        $query = Jobs::select(['*']); 

        if (request('status')!='') {
            $query->where('status', request('status'));                                  
        }   

        if (request('start_date')!='' && request('end_date')!='') {
            $from = date("Y-m-d",strtotime(request('start_date')));
            $to = date("Y-m-d",strtotime(request('end_date')));
            $query->whereDate('start_date', '>=', $from);
            $query->whereDate('end_date', '<=', $to);               
        }   

        $datatables =  Datatables::of($query);              

        $action = '';
        $access = checkAdminPermission();      

        return $datatables
        ->addColumn('action', function ($row) use($action, $access) {
            if($row->request_type == 'mobicare_match'){
                $action .='<a href="care_request/match/'.$row->id.'" class="btn btn-success btn-xs" title="Mobicare match">
                        <i class="fa fa-check-circle"></i></a>';
            }
            if(in_array('edit', $access)) {
                $action .='<a href="care_request/edit/'.$row->id.'" class="btn btn-primary btn-xs" title="Edit">
                        <i class="fa fa-pencil"></i></a>';
            }
            if(in_array('view', $access)) {
                $action .='<a href="care_request/view/'.$row->id.'" class="btn btn-info btn-xs" title="View">
                        <i class="fa fa-eye"></i></a>';
            }
            if(in_array('delete', $access)) {
                $action .='<a class="btn btn-danger btn-xs" href="#deleteModal" title="Delete" class="trigger-btn" data-toggle="modal" onclick="return showDeleteModal('.$row->id.');"><i class="fa fa-trash-o "></i></a>';
            }   
            return $action;
        })
        ->make(true);
    }

    public function add()
    {
        $job_id = Jobs::all()->last()->id;
        $job_id = $job_id + 1;
        $industry = Category::where(['status' => 1, 'parent_category'=>'0'])->get(); 
        return view('carerequest.add', compact('job_id','industry'));        
    }

    public function store(Request $request)
    {              
    	$inputs = $request->all();    
        if($inputs['request_type'] == 'normal')
            $amount_charged = 0;
        else if($inputs['request_type'] == 'mark_it_urgent')
            $amount_charged = 30;
        else if($inputs['request_type'] == 'mobicare_match')
            $amount_charged = 40;    
        $data = array(
        	'type' => $inputs['type'],
        	'job_id' => $inputs['job_id'],
            'category_id' => $inputs['category_id'],
            'subcategory_id' => isset($inputs['subcategory_id'])?$inputs['subcategory_id']:'',
        	'name' => $inputs['name'],
        	'description' => $inputs['description'],
        	'venue' => $inputs['venue'],
        	'budget' => $inputs['budget'],
        	'start_date' => date('Y-m-d', strtotime($inputs['start_date'])),
        	'start_time' => date('H:i', strtotime($inputs['start_time'])),
        	'end_date' => date('Y-m-d', strtotime($inputs['end_date'])),
        	'end_time' => date('H:i', strtotime($inputs['end_time'])),
        	'work_with_children' => $inputs['work_with_children'],
            'request_type' => $inputs['request_type'],
            'amount_charged' => $amount_charged,
            /*'working_days' =>  ($inputs['working_days']) ? implode(',',$inputs['working_days']) : '' , */
        	'status' => $inputs['status'],
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        );
        $id = Jobs::create($data)->id;

        if(isset($_FILES['job_file']['name']) && $_FILES['job_file']['name']!='') {  
            $file = $request->file('job_file');
            $file->store('jobs', ['disk' => 'public']);
            $filename = $request->job_file->hashName();
            $f = array('job_file' => $filename);
            Jobs::where('id', $id)->update($f);
        }

        Session::flash('message', 'Successfully added'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('care_request');
    }

    public function edit($id)
    {
        $jobs = Jobs::find($id);
        $industry = Category::where(['status' => 1, 'parent_category'=>'0'])->get();
        $subcategory = Category::where(['status' => 1, 'parent_category'=>$jobs->category_id])->get();
        return view('carerequest.edit', compact('jobs','industry','subcategory'));          	
    }

    public function update(Request $request)
    {              
    	$inputs = $request->all();      
    	$id = $inputs['hid_id'];  
        if($inputs['request_type'] == 'normal')
            $amount_charged = 0;
        else if($inputs['request_type'] == 'mark_it_urgent')
            $amount_charged = 30;
        else if($inputs['request_type'] == 'mobicare_match')
            $amount_charged = 40;
        $data = array(
        	'type' => $inputs['type'],
        	'job_id' => $inputs['job_id'],
            'category_id' => $inputs['category_id'],
            'subcategory_id' => isset($inputs['subcategory_id'])?$inputs['subcategory_id']:'',
        	'name' => $inputs['name'],
        	'description' => $inputs['description'],
        	'venue' => $inputs['venue'],
        	'budget' => $inputs['budget'],
        	'start_date' => date('Y-m-d', strtotime($inputs['start_date'])),
            'start_time' => date('H:i', strtotime($inputs['start_time'])),
            'end_date' => date('Y-m-d', strtotime($inputs['end_date'])),
            'end_time' => date('H:i', strtotime($inputs['end_time'])),
        	'work_with_children' => $inputs['work_with_children'],
            'request_type' => $inputs['request_type'],
            'amount_charged' => $amount_charged,
            /*'working_days' =>  isset($inputs['working_days']) ? implode(',',$inputs['working_days']) : '' ,*/
        	'status' => $inputs['status'],
        	'updated_at' => date('Y-m-d H:i:s')
        );
        Jobs::where('id', $id)->update($data);

        if(isset($_FILES['job_file']['name']) && $_FILES['job_file']['name']!='') {  
            $job = Jobs::where('id', $id)->first();
            $file = @$job->job_file;
            if($file!='') {
                unlink(storage_path('app/public/jobs/'.$file));
                Jobs::where('id', $id)->update(['job_file' => '']);
            }

            $file = $request->file('job_file');
            $file->store('jobs', ['disk' => 'public']);
            $filename = $request->job_file->hashName();
            $f = array('job_file' => $filename);
            Jobs::where('id', $id)->update($f);
        }
 
        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('care_request');
    }

    public function view($id)
    {
        $jobs = Jobs::with(['job_assigned'])->find($id);
        return view('carerequest.view', compact('jobs'));       
    }

    public function delete($id)
    {
        Jobs::where('id', $id)->delete();
        Session::flash('message', 'Care request deleted'); 
        Session::flash('alert-class', 'alert-danger'); 
    	return redirect()->back();
    }

    public function care_request_name_check(Request $request) {
    	$name =  $request->get('name');
    	$id =  $request->get('id');
    	$job_exists = Jobs::where('name', $name)->where('id', '!=', $id)->count();
    	if($job_exists>0)
    		return 'false';
    	else
    		return 'true';
    }

    public function match($id){
        $jobs = Jobs::find($id);
        $users = User::select('users.*',DB::raw('avg(job_reviews.ratings) AS rating'))
                    ->join('job_reviews','job_reviews.user_id','=','users.id','left')
                    ->whereIn('user_types_id', array(3,4))
                    // ->where('category','LIKE','%'.$jobs->category_id.'%')
                    // ->where('subcategory','LIKE','%'.$jobs->subcategory_id.'%')
                    ->where(['users.status'=>'1'])
                    ->groupBy('job_reviews.user_id')
                    ->orderBy('rating','DESC')->get();
        $suggestion = JobSuggestion::where(['job_id'=>$id, 'status'=>1])->pluck('user_id')->all();
        foreach($users as $user){
            $catArray = explode(',', $user->category);
            $subcatArray = explode(',', $user->subcategory);
            $user->Category = Category::whereIn('id',$catArray)->pluck('name')->all();
            $user->Subcategory = Category::whereIn('id',$subcatArray)->pluck('name')->all();
        }
        //echo "<pre>";print_r($users); die;
        return view('carerequest.match', compact('jobs','users','suggestion'));
    }

    public function matchstore(Request $request){
        $data = $request->all();
        $suggestion = JobSuggestion::where(['job_id'=>$data['job_id']])->pluck('user_id')->all();
        $updsuggestion = JobSuggestion::where(['job_id'=>$data['job_id']])->update(['status'=> '0']);
        foreach($data['user_id'] as $user){
            $status = '1';
            $inputs = array('assigned_at' =>date('Y-m-d H:i:s'),
                            'job_id' => $data['job_id'],
                            'user_id' => $user,
                            'status'=>$status);
            $check = JobSuggestion::where(['job_id' => $data['job_id'], 'user_id' => $user])->first();
            if(isset($check)){
                $suggestion = JobSuggestion::find($check->id);
                $suggestion->update($inputs);
            }else{
                JobSuggestion::create($inputs);
            }
        }
        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('care_request');
    }
}