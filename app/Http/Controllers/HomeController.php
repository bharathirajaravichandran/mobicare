<?php

namespace App\Http\Controllers;
use App\Models\Countries;
use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function myprofile()
    {
        $user = Auth::user();
        return view('myprofile', compact('user'));              
    }

    public function editprofile()
    {
        $user = Auth::user();
        $countries = Countries::where('status', 1)->groupBy('phonecode')->get();
        return view('editprofile', compact('countries','user'));              
    }

    public function update(Request $request)
    {              
        $inputs = $request->all(); 
        $id = $inputs['hid_id']; 

        $data = array(
            'first_name' => $inputs['first_name'],
            'last_name' => $inputs['last_name'],         
            'phone_country_code' => $inputs['phone_country_code'],
            'phone' => $inputs['phone'],
            'email' => $inputs['email'],              
            'status' => $inputs['status'],
            'updated_at' => date('Y-m-d H:i:s')
        ); 
        User::where('id',$id)->update($data);

        if(isset($inputs['password']) && isset($inputs['password_confirm'])) {
            $password=Hash::make($inputs['password']);
            User::where('id',$id)->update(['password'=>$password]);
            Auth::logout();
            return redirect('/login');
        }

        if($_FILES['photo']['name']!='') {   
            //remove old file
            $user = User::where('id', $id)->first();
            $file = $user->photo;
            if(isset($file) && $file!='')
            unlink(storage_path('app/public/user/'.$file));
            User::where('id', $id)->update(['photo' => '']);

            $file = $request->file('photo');
            $file->store('user', ['disk' => 'public']);
            $filename = $request->photo->hashName();
            $user_photo = array('photo' => $filename);
            User::where('id', $id)->update($user_photo);
        }

        Session::flash('message', 'Successfully updated'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect()->to('myprofile');
    }
}   
