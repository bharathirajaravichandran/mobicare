<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSubscription extends Model
{
    protected $table = 'user_subscription';
    protected $fillable = [
        'user_id', 'plan_id', 'plan_name', 'start_date', 'end_date', 'amount_paid', 'status', 'expiry_status'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
