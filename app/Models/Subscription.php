<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
	protected $table = 'subscription_plans';
    protected $fillable = [
        'subscription_id', 'name', 'description', 'duration', 'amount', 'status'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
