<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $fillable = [
        'name', 'image', 'status', 'parent_category', 'minimum_fees', 'competent_skills', 'hundred_pt_check', 'police_check', 'certificates_uploaded', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'industry';
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user_category()
    {
        return $this->hasOne('App\User');
    }
}
