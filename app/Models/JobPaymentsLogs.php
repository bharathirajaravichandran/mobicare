<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class JobPaymentsLogs extends Model
{
    protected $fillable = [
          'job_payments_id',
          'payment_method_id',
          'transaction_log',
          'created_at',
          'updated_at',
          'status'
    ];
    public $timestamps = false;
    protected $table = 'job_payment_logs';   

   /* use SoftDeletes;
    protected $dates = ['deleted_at'];*/
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
