<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserPermissions extends Model
{
    protected $fillable = [
        'user_id', 'module_id', 'view', 'add', 'edit', 'delete'
    ];
    public $timestamps = false;
    protected $table = 'user_permissions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
