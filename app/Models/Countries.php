<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
    protected $fillable = [
        'iso', 'name', 'nicename', 'iso3', 'numcode', 'phonecode', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $table = 'countries';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
