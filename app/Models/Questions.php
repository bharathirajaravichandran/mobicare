<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questions extends Model
{
    protected $table = 'question';
    protected $fillable = [
        'type', 'category', 'subcategory', 'question', 'answer_option', 'answer_type', 'is_mandatory', 'input_type', 'status', 'user_type'
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function getcategory()
    {
        return $this->belongsTo('App\Models\Category','category');
    }
    public function getsubcategory()
    {
        return $this->belongsTo('App\Models\Category','subcategory');
    }

}
