<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ResourceFiles extends Model
{
    protected $fillable = [
        'resource_id', 'original_file', 'file'
    ];
    public $timestamps = false;
    protected $table = 'resource_files';

    public function resources()
    {
    	return $this->belongsTo(Resources::class, 'id');
    }
}
