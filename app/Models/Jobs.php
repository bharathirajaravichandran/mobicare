<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jobs extends Model
{
    protected $fillable = [
        'type', 'job_id', 'category_id','subcategory_id', 'name', 'description', 'budget', 'quoted_amount', 'start_date', 'start_time', 'end_date', 'end_time', 'venue', 'latitude', 'langitude', 'conditions', 'work_with_children', 'job_file', 'request_type','amount_charged', 'created_at', 'updated_at', 'status'
    ];
    public $timestamps = false;
    protected $table = 'jobs';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function job_conversations()
    {
        return $this->hasMany('App\Models\JobConversations');
    }
    public function job_conversation_list()
    {
        return $this->hasMany('App\Models\JobConversations')->orderBy('created_at','desc')->groupBy('user_id');
    }

    public function job_assigned()
    {
        return $this->hasMany('App\Models\JobAssigned', 'jobs_id');
    }
    public function getcategory()
    {
        return $this->belongsTo('App\Models\Category','category_id');
    }
    public function getsubcategory()
    {
        return $this->belongsTo('App\Models\Category','subcategory_id');
    }

    public function getWorkingDaysAttribute($working_days)
    {        
        if (is_string($working_days)) {
            return explode(',',$working_days);
        }        
        return $working_days;
    }
   /* public function setWorkingDaysAttribute($working_days)
    {        
        if (is_array($working_days)) {
            $this->attributes['working_days'] = json_encode($working_days);
           return $this->attributes['working_days'];
        }
       
    }*/
}
