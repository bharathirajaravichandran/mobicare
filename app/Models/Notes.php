<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notes extends Model
{
	protected $table = 'notes';
    /*protected $fillable = [
        'category_id', 'subcategory_id', 'name', 'status', 'created_at', 'updated_at'
    ];*/

    use SoftDeletes;
   /* protected $dates = ['deleted_at'];*/

     public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
