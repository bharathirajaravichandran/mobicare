<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobReviews extends Model
{
    protected $fillable = [
        'jobs_id', 'user_id', 'is_dispute', 'ratings', 'feedback', 'status', 'reply_text', 'reply_status', 'created_at',  'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'job_reviews';   

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
