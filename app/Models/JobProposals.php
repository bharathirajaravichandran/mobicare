<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class JobProposals extends Model
{
    protected $fillable = [
        'jobs_id', 'user_id', 'my_offer','my_cover_letter', 'created_at', 'updated_at', 'status'
    ];
    protected $table = 'job_proposals';   

    public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
