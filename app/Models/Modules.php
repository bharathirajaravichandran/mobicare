<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    protected $fillable = [
        'module_name', 'link', 'font_awesome_icon', 'status', 'parent', 'orders', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'modules';
}
