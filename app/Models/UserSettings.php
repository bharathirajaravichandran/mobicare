<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    protected $fillable = [
        'name', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'user_settings';
}
