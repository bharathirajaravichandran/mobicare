<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobAssigned extends Model
{
    protected $fillable = [
        'hired_at', 'user_id', 'jobs_id', 'amount', 'created_at', 'updated_at', 'status'
    ];
    public $timestamps = false;
    protected $table = 'job_assigned';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function job()
    {
        return $this->belongsTo('App\Models\Jobs', 'jobs_id');
    }
}
