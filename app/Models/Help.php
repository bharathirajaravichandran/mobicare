<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    protected $fillable = [
        'type', 'title', 'question', 'answer', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'help';

    public function getAnswerAttribute($answer)
		{
		    return strip_tags($answer);
		}
}
