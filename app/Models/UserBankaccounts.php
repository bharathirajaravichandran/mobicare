<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBankaccounts extends Model
{
    protected $fillable = [
        'user_id', 'bank_name', 'ifsc_code', 'account_no', 'account_holder_name', 'branch_name', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'user_bank_accounts';
}
