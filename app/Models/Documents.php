<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documents extends Model
{
    protected $fillable = [
        'name', 'points', 'industry_id', 'is_mandatory', 'is_primary', 'is_graduate', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'documents';

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function industry()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function user_documents()
    {
        return $this->hasMany('App\Models\UserDocuments');
    }
}
