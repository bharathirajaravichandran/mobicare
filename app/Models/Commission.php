<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commission extends Model
{
    protected $fillable = [
        'name', 'commission_value', 'commission_type', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'admin_commission';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
