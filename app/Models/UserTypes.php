<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTypes extends Model
{
    protected $fillable = [
        'name', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'user_types';

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
