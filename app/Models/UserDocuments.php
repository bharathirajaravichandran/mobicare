<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserDocuments extends Model
{
	protected $fillable = [
        'user_id', 'industry_id', 'documents_id', 'file_path', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'user_documents';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function industry()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function documents()
    {
        return $this->belongsTo('App\Models\Documents');
    }
}
