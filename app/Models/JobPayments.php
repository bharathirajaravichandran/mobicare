<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class JobPayments extends Model
{
    protected $fillable = [
        'jobs_id', 'user_id', 'is_dispute','admin_bank_accounts','initiated_by',
          'payment_notes',
          'amount',
          'payment_type',
          'issued_on',
          'created_at',
          'updated_at',
    ];
    public $timestamps = false;
    protected $table = 'job_payments';   

   /* use SoftDeletes;
    protected $dates = ['deleted_at'];*/
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
