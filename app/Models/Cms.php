<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $fillable = [
        'pagename', 'pagelink', 'pagecontent', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'cms';
}
