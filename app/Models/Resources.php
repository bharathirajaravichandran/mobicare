<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resources extends Model
{
    protected $fillable = [
        'title', 'description', 'type', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'resources';
    
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function resourcefiles()
	{
		return $this->hasMany(ResourceFiles::class, 'resource_id');
	}
}
