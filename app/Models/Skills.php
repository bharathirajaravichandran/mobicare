<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skills extends Model
{
	protected $table = 'skills';
    protected $fillable = [
        'category_id', 'name', 'status', 'created_at', 'updated_at'
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id');
    }
}
