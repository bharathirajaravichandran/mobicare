<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobSuggestion extends Model
{
    protected $table = 'job_suggestion';
    protected $fillable = [
        'assigned_at', 'job_id', 'user_id', 'status'
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function job()
    {
        return $this->belongsTo('App\Models\Jobs','job_id');
    }
}
