<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserQuestionsAnswer extends Model
{
    protected $fillable = [
        'jobs_id', 'user_id', 'question_id', 'type', 'answer', 'status', 'created_at',  'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'user_question_answer';   
    use SoftDeletes;
   

    public function jobs()
    {
        return $this->belongsTo('App\Models\Jobs');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
