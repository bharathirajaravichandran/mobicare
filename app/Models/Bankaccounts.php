<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bankaccounts extends Model
{
    protected $fillable = [
        'display_name', 'bank_name', 'account_no', 'ifsc_code', 'branch_name', 'is_primary', 'status', 'created_at', 'updated_at'
    ];
    public $timestamps = false;
    protected $table = 'admin_bank_accounts';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
